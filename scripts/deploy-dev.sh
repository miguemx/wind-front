#!/usr/bin/env bash

SSH_USER="tidi"
SSH_DOMAIN="tide.company"
PROJECT_FULL_PATH="/home/tidi/public_html/wind-dev"

DEV=$(grep "REACT_APP_BUILD=dev" .env)
if [[ "$DEV" != "REACT_APP_BUILD=dev" ]]; then
      echo "REACT_APP_BUILD variable in .env must be set to 'dev'"
      exit 1
fi

echo "Building app..."
yarn run build > /dev/null
OUT=$?
if [[ $OUT -ne 0 ]]; then
    echo "$(tput setaf 1)Error \"npm run build\" command returned non-zero value ($OUT) $(tput sgr 0)"
    exit $OUT
fi

echo "Uploading files to $PROJECT_FULL_PATH"
scp -rq build/* "$SSH_USER@$SSH_DOMAIN:$PROJECT_FULL_PATH"
OUT=$?
if [[ $OUT -ne 0 ]]; then
    echo "$(tput setaf 1)Error \"scp\" command returned non-zero value ($OUT) $(tput sgr 0)"
    exit $OUT
fi

echo "Success"
