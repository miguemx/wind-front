#!/usr/bin/env bash

SSH_USER="wind"
SSH_DOMAIN="wind.tide.mx"
PROJECT_FULL_PATH="/home/wind/apps/wind-front"

PROD=$(grep "REACT_APP_BUILD=prod" .env)
if [[ "$PROD" != "REACT_APP_BUILD=prod" ]]; then
    sed -ibak "/REACT_APP_BUILD=/c\\REACT_APP_BUILD=prod" .env

    if [[ $? -ne 0 ]]; then
        OUT=$?
        echo "$(tput setaf 1)Error \"sed -ibak \"/REACT_APP_BUILD=/c\\REACT_APP_BUILD=prod\" .env\" command returned non-zero value ($OUT) $(tput sgr 0)"
        mv .envbak .env
        exit $?
    fi

fi

echo "Building app..."
yarn run build > /dev/null
OUT=$?
if [[ $OUT -ne 0 ]]; then
    echo "$(tput setaf 1)Error \"npm run build\" command returned non-zero value ($OUT) $(tput sgr 0)"
    mv .envbak .env
    exit $OUT
fi
mv .envbak .env

echo "Uploading files to $PROJECT_FULL_PATH"
scp -rq build/* "$SSH_USER@$SSH_DOMAIN:$PROJECT_FULL_PATH"
OUT=$?
if [[ $OUT -ne 0 ]]; then
    echo "$(tput setaf 1)Error \"scp\" command returned non-zero value ($OUT) $(tput sgr 0)"
    exit $OUT
fi

echo "Success"
