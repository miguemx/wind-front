import React, { useContext } from 'react';
import WindInput from '../../../../../components/formComponents/WindInput/WindInput';
import WindSwitch from '../../../../../components/formComponents/WindSwitch/WindSwitch';
import TideEntitySelect from '../../../../../components/utility/TideEntitySelect/TideEntitySelect';
import { SecurityContext } from '../../../../../services/SecurityManager';

const UserInfo = ({ bindSimple, userId }) => {
  const security = useContext(SecurityContext);

  const userCannotEditCompany = security.isTenantUser();
  
  return (
    <> 
      <div className='fields'>
        <WindInput
          className='project-field'
          placeholder={"Nombre"}
          {...bindSimple('name')}
        />

        <WindInput
          className='project-field'
          placeholder={"Apellido"}
          {...bindSimple('lastName')}
        />

        <TideEntitySelect
          className='project-field'
          entity='clients'
          placeholder={"Empresa"}
          {...bindSimple('client')}
          disabled={userCannotEditCompany}
        />

        <WindSwitch
          className='project-field'
          placeholder={'Activo'}
          {...bindSimple('isActive')}
        />

        {!userId && 
          <WindSwitch
            className='project-field'
            placeholder={'Enviar accesos'}
            {...bindSimple('sendAccess')}  
          />}
      </div>

      <div className='fields'>
        <WindInput
          className='project-field'
          placeholder={"Usuario"}
          {...bindSimple('username')}
        />

        <WindInput
          className='project-field'
          placeholder={"Teléfono"}
          {...bindSimple('phone')}
        />

        <WindInput
          className='project-field'
          placeholder={"Puesto"}
          {...bindSimple('position')}
        />

        <WindInput
          className='project-field'
          placeholder={"Email"}
          {...bindSimple('email')}
        />
      </div>
    </>
  );
}
 
export default UserInfo;
