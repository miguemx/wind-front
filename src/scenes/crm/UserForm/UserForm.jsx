import { faSave } from '@fortawesome/pro-light-svg-icons';
import React, { useCallback, useMemo, useEffect } from 'react';
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import ToolBar from '../../../components/utility/ToolBar/ToolBar';
import TopBar from '../../../components/utility/TopBar/TopBar';
import useFormState from '../../../hooks/useFormState';
import { ApiContext } from '../../../services/api/api-config';
import { getEmptyUser, prepareUserForServer, usersDetailSGroups } from '../../../services/modelUtils/userUtils';
import { getNotifier } from '../../../services/notifier';
import { paths } from '../../../services/routes/appRoutes';
import UserInfo from './components/UserInfo/UserInfo';
import './UserForm.scss';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import DeactivateUserConfirmModal from './components/DeactivateUserConfirmModal/DeactivateUserConfirmModal';
import useBoolean from '../../../hooks/useBoolean';
import SimpleCardTwoColumns from '../../../components/layout/SimpleCardTwoColumns/SimpleCardTwoColumns';
import UserAvatar from './components/UserAvatar/UserAvatar';

const UserForm = () => {
  const { form, setForm, bindSimple } = useFormState(() =>getEmptyUser());
  const [ deactivateUserModal, deactivateUserModalOn , deactivateUserModalOff, ] = useBoolean(false);
  const history = useHistory();
  const api = useContext(ApiContext);

  const urlParams = window.location.search;
  const parsedParams = new URLSearchParams(urlParams);

  const clientId = parsedParams.get('client');

  const { id } = useParams();

  // ----- Load User data
  const loadUserData = 'UserDetail.get.'+id;

  const reloadUser = useCallback(() => {
    api.users.get({ id, loadingId: loadUserData, params: { sGroups: usersDetailSGroups } })
      .then(user => {
        setForm(user);
      })
      .catch(() => getNotifier().error('No se encontró el usuario solicitado'));
  }, [api, id, loadUserData, setForm]);

  const loadClient = useCallback(() => {
    api.clients.get({ id: clientId }).then(client => setForm( form => ({...form, client})));
  }, [api, clientId, setForm]);

  useEffect(()=>{ if(id) { reloadUser() } },[id, reloadUser]);

  useEffect(()=> { if(clientId) { loadClient() }}, [loadClient, clientId]);

  // ----- Save User data
  const loadingId = 'UserForm.create';

  const handleSave = useCallback(()=>{
    const { id } = form;
    const apiMethod = id ? 'update' : 'create';

    let user;
    try{
        user = prepareUserForServer({...form });
    }
    catch (e){
        return getNotifier().warning(e.message);
    }

    const params = { id, params: user, loadingId };
    api.users[apiMethod](params)
      .then((user)=>{
        getNotifier().success(`El usuario se ha ${id?'editado':'creado'} correctamente`);
        history.replace( paths.usersDetail.replace(':id', user.id) );
      })
      .catch((e)=>{ getNotifier().error(e.message) })
  }, [form, api, history]);

  const loading = useSelector(s=>!!s.loadingIds[loadingId]);

  const tools = useMemo(()=>[
    { icon: faSave, callback: !form.isActive?() => deactivateUserModalOn():handleSave, text: "Guardar usuario", disabled: loading },
  ], [ handleSave, loading, form, deactivateUserModalOn ]);

  const Avatar = <UserAvatar user={form} className="avatar" reloadUser={reloadUser} />;

  return (
    <div className={"UserForm  wind-scene"}>

      <TopBar
        title={`${id?'Editar':'Nuevo'} usuario`}
        titleLinkBack
      />

      <ToolBar tools={tools} />
      <div className='center-container'>
        <SimpleCardTwoColumns title="Información general" child2={id && Avatar}>
          <UserInfo bindSimple={bindSimple} userId={id} />
        </SimpleCardTwoColumns>
      </div>

      {deactivateUserModal && <DeactivateUserConfirmModal onConfirm={handleSave} onClose={deactivateUserModalOff} />}
    </div>
  );
}

export default UserForm;
