
import React, {useState} from 'react';
import './Dashboard.scss';
import TopBar from "../../../components/utility/TopBar/TopBar";
import SimpleCard from '../../../components/layout/SimpleCard/SimpleCard';
import Payments from './components/Payments/Payments';
import AccountsReceivable from './components/AccountsReceivable /AccountsReceivable ';
import MoneyFlow from './components/MoneyFlow/MoneyFlow';
import PaymentsByCategory from './components/PaymentsByCategory/PaymentsByCategory';
import IconButton from '../../../components/utility/IconButton/IconButton';
import { faChartPie, faList } from '@fortawesome/pro-light-svg-icons';
import PaymentsByCategoryTable from './components/PaymentsByCategoryTable/PaymentsByCategoryTable';
import WindDatePicker from '../../../components/formComponents/WindDatePicker/WindDatePicker';
import useFormState from '../../../hooks/useFormState';
import IncomesByCategory from './components/IncomesByCategory/IncomesByCategory';
import IncomesByCategoryTable from './components/IncomesByCategoryTable/IncomesByCategoryTable';
import TideEntitySelect from '../../../components/utility/TideEntitySelect/TideEntitySelect';

const initialState = {
    paymentsPeriodStartDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
    paymentsPeriodEndDate: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0, 23, 59, 59),
    incomesPeriodStartDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
    incomesPeriodEndDate: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0, 23, 59, 59),
    client: null
}

const Dashboard = () => {
    const [showPaymentsGraph, setShowPaymentsGraph] = useState(true);
    const [showIncomesGraph, setShowIncomesGraph] = useState(true);
    const {form, bindSimple} = useFormState(initialState);

    return (
        <div className={"Dashboard wind-scene"}>

            <TopBar
                title="Dashboard"
            />

            <div className='center-container'>
                <div className="dashboard-grid-row-0">                    
                    <SimpleCard className='general-info' title="Flujos">
                        <MoneyFlow />
                    </SimpleCard>
                </div>

                <div className="dashboard-grid-row">
                    <SimpleCard className='general-info' title="Egresos por categoría">
                        <div className='graph-buttons'>
                            <IconButton icon={faChartPie} onClick={() => setShowPaymentsGraph(true) } />
                            <IconButton icon={faList} color={"black"} className="mt-2" onClick={() => setShowPaymentsGraph(false) } />
                        </div>

                        <div className='mt-filters'>
                            <WindDatePicker
                                label="Desde"
                                className="mr-2"
                                {...bindSimple('paymentsPeriodStartDate')}
                            />
                        </div>
                        <div className='mt-filters'>
                            <WindDatePicker
                                label="Hasta"
                                {...bindSimple('paymentsPeriodEndDate')}
                            />
                        </div>

                        {showPaymentsGraph ? 
                            <PaymentsByCategory periodStartDate={form.paymentsPeriodStartDate} periodEndDate={form.paymentsPeriodEndDate} />
                            : 
                            <PaymentsByCategoryTable periodStartDate={form.paymentsPeriodStartDate} periodEndDate={form.paymentsPeriodEndDate} />}
                    </SimpleCard>
                    <SimpleCard className='general-info' title="Ingresos por categoría">
                        <div className='graph-buttons--incomes'>
                            <IconButton icon={faChartPie} onClick={() => setShowIncomesGraph(true) } />
                            <IconButton icon={faList} color={"black"} className="mt-2" onClick={() => setShowIncomesGraph(false) } />
                        </div>

                        <div className='mt-filters'>
                            <WindDatePicker
                                label="Desde"
                                className="mr-2"
                                {...bindSimple('incomesPeriodStartDate')}
                            />
                        </div>
                        <div className='mt-filters'>
                            <WindDatePicker
                                label="Hasta"
                                {...bindSimple('incomesPeriodEndDate')}
                            />
                        </div>
                        <div className='mt-filters'>
                            <TideEntitySelect
                                label="Cliente"
                                {...bindSimple('incomesClient')}
                                entity='Clients'
                                preload={true}
                            />
                        </div>

                        {showIncomesGraph ? 
                            <IncomesByCategory 
                                periodStartDate={form.incomesPeriodStartDate} 
                                periodEndDate={form.incomesPeriodEndDate} 
                                client={form.incomesClient}
                            />
                            : 
                            <IncomesByCategoryTable 
                                periodStartDate={form.incomesPeriodStartDate} 
                                periodEndDate={form.incomesPeriodEndDate} 
                                client={form.incomesClient}
                            />}
                    </SimpleCard>
                </div>
                
                <div className="dashboard-grid-row">
                    <SimpleCard className='general-info' title="Pagos pendientes">
                        <Payments />
                    </SimpleCard>
                    <SimpleCard className='general-info' title="Cuentas por cobrar">
                        <AccountsReceivable />
                    </SimpleCard>
                </div>
            </div>
        </div>
    );
}

export default Dashboard;