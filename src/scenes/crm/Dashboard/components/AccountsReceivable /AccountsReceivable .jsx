import React, {useMemo} from 'react';
import {quoteListSGroups} from "../../../../../services/modelUtils/quoteUtils";
import {Link} from "react-router-dom";
import {paths} from "../../../../../services/routes/appRoutes";
import { moneyFormatter } from '../../../../../services/currencyUtils';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';

const AccountsReceivable = () => {
    const columns = useMemo(()=>[
        { Header:"Cotización", 
            id: 'folio',
            accessor: (quote)=>
                <Link to={paths.quoteDetail.replace(':id', quote.id)} data-tooltip={'Ver cotización'} >
                    {quote.title}
                </Link>,
        },
        { Header:"Concepto", accessor: 'title' },        
        {Header:"Cantidad", id:'payed',  accessor: (q) => `$ ${moneyFormatter(Number(q.pendingAmount))}`}
    ], []);

    const quoteFilters = useMemo(() => ({
        sGroups:quoteListSGroups, 
        'order[createdDate]':'DESC',
    }), []);

    const table = useTideTable({
        entity: 'quotes',
        columns,
        requestFilters: quoteFilters,
        itemsPerPage: 5,
    });

    return (
        <div className={"QuoteTable"}>

            <TideReactTable
                {...table.tableProps}
            />

        </div>
    );
};

export default AccountsReceivable;
