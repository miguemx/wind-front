import React, {useMemo} from 'react';
import { transactionGraphSGroups } from "../../../../../services/modelUtils/transactionUtils";
import { moneyFormatter } from '../../../../../services/currencyUtils';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import './Payments.scss';

const Payments = () => {
    const columns = useMemo(()=>[
      { Header:"Folio", accessor: 'folio' },
      { Header:"Concepto", accessor: 'concept' },
        {Header:"Cantidad", id:'amount',  accessor: (q) => `$ ${moneyFormatter(Number(q.amount))}`}
    ], []);

    const transactionsFilters = useMemo(() => ({
        sGroups: transactionGraphSGroups,
        'status': 'pending',
      }), []);

    const table = useTideTable({
        entity: 'transactions',
        columns,
        requestFilters: transactionsFilters,
        itemsPerPage: 5,
    });

    return (
        <div className={"Payments"}>

            <TideReactTable
                {...table.tableProps}
            />

        </div>
    );
};

export default Payments;