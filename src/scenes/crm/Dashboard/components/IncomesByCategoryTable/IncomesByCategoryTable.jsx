import React, {useMemo} from 'react';
import { moneyFormatter } from '../../../../../services/currencyUtils';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import './IncomesByCategoryTable.scss';

const IncomesByCategoryTable = ({periodStartDate, periodEndDate, client}) => {
    const columns = useMemo(()=>[
        { Header:"Categoría", accessor: 'name'},
        {Header:"Total", id:'total',  accessor: (cat) => `$ ${moneyFormatter(Number(cat.total))}`}
    ], []);

    const table = useTideTable({
        entity: 'transactionCategories',
        getMethod: 'incomesByCategory',
        columns,
    });

    return (
        <div className={"IncomesByCategoryTable"}>

            <TideReactTable
                {...table.tableProps}
            />

        </div>
    );
};

export default IncomesByCategoryTable;
