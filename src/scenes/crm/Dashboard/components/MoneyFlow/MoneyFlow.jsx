
import React, { useContext, useEffect, useMemo, useState, useCallback } from "react";
import ReactEcharts from "echarts-for-react";
import { transactionGraphSGroups } from "../../../../../services/modelUtils/transactionUtils";
import { ApiContext } from "../../../../../services/api/api-config";
import moment from "moment";

function getDates (startDate, endDate) {
    const dates = [];
    let currentDate = startDate;
    const addDays = function (days) {
        const date = new Date(this.valueOf())
        date.setDate(date.getDate() + days);
        return date;
    }

    while (currentDate <= endDate) {
        dates.push(currentDate);
        currentDate = addDays.call(currentDate, 1);
    }

    return dates;
}

const initialData = {
    incomes: [],
    expenses: [],
    dates: []
}

const MoneyFlow = () => {
    const to = useMemo(() => new Date(), []);
    const from = useMemo(() => new Date(to.getFullYear(), to.getMonth(), 1), [to]);
    const datesArray = useMemo(() => getDates(from, to), [from, to]);

    const api = useContext(ApiContext);

    const apiConfig = useMemo(() => ({params: {
        sGroups: transactionGraphSGroups,
        'transactionDate[after]': from,
        'transactionDate[before]': to,
        'status': 'approved'
    }}), [from, to]);

    const [data, setData] = useState(initialData);
    
    const { incomes, expenses, dates } = data;

    const generateData = useCallback((transactions) => {        
        let incomes = [];
        let expenses = [];
        let formatedDates = [];

        datesArray?.forEach(date => {
            const dateTransactions = transactions.filter(transaction =>  moment(transaction.transactionDate).format('MM/DD/YYYY') === moment(date).format('MM/DD/YYYY'));
            incomes.push(dateTransactions.filter(transaction => transaction.transactionType === 'income').reduce((acc, transaction) => acc + (transaction.originalAmount/1000), 0));
            expenses.push(dateTransactions.filter(transaction => transaction.transactionType === 'expense').reduce((acc, transaction) => acc + (transaction.originalAmount/1000), 0));

            formatedDates.push(moment(date).format('MMM DD'));
        });
        
        setData({dates: formatedDates, incomes, expenses});
    }, [datesArray]);

    // load transactions data
    useEffect(() => {
        api.transactions.get(apiConfig).then(generateData)
    }, [api, apiConfig, generateData]);

    const options = {
        xAxis: {
            type: 'category',
            data: dates,
            axisLabel:{
                fontSize: 21,
                color: function(value, index){
                    return '#D1D4D9';
                }
            }
        },
        legend: {},
        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    formatter: function (value) {
                        return   value+"K"
                    },
                    fontSize: 21,
                    color: function(value, index){
                        return '#D1D4D9';
                    }
                }
            }
        ],
        series: [
            {
                name: 'Ingresos',
                data: incomes,
                type: 'line',
                smooth: true,
                lineStyle: {
                    color: '#e77678',
                    width: 5,
                    shadowBlur: 70.5,
                    shadowOffsetY: 50
                },
                emphasis: {
                    focus: 'series',
                    label: {
                        show: true,
                        position: 'top',
                        textStyle: {
                            fontSize: '14',
                            fontWeight: 'bold',
                            backgroundColor: '#000',
                            color: '#fff',
                            padding: [10, 10]
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: true
                    }
                },
            },
            {
                name: 'Egresos',
                data: expenses,
                type: 'line',
                smooth: true,
                lineStyle: {
                    color: '#4476AA',
                    width: 5,
                    shadowBlur: 70.5,
                    shadowOffsetY: 50
                },
                emphasis: {
                    focus: 'series',
                    label: {
                        show: true,
                        position: 'top',
                        textStyle: {
                            fontSize: '14',
                            fontWeight: 'bold',
                            backgroundColor: '#000',
                            color: '#fff',
                            padding: [10, 10]
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: true
                    }
                },
            }
        ]
    };

    return (
        <ReactEcharts
            option={options}
            style={{height: "400px", width: "100%"}}
        />
    );
}
export default MoneyFlow;
