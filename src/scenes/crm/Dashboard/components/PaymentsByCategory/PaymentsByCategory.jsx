import { useState, useEffect, useContext } from "react";
import ReactEcharts from "echarts-for-react";
import {ApiContext} from "../../../../../services/api/api-config";
import './PaymentsByCategory.scss';
import { moneyFormatter } from "../../../../../services/currencyUtils";

const PaymentsByCategory = ({periodStartDate, periodEndDate}) => {
    const api = useContext(ApiContext);

    // load graph data
    const [paymentsData, setPaymentsData] = useState();
    useEffect(() => { 
        let filters = {};

        if (periodStartDate) {
            filters["from"] = periodStartDate;
        }

        if (periodEndDate) {
            filters["to"] = periodEndDate;
        }

        api.transactionCategories.expensesByCategories({params: filters}).then(assignData) 
    }, [api, periodStartDate, periodEndDate]);
    
    const assignData = (data) => {
        let graphData = [];

        data.forEach(category => {
            graphData.push({
                "value": Math.abs(category.total),
                "name": category.name,
                "itemStyle": {}
            });
        });

        setPaymentsData(graphData);
    };

    const dataNames = paymentsData?.map((i) => i.name);

    const style = {
        height: "400px",
        width: "100%",
        margin: "35px auto 0 auto",
        fontSize: "9px",
    };

    let options = {
        toolbox: {
            show: true,
            feature: {
                mark: {
                    show: true
                },
                magicType: {
                    show: true,
                    type: ["pie", "funnel"]
                }
            }
        },
        graphic: [
            {
                type: "group",
                rotation: Math.PI / 4,
                bounding: "raw",
                right: 200,
                bottom: 100,
                z: 100,
                children: []
            },
            {
                type: "group",
                left: "0",
                top: "bottom",
                children: []
            }
        ],
        // Hover Tooltip
        tooltip: {
            trigger: "item",
            formatter: function (params) {
                var colorSpan = '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + params.color + '"></span>';
                let rez =  '<div style="min-width: 150px;">' + 
                                '<div>' + params.seriesName + '</div>' +
                                '<div style="display: flex; justify-content: space-between;"><div>' + colorSpan + params.data.name + '</div><div>'+  moneyFormatter(params.data.value) +'</div></div>' +
                            '</div>';
                
                return rez;
            } 
        },
        title: {
            text: "Egresos",
            left: "center",
            top: '190px',
            textStyle: {
                color: "#4C4C4E"
            }
        },
        calculable: true,
        legend: {
            icon: "suaquare",
            x: "center",
            y: "0px",
            data: dataNames,
            textStyle: {
                color: "#4C4C4E",
                fontSize: "12px"
            }
        },
        series: [
            {
                name: "Egresos",
                type: "pie",
                animationDuration: 2000,
                animationEasing: "quarticInOut",
                radius: [90, 150],
                avoidLabelOverlap: false,
                startAngle: 90,
                hoverOffset: 105,
                center: ["50%", "50%"],
                //roseType: "area",
                selectedMode: "multiple",
                label: {
                    normal: {
                        show: false,
                        formatter: "{c}" // {c} data: [{value:},]
                    },
                    emphasis: {
                        show: false
                    }
                },
                labelLine: {
                    normal: {
                        show: true,
                        smooth: false,
                        length: 20,
                        length2: 10
                    },
                    emphasis: {
                        show: true
                    }
                },
                formatter: "xxx{b}",
                data: paymentsData
            }
        ]
    };

    return (
        <ReactEcharts
            option={options}
            style={style}
        />
    );
}

export default PaymentsByCategory;
