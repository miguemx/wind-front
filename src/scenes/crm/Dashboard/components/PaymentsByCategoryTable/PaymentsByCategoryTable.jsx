import React, {useMemo} from 'react';
import { moneyFormatter } from '../../../../../services/currencyUtils';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import './PaymentsByCategoryTable.scss';

const PaymentsByCategoryTable = () => {
    const columns = useMemo(()=>[
        { Header:"Categoría", accessor: 'name'},
        {Header:"Total", id:'total',  accessor: (cat) => `$ ${moneyFormatter(Number(cat.total))}`}
    ], []);

    const table = useTideTable({
        entity: 'transactionCategories',
        getMethod: 'expensesByCategory',
        columns,
    });

    return (
        <div className={"PaymentsByCategoryTable"}>

            <TideReactTable
                {...table.tableProps}
            />

        </div>
    );
};

export default PaymentsByCategoryTable;
