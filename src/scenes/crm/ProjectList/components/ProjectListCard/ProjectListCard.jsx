import React, { useState } from 'react';
import  './ProjectListCard.scss';
import CompanyBadge from "../../../../../components/crmComponents/CompanyBadge/CompanyBadge";
import {Link} from "react-router-dom";
import {paths} from "../../../../../services/routes/appRoutes";
import {getProjectStatusTrans} from "../../../../../services/modelUtils/projectUtils";
import {moneyFormatter} from "../../../../../services/currencyUtils";

const ProjectListCard = ( {project} ) => {

    const {approvedQuotesSum, paymentsSum} =  project.projectStats||{};
    const pendingPayment = (approvedQuotesSum - paymentsSum)||0;
    const [showDetails, setShowDetails] = useState(false);

    return (
        <div className='ProjectListCard' 
            onMouseOver={() => setShowDetails(true)}
            onMouseLeave={() => setShowDetails(false)}
        >
            <div className={'company-margin'}>
                <CompanyBadge text={project.client?.initials||'CL'}/>
            </div>
            <div className='content'>
                <Link to={paths.projectDetail.replace(":id", project.id)} data-tooltip={"Ir al proyecto"}>
                    <h2 className='project-name'>{project.name}</h2>
                </Link>

                <div className='stats' style={{ display: showDetails ? 'flex' : 'none' }}>
                    <div className='stats-col'>
                        <Link to={paths.clientDetail.replace(":id", project.client?.id)} data-tooltip={"Ver perfil del cliente"}>
                            <p className='stat main'>{project.client?.name||'Sin cliente'}</p>
                        </Link>
                        <p className='stat'>{project.projectStats?.pendingQuotesCount||0} cotizaciones pendientes</p>
                        <p className='stat'>{project.projectStats?.approvedQuotesCount||0} cotizaciones aprobadas</p>
                    </div>
                    <div className='stats-col'>
                        <Link to={`${paths.clientDetail.replace(":id", project.client?.id)}/?tab=accountStatement`} data-tooltip={"Ver estado de cuenta del cliente"}>
                            <p className='stat main'>{getProjectStatusTrans(project.status)}</p>
                        </Link>
                        <p className='stat'>Pendiente $ {moneyFormatter(pendingPayment)} </p>
                        <p className='stat'>Costo total $ {moneyFormatter(approvedQuotesSum||0)}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProjectListCard;