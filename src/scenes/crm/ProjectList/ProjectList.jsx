import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import './ProjectList.scss';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {faList, faPlus, faFileExcel, faFileInvoiceDollar, faMessageQuestion} from "@fortawesome/pro-light-svg-icons";
import {useHistory} from "react-router-dom";
import {paths} from "../../../services/routes/appRoutes";
import {ApiContext} from "../../../services/api/api-config";
import {useSelector} from "react-redux";
import {projectListSGroups, projectStatus, projectStatusOptionsForSelect} from "../../../services/modelUtils/projectUtils";
import _ from 'lodash';
import {filterTypes} from "../../../services/searchUtils";
import { SecurityContext } from '../../../services/SecurityManager';
import ProjectListCard from './components/ProjectListCard/ProjectListCard';

const filtersConfig = {
    placeholder: 'Buscar un proyecto...', filters: [
        {main: true, field: 'search'},
        {field: 'name', label:'Nombre del proyecto'},
        {field: 'client.name', label:'Cliente'},
        {type: filterTypes.ORDER, field: 'order[createdDate]', label:'Ordenar por creación'},
        {type: filterTypes.SELECT, field: 'status', label:'Estatus', isMulti: true, options:projectStatusOptionsForSelect},
    ]
};
const initialFilters = {status:[projectStatus.PENDING, projectStatus.IN_DEVELOPMENT, projectStatus.OPEN]};

const ProjectList = () => {

    const api = useContext(ApiContext);
    const securityManager = useContext(SecurityContext);
    const history = useHistory();
    const [filters, setFilters] = useState({});

    // ----- Loading project list
    const [projects, setProjects] = useState([]);
    const loadProjects = useMemo(() => _.debounce((filters) => {
        api.projects.get({params: {sGroups: projectListSGroups, ...filters, ...initialFilters}}).then(setProjects);
    }, 650), [api]);

    useEffect(() => {
        loadProjects(filters);
    }, [loadProjects, filters]);

    useEffect(() => {
        api.projects.generalStats({customProp: 'projectsStats'});
    }, [api]);
    const projectsStats = useSelector(s => s.api.projectsStats);


    // ----- Link to add projects
    const handleAdd = useCallback(() => history.push(paths.projectNew), [history]);
    const redirectToProjectsTable = useCallback(() => history.push(paths.projectsTable), [history]);
    const exportToExcel = useCallback(() => {window.location.href = api.projects.getExportExcelUrl({pagination: false})}, [api]);
    const tools = useMemo(() => {
        let tools = [
            {icon: faPlus, callback: handleAdd, text: "Crear nuevo proyecto"},
            {icon: faList, callback: redirectToProjectsTable, text: "Ver tabla de proyectos"},
            {icon: faFileInvoiceDollar, callback: ()=>history.push(paths.quoteForm), text: "Nueva cotización",
                disabled: !securityManager.canAdminQuotes()},
            {icon: faMessageQuestion, callback: ()=>history.push(paths.quotationRequestNew), text: "Nueva solicitud",
                disabled: !securityManager.canAdminQuotationRequests()},
            {icon: faFileExcel, callback: exportToExcel, text: "Descargar excel"},
        ];

        return tools.filter(t => !t.disabled);
    }, [handleAdd, redirectToProjectsTable, exportToExcel, history, securityManager]);

    // Display vars
    const activeProjects = Number(projectsStats?.activeProjectsCount) || 0;
    const pendingPaymentQuotes = Number(projectsStats?.pendingPaymentQuotesCount) || 0;
    const pendingQuotes = Number(projectsStats?.pendingQuotesCount) || 0;

    return (
        <div className={"ProjectList wind-scene"}>
            <TopBar title="Proyectos" filters={filters} filtersConfig={filtersConfig} onFiltersChange={setFilters}/>
            <ToolBar tools={tools}/>
            <div className='center-container'>
                <div className='projects-header'>
                    <span className='project-stat'>
                        {activeProjects} {activeProjects === 1 ? 'proyecto activo' : 'proyectos activos'}
                    </span>
                    <span className='project-stat'>
                        {pendingPaymentQuotes} {pendingPaymentQuotes === 1 ? 'cotización pendiente' : 'cotizaciones pendientes'} de pago
                    </span>
                    <span className='project-stat'>
                        {pendingQuotes} {pendingQuotes === 1 ? 'cotización' : 'cotizaciones'} por aprobar
                    </span>
                </div>

                {projectStatusOptionsForSelect.map(status => {
                    const filteredProjects = projects.filter(p => p.status === status.value);

                    return filteredProjects.length > 0 &&
                            <div key={status.value} className='project-status'>
                                    <div className='project-status-title'>{status.label}</div>

                                    <div className='projects-grid'>
                                        {filteredProjects.map(project =>
                                            <ProjectListCard key={project.id} status={status} project={project}/>)}
                                    </div>
                                </div>;
                })}
            </div>
        </div>
    );
};

export default ProjectList;
