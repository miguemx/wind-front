
import React, { useCallback, useMemo, useContext } from 'react'
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import { paths } from "../../../services/routes/appRoutes";
import { faFileExcel, faPlus } from "@fortawesome/pro-light-svg-icons";
import { useHistory, useLocation } from "react-router-dom";
import useTideTable from "../../../components/utility/TideReactTable/useTideTable";
import TideReactTable from "../../../components/utility/TideReactTable/TideReactTable";
import { filterTypes } from "../../../services/searchUtils";
import {
    paymentQuoteStatus, paymentQuoteStatusTrans,
    quoteListSGroups,
    quoteListTableColumns,
    quoteStatus,
    quoteStatusTrans
} from "../../../services/modelUtils/quoteUtils";
import { ApiContext } from "../../../services/api/api-config";
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';
import _ from 'lodash';

const filtersConfig = {
    placeholder: 'Buscar una cotización...', filters: [
        { main: true, field: 'search' },
        { type: filterTypes.TEXT, field: 'title', label: 'Buscar por título' },
        { type: filterTypes.TEXT, field: 'folio', label: 'Buscar por folio' },
        { type: filterTypes.TEXT, field: 'client.name', label: 'Buscar por empresa' },
        { type: filterTypes.TEXT, field: 'project.name', label: 'Buscar por proyecto' },
        { type: filterTypes.SELECT, field: 'status', label: 'Buscar por estado', isMulti: true, options:[
            ..._.map(quoteStatus, (status)=>({value: status, label: quoteStatusTrans[status]}))
        ] },
        {type: filterTypes.ORDER, field: 'order[createdDate]', label:'Ordenar por creación'},
        {
            type: filterTypes.SELECT,
            field: 'paymentStatus',
            label: 'Buscar por estado de pago',
            isMulti: true,
            options: [
                ..._.map(paymentQuoteStatus, (status)=>({value: status, label: paymentQuoteStatusTrans[status]}))
            ]
        },
    ]
};

const QuoteList = () => {
    const api = useContext(ApiContext);

    const { search } = useLocation();
    const history = useHistory();

    const urlFilters = useUrlFilters();

    const projectId = (new URLSearchParams(search)).get('project') || '';
    const requestFilters = useMemo(() => ({sGroups: quoteListSGroups, project: projectId, 'order[createdDate]':"DESC" }), [projectId]);

    //Table config
    const table = useTideTable({
        entity: 'quotes',
        columns: quoteListTableColumns,
        requestFilters,
        ...urlFilters
    });

    const { tableProps, filters, setFilters } = table;

    useSearchHistory(table);

    const exportToExcel = useCallback(() => {
        window.location.href = api.quotes.getExportExcelUrl({ pagination: false, ...requestFilters, ...filters })
    }, [api, requestFilters, filters]);

    // config toolbar
    const tools = useMemo(() => [
        { icon: faPlus, callback: ()=>history.push(paths.quoteForm+`?project=${projectId}`), text: "Agregar nueva cotización", testId: 'add-client-button' },
        { icon: faFileExcel, callback: exportToExcel, text: "Descargar excel", testId: 'export-excel-button' },
    ], [history, projectId, exportToExcel]);

    return (
        <div className={"QuoteList wind-scene"}>

            <TopBar
                title="Cotizaciones"
                filters={filters}
                onFiltersChange={setFilters}
                filtersConfig={filtersConfig}
            />

            <ToolBar tools={tools} />

            <div className='center-container'>
                <TideReactTable {...tableProps} />
            </div>

        </div>
    )
}

export default QuoteList;
