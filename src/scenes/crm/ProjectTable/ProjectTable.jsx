import {
    faFileExcel,
    faFileInvoiceDollar,
    faLayerGroup,
    faMessageQuestion,
    faPlus
} from '@fortawesome/pro-light-svg-icons';
import moment from 'moment';
import React, {useMemo, useCallback, useContext} from 'react';
import {Link, useHistory} from 'react-router-dom';
import TideReactTable from '../../../components/utility/TideReactTable/TideReactTable';
import useTideTable from '../../../components/utility/TideReactTable/useTideTable';
import ToolBar from '../../../components/utility/ToolBar/ToolBar';
import TopBar from '../../../components/utility/TopBar/TopBar';
import {moneyFormatter} from '../../../services/currencyUtils';
import {
    projectTableSGroups,
    projectStatusOptionsForSelect,
    projectStatusTrans
} from '../../../services/modelUtils/projectUtils';
import {paths} from '../../../services/routes/appRoutes';
import {ApiContext} from "../../../services/api/api-config";
import {filterTypes} from "../../../services/searchUtils";
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';
import { SecurityContext } from '../../../services/SecurityManager';

const customProp = 'projectTable';
const filtersConfig = {
    placeholder: 'Buscar un proyecto...', filters: [
        {main: true, field: 'search'},
        {field: 'name', label:'Nombre del proyecto'},
        {field: 'client.name', label:'Cliente'},
        {type: filterTypes.ORDER, field: 'order[createdDate]', label:'Ordenar por creación'},
        {type: filterTypes.SELECT, field: 'status', label:'Estatus', isMulti: true, options:projectStatusOptionsForSelect},
        {field: 'folio', label:'Folio del proyecto'},
    ]
};

const ProjectTable = () => {
    const api = useContext(ApiContext);
    const securityManager = useContext(SecurityContext);
    const history = useHistory();

    const urlFilters = useUrlFilters();

    // ----- Loading project table
    const columns = useMemo(() => [
        {Header: "Folio", accessor: (p) => p.folio},
        {
            Header: "Nombre", id: 'name',
            accessor: (project)=>
            <Link to={paths.projectDetail.replace(':id', project.id)} data-tooltip={"Ir a proyectos"}>
                {project.name}
            </Link>
        },
        {Header: "Cliente", accessor: 'client.name'},
        {Header: "Estado", id: 'status', accessor: p=>projectStatusTrans[p.status]||p.status},
        {Header: "Cot. totales", accessor: 'projectStats.quotesCount'},
        {Header: "Cot. aprobadas", accessor: 'projectStats.approvedQuotesCount'},
        {
            Header: "Pendiente de pago",
            accessor: (p) => `$ ${moneyFormatter(Number(p.projectStats?.approvedQuotesSum) - Number(p.projectStats?.paymentsSum))}`
        },
        {
            Header: "Última actualización",
            accessor: (p) => (p.updatedDate ? moment(p.updatedDate).format('DD/MM/YYYY') : null)
        },
    ], []);

    const projectFilters = useMemo(() => ({sGroups: projectTableSGroups}), []);
    const projectOptions = useMemo(() => ({ customProp }), []);

    const table = useTideTable({
        entity: 'projects',
        columns,
        requestOptions: projectOptions,
        requestFilters: projectFilters,
        ...urlFilters
    });

    const { tableProps, filters, setFilters } = table;

    useSearchHistory(table);

    // ----- Link to add projects
    const handleAdd = useCallback(() => history.replace(paths.projectNew), [history]);
    const redirectToProjectsList = useCallback(() => history.replace(paths.projects), [history]);
    const exportToExcel = useCallback(() => {
        window.location.href = api.projects.getExportExcelUrl({pagination: false})
    }, [api]);

    const tools = useMemo(() => {
        let tools = [
            {icon: faPlus, callback: handleAdd, text: "Crear nuevo proyecto"},
            {icon: faLayerGroup, callback: redirectToProjectsList, text: "Vista de proyectos activos"},
            {icon: faFileInvoiceDollar, callback: ()=>history.push(paths.quoteForm), text: "Nueva cotización", id: 'newQuote',
                disabled: !securityManager.canAdminQuotes()},
            {icon: faMessageQuestion, callback: ()=>history.push(paths.quotationRequestNew), text: "Nueva solicitud",
                disabled: !securityManager.canAdminQuotationRequests()},
            {icon: faFileExcel, callback: exportToExcel, text: "Descargar excel"},
        ];

        return tools.filter(t => !t.disabled);
    }, [handleAdd, redirectToProjectsList, exportToExcel, history, securityManager]);

    return (
        <div className={"ProjectTable wind-scene"}>

            <TopBar
                title="Proyectos"
                filters={filters}
                onFiltersChange={setFilters}
                filtersConfig={filtersConfig}
            />

            <ToolBar tools={tools}/>

            <div className='center-container'>
                <TideReactTable {...tableProps}/>
            </div>
        </div>
    );
}

export default ProjectTable;
