import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import './ProjectDetail.scss';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {faTrashCan, faFileInvoiceDollar, faMessageQuestion, faCalendarLines, faPencil, faCommentPen, faShieldKeyhole, faHandHoldingUsd} from "@fortawesome/pro-light-svg-icons";
import UnderlinedTitle from "../../../components/layout/UnderlinedTitle/UnderlinedTitle";
import SimpleCard from "../../../components/layout/SimpleCard/SimpleCard";
import {paths} from "../../../services/routes/appRoutes";
import InfoPair from "../../../components/layout/InfoPair/InfoPair";
import QuoteTable from "../../../components/crmComponents/QuoteTable/QuoteTable";
import {ApiContext} from "../../../services/api/api-config";
import {Link, useHistory, useParams} from "react-router-dom";
import {projectDetailSGroups, projectStatusOptions, projectStatusTrans} from "../../../services/modelUtils/projectUtils";
import moment from "moment";
import { moneyFormatter } from '../../../services/currencyUtils';
import QuotationRequestTable from '../../../components/crmComponents/QuotationRequestTable/QuotationRequestTable';
import FileContainer from "../../../components/crmComponents/FileContainer/FileContainer";
import WindSelect from '../../../components/formComponents/WindSelect/WindSelect';
import { getNotifier } from '../../../services/notifier';
import useBoolean from '../../../hooks/useBoolean';
import AclPermissionsModal from "../../../components/crmComponents/AclPermissionsModal/AclPermissionsModal";
import MemorandumTable from '../../../components/crmComponents/MemorandumTable/MemorandumTable';
import Conversations from '../../../components/crmComponents/Conversations/Conversations';
import TransactionFormModal from '../../../components/formComponents/TransactionFormModal/TransactionFormModal';
import { SecurityContext } from '../../../services/SecurityManager';
import { quoteStatusForFilterInPayments } from '../../../services/modelUtils/quoteUtils';

const ProjectDetail = () => {

    const api = useContext(ApiContext);
    const securityManager = useContext(SecurityContext);
    const history = useHistory();
    const { id } = useParams();

    const [ showTransactionForm, transactionFormOn, transactionFormOff ] = useBoolean(false);
    const [showConversations, setShowConversations] = useState();

    // ----- Load project data
    const [project, setProject] = useState();
    const apiConfig = useMemo(()=>({ id, params:{ sGroups: projectDetailSGroups } }),
        [id]);
    useEffect(()=>{
        api.projects.get(apiConfig).then(setProject);
    },[api, apiConfig]);

    const handleRemove = useCallback(()=> {
        if(!project) return;

        api.projects.delete(apiConfig).then(()=> history.push(paths.projects));
    },[api, history, apiConfig, project]);

    const redirectToEditProject = useCallback(()=>history.push(paths.projectEdit.replace(':id', id)), [history, id]);

    // ----- Change project status -----
    const handleChangeStatus = useCallback((statusOption) => {
        const status = statusOption.value;

        api.projects.update({
            id,
            params: { status, sGroups: projectDetailSGroups },
        }).then((project) => {
            setProject(project);
            getNotifier().success('El estado del proyecto se actualizó correctamente 🎉');
        })
        .catch(() => { getNotifier().error('Hubo un error al actualizar el estado del proyecto'); }
        );
    }, [api, id]);

    // Acl Permissions Modal Config
    const [ aclProjectPermissionsModal, aclProjectPermissionsModalOn, aclProjectPermissionsModalOff ] = useBoolean(false);

    // Toolbar Config
    const tools = useMemo(()=>{
        const baseItems = [];

        if(securityManager.canAdminQuotes())
            baseItems.push({ icon: faFileInvoiceDollar, callback: ()=>history.push(paths.quoteForm+`?project=${id}`), text: "Nueva cotización", testId: "add-quote-button" });
        
        baseItems.push({ icon: faMessageQuestion, callback: ()=>history.push(paths.quotationRequestNew+`?project=${id}`), text: "Nueva solicitud", testId: "add-quotation-request-button" });
        
        if(securityManager.canAdminMemorandums())
            baseItems.push({ icon: faCalendarLines, callback: ()=>history.push(paths.memorandumNew+`?project=${id}`), text: "Nueva minuta", testId: "add-memorandum-button" });

        baseItems.push({ icon: faPencil, callback: redirectToEditProject, text: "Editar proyecto", testId: "edit-project-button" },);
        baseItems.push({ icon: faCommentPen, callback: ()=>setShowConversations(true), text: "Ver comentarios", testId: "see-comments-button"  },);
        baseItems.push({ icon: faShieldKeyhole, callback: aclProjectPermissionsModalOn, text: "Permisos de acceso", testId: "acl-project-permissions-button" },);
        
        if(securityManager.canAdminTransactions())
            baseItems.push({ icon: faHandHoldingUsd, callback: () => transactionFormOn(), text: "Agregar nuevo pago", testId: "project-detail-add-transaction-button" })
        
            baseItems.push({ icon: faTrashCan, callback: handleRemove, text: "Eliminar proyecto", testId: "delete-project-button" })

        return baseItems;
    },[history, id, redirectToEditProject, aclProjectPermissionsModalOn, transactionFormOn, handleRemove, securityManager]);

    const closeConversations = useCallback(()=>setShowConversations(false), [setShowConversations]);
    const textNoDate='<Sin fecha>';

    const approvedQuotes = project?.quotes?.filter(q => quoteStatusForFilterInPayments.find(s => s===q.status));

    return (
        <div className={"ProjectDetail wind-scene"}>

            <TopBar
                title="Proyecto"
                titleLinkBack
            />

            <ToolBar tools={tools} />

            <div className='center-container'>
                <div className='project-header' >
                    <UnderlinedTitle secondary>{project?.name}</UnderlinedTitle>
                    <div className='project-status-container'>
                        <div className='project-status-select'>
                            <WindSelect
                                label="Estado"
                                value={{ value: project?.status||'', label: projectStatusTrans[project?.status] }}
                                options={projectStatusOptions}
                                onChange={handleChangeStatus}
                                placeholder={projectStatusTrans[project?.status]}
                            />
                        </div>
                    </div>
                </div>

                {project && <>
                    <SimpleCard className='general-info' title="Información general">
                        <div className={"info-block"}>
                            <Link to={paths.clientDetail.replace(":id", project.client?.id)} data-tooltip={"Ver perfil del cliente"}>
                                <InfoPair value={project.client?.name} title={"Cliente"}/>
                            </Link>
                            <InfoPair value={project.folio} title={"Folio"}/>
                            <InfoPair value={`$ ${moneyFormatter(Number(project.projectStats?.approvedQuotesSum))}`} title={"Costo total"}/>
                            <InfoPair value={`$ ${moneyFormatter(Number(project.projectStats?.approvedQuotesSum - project.projectStats?.paymentsSum ))}`} title={"Pendiente"}/>
                            <InfoPair value={project.startDate? moment(project.startDate).format('DD/MM/YYYY'):
                                textNoDate} title={"Inicio de proyecto"}/>
                            <InfoPair value={project.updatedDate? moment(project.updatedDate).format('DD/MM/YYYY'):
                                textNoDate} title={"Última actualización"}/>
                            <InfoPair value={project.endDate? moment(project.endDate).format('DD/MM/YYYY'):
                                textNoDate} title={"Fin de proyecto"}/>
                            <InfoPair value={project.createdBy?.fullName || 'Sin información'} title={"Creado por:"}/>
                        </div>

                        <InfoPair
                            value={project.description}
                            title={"Descripción"}
                        />
                    </SimpleCard>

                    <SimpleCard title="Cotizaciones">
                        { securityManager.canAdminMemorandums() ? <QuoteTable project={project} /> : <>Sin acceso</> }
                    </SimpleCard>

                    <div className="two-cols">
                        <SimpleCard title="Solicitudes">
                            <QuotationRequestTable project={project} />
                        </SimpleCard>
                        <SimpleCard title="Minutas">
                            { securityManager.canAdminMemorandums() ? <MemorandumTable project={project} /> : <>Sin acceso</> }
                        </SimpleCard>
                    </div>

                    <SimpleCard title="Archivos">
                        <FileContainer fileContainer={project?.fileContainer} />
                    </SimpleCard>
                </>}

            </div>
            
            {showConversations &&
                <Conversations 
                    id={project?.conversation?.id} 
                    conversationTitle={project?.name} 
                    closeConversations={closeConversations}
                    clientId={project?.client?.id}
                />}

            { aclProjectPermissionsModal && project && (
                <AclPermissionsModal
                    entity='project'
                    entityObject={project}
                    onClose={aclProjectPermissionsModalOff}
                />
            )}

            { showTransactionForm && (
                <TransactionFormModal
                    client={project?.client}
                    quotes={approvedQuotes}
                    onClose={transactionFormOff}
                    project={project}
                />
            )}
        </div>
    );
};

export default ProjectDetail;
