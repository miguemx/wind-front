import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import './QuoteDetail.scss';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import UnderlinedTitle from "../../../components/layout/UnderlinedTitle/UnderlinedTitle";
import {getIvaAmount, getQuoteSubtotal, quoteDetailSGroups, quoteReviewDetails, quoteStatus, quoteStatusOptions, quoteStatusTrans} from "../../../services/modelUtils/quoteUtils";
import {ApiContext} from "../../../services/api/api-config";
import {Link, useHistory, useParams} from "react-router-dom";
import SimpleCard from "../../../components/layout/SimpleCard/SimpleCard";
import InfoPair from "../../../components/layout/InfoPair/InfoPair";
import moment from "moment";
import Switch from "../../../components/utility/Switch/Switch";
import {moneyFormatter} from "../../../services/currencyUtils";
import QuoteProduct from "./components/QuoteProduct/QuoteProduct";
import useBoolean from "../../../hooks/useBoolean";
import {
    faTrashCan,
    faPen,
    faFilePdf,
    faCheck,
    faHandHoldingUsd,
    faShieldKeyhole,
    faTimes, faClone
} from "@fortawesome/pro-light-svg-icons";
import Modal from "../../../components/utility/Modal/Modal";
import {paths} from "../../../services/routes/appRoutes";
import {getNotifier} from "../../../services/notifier";
import TransactionFormModal from '../../../components/formComponents/TransactionFormModal/TransactionFormModal';
import AclPermissionsModal from "../../../components/crmComponents/AclPermissionsModal/AclPermissionsModal";
import {displayDate} from "../../../services/jsUtils";
import WindSelect from '../../../components/formComponents/WindSelect/WindSelect';

const QuoteDetail = (factory, deps) => {

    const api = useContext(ApiContext);
    const history = useHistory();
    const { id } = useParams();

    const [ showTransactionForm, transactionFormOn, transactionFormOff ] = useBoolean(false);
    const [ showConfirmationIvaModal, confirmationIvaModalOn, confirmationIvaModalOff ] = useBoolean(false);

    // ----- Load quote from server -----
    const [quote, setQuote] = useState();
    useEffect(()=>{
        api.quotes.get({ id, params:{sGroups: quoteDetailSGroups}, customProp:'_' }).then(setQuote);
    },[api, id]);

     // ----- Change quote status -----
    const handleChangeStatus = useCallback((statusOption) => {
        const status = statusOption.value;

        api.quotes.update({
            id,
            params: { status, sGroups: quoteDetailSGroups },
        }).then((quote) => {
            setQuote(quote);
            getNotifier().success('El estado de la cotización se actualizó correctamente 🎉');
        })
        .catch(() => { getNotifier().error('Hubo un error al actualizar el estado de la cotización'); }
        );
    }, [api, id]);
    
    // ---- Delete quote -----
    const [isDeleting, startDeleting, stopDeleting] = useBoolean();
    const handleDelete = useCallback(()=>{
        api.quotes.delete({ id:quote.id })
            .then(()=>{
                stopDeleting();
                history.replace(paths.projectDetail.replace(':id', quote.project.id));
                getNotifier().success('La cotización ha sido eliminada 🗑');
            });
    },[api, history, quote, stopDeleting]);

    const handleEdit = useCallback(() => {
        history.push( paths.quoteEdit.replace(':id', id) );
    }, [history, id]);
    const handleClone = useCallback(() => {
        history.push( paths.quoteClone.replace(':id', id) );
    }, [history, id]);

    const downloadPdf = useCallback(()=>{
        if(quote)
            window.location.href = api.quotes.getDownloadPdfUrl(quote);
    }, [quote, api]);

    // ---- Approve quote -----
    const [isApproving, startApproving, stopApproving] = useBoolean();
    const approveQuote = useCallback(()=>{
        if (quote) {
            if(quote.status === quoteStatus.APPROVED) return getNotifier().warning('La cotización ya ha sido aprobada');

            api.quotes.update({ id:quote.id, params:{ status: quoteStatus.APPROVED, sGroups: quoteDetailSGroups } })
                .then((newQuote)=>{
                    getNotifier().success('La cotización ha sido aprobada 🎉');
                    setQuote(newQuote);
                    stopApproving();
                });
        }
    }, [api, quote, setQuote, stopApproving]);

    // ---- Reject quote -----
    const [isRejecting, startRejecting, stopRejecting] = useBoolean();
    const rejectQuote = useCallback(()=>{
        if (quote) {
            if(quote.status === quoteStatus.REJECTED) return getNotifier().warning('La cotización ya ha sido rechazada');

            api.quotes.update({ id:quote.id, params:{ status: quoteStatus.REJECTED, sGroups: quoteDetailSGroups } })
                .then((newQuote)=>{
                    getNotifier().success('La cotización ha sido rechazada 🎉');
                    setQuote(newQuote);
                    stopRejecting();
                });
        }
    }, [api, quote, setQuote, stopRejecting]);

    // Acl Permissions Modal Config
    const [ aclProjectPermissionsModal, aclProjectPermissionsModalOn, aclProjectPermissionsModalOff ] = useBoolean(false);

    // -- Toolbar --
    const tools = useMemo(() => [
        {icon: faCheck, callback: startApproving, text: 'Aprobar cotización', disabled: quote && quote.status === quoteStatus.APPROVED, testId: "approve-quote-button"},
        {icon: faTimes, callback: startRejecting, text: 'Rechazar cotización', disabled: quote && quote.status === quoteStatus.REJECTED, testId: "reject-quote-button"},
        {icon: faPen, callback: handleEdit, text: "Editar cotización", testId: "edit-quote-button"},
        {icon: faClone, callback: handleClone, text: "Clonar cotización", testId: "edit-quote-button"},
        {icon: faShieldKeyhole, callback: aclProjectPermissionsModalOn, text: "Permisos de acceso", testId: "acl-project-permissions-button" },
        {icon: faTrashCan, callback: startDeleting, text: "Eliminar cotización", testId: "delete-quote-button"},
        {icon: faHandHoldingUsd, callback: () => transactionFormOn(), text: "Agregar nuevo pago", testId: "quote-detail-add-transaction-button" },
        {icon: faFilePdf, callback: downloadPdf, text: "Descargar PDF", testId: "download-pdf-button"},
    ], [startDeleting, handleEdit, handleClone, downloadPdf, quote, startApproving, transactionFormOn, aclProjectPermissionsModalOn, startRejecting]);

    // -- toggle IVA
    const toggleIva = useCallback(() => {
        if (quote) {
            quote.iva = !quote.iva;
            
            const iva = quote.iva;
            const subtotal = getQuoteSubtotal(quote);
            const total = subtotal + getIvaAmount(quote, subtotal);

            api.quotes.update({ id:quote.id, params:{ iva, subtotal: subtotal.toString(), total: total.toString(), sGroups: quoteDetailSGroups } })
                .then((newQuote)=>{
                    getNotifier().success('El IVA se actualizó correctamente 🎉');
                    setQuote(newQuote);
                    confirmationIvaModalOff();
                }
            );
        }
    }, [api, quote, setQuote, confirmationIvaModalOff]);

    return (
        <div className={"QuoteDetail wind-scene"}>

            <TopBar title="Cotización" titleLinkBack/>
            <ToolBar tools={tools} />

            <div className='center-container'>

                <div className='quote-header' >
                    <UnderlinedTitle secondary>{quote?.title}</UnderlinedTitle>
                    <div className='quote-status-container'>
                  
                            <div className="quote-status-select">
                                <WindSelect
                                    label="Estado"
                                    value={{value: quote?.status||'', label: quoteStatusTrans[quote?.status]}}
                                    options={quoteStatusOptions}
                                    onChange={handleChangeStatus}
                                    placeholder= {quoteStatusTrans[quote?.status]}
                                />
                            </div>
          
                    </div>
                </div>

                <SimpleCard title='Información general' className='main-info'>
                    <div className='info-blocks'>
                        <Link to={paths.clientDetail.replace(":id", quote?.project?.client?.id)} data-tooltip={"Ver perfil del cliente"}>
                            <InfoPair value={quote?.project?.client?.name} title='Cliente' />
                        </Link>
                        <Link to={paths.projectDetail.replace(":id", quote?.project?.id)} data-tooltip={"Ver detalle del proyecto"}>
                            <InfoPair value={quote?.project?.name} title='Proyecto' />
                        </Link>
                        <InfoPair value={quote?.folio} title='Cotización' />
                        <InfoPair value={quote?.quotationRequest?.folio||'NA'} title='Solicitud' />
                        <InfoPair value={quote?.expirationDate?displayDate(quote?.expirationDate):'NA'} title='Expiración' />
                        <InfoPair value={quote?.createdBy?.fullName||'Sin información'} title='Creado por' />
                        <InfoPair value={<Switch value={quote?.iva} onChange={confirmationIvaModalOn} />} title='IVA' />
                    </div>
                    <br />
                    {quoteReviewDetails[quote?.status] &&
                        <div className='info-blocks'>
                            <InfoPair value={quote?.reviewedBy?.fullName} title={quoteReviewDetails[quote?.status].title} />
                            <InfoPair value={quote?.reviewedDate && moment(quote.reviewedDate).format('DD/MM/YYYY')} title={quoteReviewDetails[quote?.status].dateTitle} />
                            <InfoPair value={quote?.expirationDate && moment(quote.expirationDate).format('DD/MM/YYYY')} title='Expiración' />
                            <div />
                            <div />
                        </div>
                    }
                </SimpleCard>

                <SimpleCard className='total-info'>
                    <div className='amount-pair'>
                        <p className='label'>Subtotal</p>
                        <p className='value'>$ {moneyFormatter(quote?.subtotal)}</p>
                    </div>
                    <div className='amount-pair'>
                        <p className='label'>Total</p>
                        <p className='value'>$ {moneyFormatter(quote?.total)}</p>
                    </div>
                </SimpleCard>

                <SimpleCard title='Productos'>
                    {quote?.quoteProducts?.map( product=> <QuoteProduct key={product.id} quoteProduct={product}/> )}
                </SimpleCard>
            </div>

            {/* ----- Deleting Modal ----- */}
            {isDeleting && <Modal
                title='Eliminar cotización'
                mainButtonAction={handleDelete}
                mainButtonColor={'danger'}
                mainButtonText={'Eliminar'}
                secondaryButtonAction={stopDeleting}
                onClose={stopDeleting}
            >
                <p>¿ Estás seguro que quieres eliminar la cotización <strong>{quote.title} - {quote.folio}</strong> ?</p>
            </Modal>}

            {/* ----- Approving Modal ----- */}
            {isApproving && <Modal
                title='Aprobar cotización'
                mainButtonAction={approveQuote}
                mainButtonColor={'success'}
                mainButtonText={'Aprobar'}
                secondaryButtonAction={stopApproving}
                onClose={stopApproving}
            >
                <p>¿ Estás seguro que quieres aprobar la cotización <strong>{quote.title} - {quote.folio}</strong> ?</p>
            </Modal>}

            {/* ----- Rejecting Modal ----- */}
            {isRejecting && <Modal
                title='Rechazar cotización'
                mainButtonAction={rejectQuote}
                mainButtonColor={'success'}
                mainButtonText={'Rechazar'}
                secondaryButtonAction={stopRejecting}
                onClose={stopRejecting}
            >
                <p>¿ Estás seguro que quieres rechazar la cotización <strong>{quote.title} - {quote.folio}</strong> ?</p>
            </Modal>}

            {/* ----- Transaction Form ----- */}
            {showTransactionForm && (
                <TransactionFormModal
                    onClose={transactionFormOff}
                    quotes={[quote]}
                    client={quote?.project?.client}
                />
            )}


            { aclProjectPermissionsModal && quote && (
                <AclPermissionsModal
                    entity='quote'
                    entityObject={quote}
                    onClose={aclProjectPermissionsModalOff}
                />
            )}

            {/* ----- Show IVA Modal ----- */}
            { showConfirmationIvaModal && (
                <Modal
                    title='IVA'
                    mainButtonAction={toggleIva}
                    mainButtonColor={'success'}
                    mainButtonText={'Guardar'}
                    secondaryButtonAction={confirmationIvaModalOff}
                    onClose={confirmationIvaModalOff}
                >
                    <p>¿ Estás seguro que quieres <strong>{ quote.iva ? 'quitar' : 'agregar' }</strong> el IVA de la cotización <strong>{quote.title} - {quote.folio}</strong> ?</p>
                </Modal>
            )}
        </div>
    );
};

export default QuoteDetail;
