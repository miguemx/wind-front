import React from 'react';
import './QuoteProduct.scss';
import InfoPair from "../../../../../components/layout/InfoPair/InfoPair";
import {moneyFormatter} from "../../../../../services/currencyUtils";
import WindSwitch from '../../../../../components/formComponents/WindSwitch/WindSwitch';
import RichTextEditor from '../../../../../components/formComponents/RichTextEditor/RichTextEditor';

const QuoteProduct = ({ quoteProduct }) => {
    return (
        <div className={"QuoteProduct"}>
            <div className='product-header'>
                <InfoPair title={'Nombre del producto'} value={quoteProduct.name} />
                {quoteProduct.quantity > 1 &&
                <InfoPair title={'Cantidad'} value={quoteProduct.quantity} />}
                <InfoPair title={'Precio'} value={'$ '+moneyFormatter(quoteProduct.price)} />
                {quoteProduct.quantity > 1 &&
                <InfoPair title={'Total'} value={'$ '+moneyFormatter(quoteProduct.quantity*quoteProduct.price)} />}
                <WindSwitch
                    className='project-field'
                    placeholder={'Opcional'}
                    value={quoteProduct?.isOptional}
                    onChange={()=>{}}
                />
            </div>

            <RichTextEditor
                html={quoteProduct?.description}
                readonly={true}
            />

            <hr className="rounded"></hr>
        </div>
    );
};

export default QuoteProduct;
