import React, { useCallback, useContext, useMemo } from 'react'
import { useHistory, useParams } from 'react-router-dom';
import ToolBar from '../../../components/utility/ToolBar/ToolBar';
import TopBar from '../../../components/utility/TopBar/TopBar';
import useFormState from '../../../hooks/useFormState';
import { ApiContext } from '../../../services/api/api-config';
import { faSave, faTrashCan} from '@fortawesome/pro-light-svg-icons';
import { useSelector } from 'react-redux';
import './FiscalDataForm.scss';
import WindInput from '../../../components/formComponents/WindInput/WindInput';
import SimpleCard from '../../../components/layout/SimpleCard/SimpleCard';
import { convertFiscalDataToForm, getEmptyFiscalData, fiscalDataSGroups, prepareFiscalDataForServer } from '../../../services/modelUtils/fiscalDataUtils';
import { getNotifier } from '../../../services/notifier';
import { useEffect } from 'react';
import {paths} from "../../../services/routes/appRoutes";
import useBoolean from '../../../hooks/useBoolean';
import Modal from '../../../components/utility/Modal/Modal';

const FiscalDataForm = () => {
    const api = useContext(ApiContext);
    const { form, setForm, bindSimple } = useFormState(() => getEmptyFiscalData() );
    const { id } = useParams();
    const history = useHistory();

    // ------ Load data ------
    useEffect(() => {
        if (id)
            api.fiscalDatas.get({ id, params:{sGroups: fiscalDataSGroups}, customProp:'_' })
                .then(fiscalData => setForm(convertFiscalDataToForm(fiscalData)));
    }, [id, api, setForm]);

    // ------ Save to server ------
    const loadingId = 'InvoiceForm.create.p'+id;
    const loading = useSelector(s=>!!s.loadingIds[loadingId]);

    const handleSave = useCallback(()=>{
        const method = id ? 'update' : 'create';
        let fiscalData = null;
        
        try {
            fiscalData = prepareFiscalDataForServer(form);
        } catch (err) {
            return getNotifier().error(err.message||err.detail);
        }
        const params = {
            id,
            params: fiscalData
        };

        api.fiscalDatas[method](params).then(() => {
            getNotifier().success('Se ha guardado correctamente');
            history.push( paths.fiscales );
        });
    },[form, api, history, id]);

    // ---- Delete fiscal data -----
    const [isDeleting, startDeleting, stopDeleting] = useBoolean();
    const handleDelete = useCallback(()=>{
        api.fiscalDatas.delete({ id: form.id })
            .then(()=>{
                stopDeleting();
                history.replace(paths.fiscales);
                getNotifier().success('Los datos fiscales han sido eliminados 🗑');
            });
    },[api, history, form, stopDeleting]);

    // ------ Toolbar definition ------
    const tools = useMemo(()=>[
        { icon: faSave, callback: handleSave, text: "Guardar datos fiscales", disabled: loading  },
        { icon: faTrashCan, callback: startDeleting, text: "Eliminar datos fiscales", testId: "delete-fiscaldata-button"},
    ],[handleSave, loading, startDeleting]);

    return (
        <div className={"FiscalDataForm wind-scene"}>
            <TopBar
                title={ id ? "Editar Datos Fiscales" : "Nuevos Datos Fiscales" }
                titleLinkBack
            />

            <ToolBar tools={tools} />

            <SimpleCard className='general-info' title="Información general">
                <div className='fields'>

                    <WindInput
                        className='project-field'
                        placeholder={"RFC*"}
                        type={'text'}
                        {...bindSimple('rfc')}
                    />

                    <WindInput
                        className='project-field'
                        placeholder={"Nombre o Razón social*"}
                        type={'text'}
                        {...bindSimple('businessName')}
                    />

                </div>
                <div className='fields'>
                    <WindInput
                        className='project-field'
                        placeholder={"EMail*"}
                        type={'text'}
                        {...bindSimple('email')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"Nombre comercial"}
                        type={'text'}
                        {...bindSimple('comercialName')}
                    />
                </div>
            </SimpleCard>

            <SimpleCard className='general-info' title="Domicilio fiscal">
                <div className='fields'>
                    <WindInput
                        className='project-field'
                        placeholder={"Calle"}
                        type={'text'}
                        {...bindSimple('street')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"Núm. ext."}
                        type={'text'}
                        {...bindSimple('externalNumber')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"Núm. int."}
                        type={'text'}
                        {...bindSimple('internalNumber')}
                    />
                    
                </div>
                <div className='fields'>
                    <WindInput
                        className='project-field'
                        placeholder={"Colonia"}
                        type={'text'}
                        {...bindSimple('neighborhood')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"Ciudad"}
                        type={'text'}
                        {...bindSimple('city')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"C.P.*"}
                        type={'text'}
                        {...bindSimple('zip')}
                    />
                </div>
                <div className='fields'>
                    <WindInput
                        className='project-field'
                        placeholder={"Municipio"}
                        type={'text'}
                        {...bindSimple('delegation')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"Estado"}
                        type={'text'}
                        {...bindSimple('state')}
                    />
                    <WindInput
                        className='project-field'
                        placeholder={"País"}
                        type={'text'}
                        {...bindSimple('country')}
                    />
                </div>
            </SimpleCard>
            
            {/* ----- Deleting Modal ----- */}
            {isDeleting && <Modal
                title='Eliminar datos fiscales'
                mainButtonAction={handleDelete}
                mainButtonColor={'danger'}
                mainButtonText={'Eliminar'}
                secondaryButtonAction={stopDeleting}
                onClose={stopDeleting}
            >
                <p>¿ Estás seguro que quieres eliminar las datos fiscales de <strong>{form.rfc} - {form.businessName}</strong> ?</p>
            </Modal>}
        </div>
    )
}

export default FiscalDataForm