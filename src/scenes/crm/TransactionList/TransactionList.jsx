import React, {useMemo, useContext, useCallback, useState} from 'react';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {faPlus, faFileExport} from "@fortawesome/pro-light-svg-icons";
import useTideTable from "../../../components/utility/TideReactTable/useTideTable";
import TideReactTable from "../../../components/utility/TideReactTable/TideReactTable";
import {transactionsTableSGroups, transactionsTableColumns} from "../../../services/modelUtils/transactionUtils";
import {filterTypes} from "../../../services/searchUtils";
import {ApiContext} from "../../../services/api/api-config";
import './TransactionList.scss';
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';
import TransactionFormModal from '../../../components/crmComponents/TransactionFormModal/TransactionFormModal';

const requestFilters={sGroups: transactionsTableSGroups};
const filtersConfig = {
    placeholder: 'Buscar una transacción...', filters: [
        {main: true, field: 'concept'},
        {type: filterTypes.TEXT, field: 'concept', label:'Buscar por concepto'},
        {type: filterTypes.TEXT, field: 'bankAccount.name', label:'Buscar por cuenta'},
        {type: filterTypes.TEXT, field: 'invoice.folio', label:'Buscar por factura'},
        {type: filterTypes.ORDER, field: 'order[amount]', label:'Ordenar por monto'},
    ]
};

const TransactionList = () => {
  const urlFilters = useUrlFilters();
  const [showModalForm, setShowModalForm] = useState();
  const api = useContext(ApiContext);

  //Table config
  const table = useTideTable({
    entity: 'transactions',
    columns: transactionsTableColumns,
    requestFilters,
    ...urlFilters
  });

  const { tableProps, filters, setFilters } = table;

  useSearchHistory(table);

  const exportToExcel = useCallback(() => {window.location.href = api.transactions.getExportExcelUrl({pagination: false, ...requestFilters})}, [api]);

  const tools = useMemo(() => [
    { icon: faPlus, callback: ()=>setShowModalForm(true), text: "Agregar nueva transacción", testId: 'add-transaction-button' },
    { icon: faFileExport, callback: exportToExcel, text: "Exportar transacciones", testId: 'export-transactions-xls-button' },
  ], [exportToExcel]);

  const onClose = useCallback(() => {
    table.reload();
    setShowModalForm(false);
  }, [table]);

  return (
    <div className={"TransactionList wind-scene"}>

      <TopBar
        title="Transacciones"
        filters={filters}
        onFiltersChange={setFilters}
        filtersConfig={filtersConfig}
      />

      <ToolBar tools={tools} />

      <div className='center-container'>
        <TideReactTable {...tableProps} className="table-align-left" />
      </div>

      {showModalForm && <TransactionFormModal onClose={onClose} />}

    </div>
  );
}

export default TransactionList;