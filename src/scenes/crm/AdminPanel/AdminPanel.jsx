import React from 'react'
import TopBar from '../../../components/utility/TopBar/TopBar';
import './AdminPanel.scss';
import TenantRfcForm from './components/tenantrfcs/TenantRfcForm';

const AdminPanel = () => {

    return (
        <div className={"AdminPanel wind-scene"}>
            <TopBar
                title={ "Administración" }
                titleLinkBack
            />

            <TenantRfcForm />
            
        </div>
    )
}

export default AdminPanel;