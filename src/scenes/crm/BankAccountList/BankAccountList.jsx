import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {faPlus} from "@fortawesome/pro-light-svg-icons";
import {faFileExport} from "@fortawesome/pro-light-svg-icons";
import {bankAccountListSGroups} from "../../../services/modelUtils/bankAccountUtils";
import {filterTypes} from "../../../services/searchUtils";
import BankAccountCard from "./components/BankAccountCard/BankAccountCard";
import './BankAccountList.scss'
import {ApiContext} from "../../../services/api/api-config";
import _ from "lodash"
import BankAccountFormModal from "../BankAccountFormModal/BankAccountFormModal";
import useBoolean from "../../../hooks/useBoolean";

const requestFilters = {sGroups: bankAccountListSGroups};
const filtersConfig = {
    placeholder: 'Buscar una cuenta...', filters: [
        {main: true, field: 'name'},
        {type: filterTypes.TEXT, field: 'name', label: 'Buscar por nombre'},
        {type: filterTypes.TEXT, field: 'number', label: 'Buscar por número de cuenta'}
    ]
};

const BankAccountList = () => {

    const api = useContext(ApiContext);
    const [bankAccounts, setBankAccounts] = useState([]);
    const [filters, setFilters] = useState([])
    const [showingForm, showForm, hideForm] = useBoolean(false);
    const [ accountEdit, setAccountEdit ] = useState(0);

    const loadBankAccounts = useMemo(() => _.debounce((filters) => {
        api.bankAccounts.get({params: {...requestFilters, ...filters}, pagination: false}).then((data) => {
            setBankAccounts(data);
        })
    }, 650), [api]);

    const exportBankAccount = useCallback(() => {
        alert("todo: Export bank account");
    }, []);

    useEffect(() => {
        loadBankAccounts(filters)
    }, [filters, loadBankAccounts]);


    const tools = useMemo(() => [
        {icon: faPlus, callback: showForm, text: "Agregar nueva cuenta", testId: 'add-bank-account-button'},
        {
            icon: faFileExport,
            callback: exportBankAccount,
            text: "Exportar cuentas",
            testId: 'export-bank-account-button'
        },
    ], [showForm, exportBankAccount]);

    const editAccount = (idAccount) => {
        setAccountEdit( idAccount );
    }

    const closeModalAccount = () => {
        hideForm();
        setAccountEdit( 0 );
    }

    return (
        <div className={"BankAccountList wind-scene"}>
            <TopBar
                title="Cuentas"
                filters={filters}
                onFiltersChange={setFilters}
                filtersConfig={filtersConfig}
            />

            <ToolBar tools={tools}/>

            <div className='center-container'>
                <div className='bank-accounts-grid'>
                    {bankAccounts.map(bankAccount =>
                        <BankAccountCard 
                            key={bankAccount.id} 
                            bankAccount={bankAccount}
                            showEdit={editAccount}
                            />
                    )}
                </div>
            </div>

            { (showingForm || !!accountEdit) &&
                <BankAccountFormModal
                    onClose={closeModalAccount}
                    onBankAccountSaved={loadBankAccounts}
                    accountId={accountEdit}
                />
            }
        </div>
    );
};

export default BankAccountList;
