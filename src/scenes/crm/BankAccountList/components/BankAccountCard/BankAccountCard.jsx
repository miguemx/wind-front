import React from 'react';
import './BankAccountCard.scss';
import {moneyFormatter} from "../../../../../services/currencyUtils";
import {paths} from "../../../../../services/routes/appRoutes";
import {Link} from "react-router-dom";
import { faGear } from "@fortawesome/pro-light-svg-icons";
import IconButton from '../../../../../components/utility/IconButton/IconButton';

const BankAccountCard = ({bankAccount, showEdit}) => {

    return (
        <div className={"BankAccountCard"}>
            <div className='content data-area'>
                <Link to={paths.bankAccountDetail.replace(":id", bankAccount.id)} data-tooltip={"Ir a detalles de cuenta"} className={"account-link"}>
                    <h2 className='name UnderlinedTitle secondary'>
                        <span className='title-content'>{bankAccount?.bank?.name}</span>
                    </h2>
                    <h3 className={"account-title"}>{bankAccount?.name}</h3>
                </Link>
                <p className='balance'>$ {moneyFormatter(bankAccount?.balance)} MXN</p>
            </div>

            <div>
                <IconButton
                    icon={faGear}
                    color={ 'black' }
                    onClick={ () => { showEdit(bankAccount.id) } }
                />
            </div>
        </div>
    );
};

export default BankAccountCard;
