import React, { useCallback, useMemo, useContext } from 'react';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {paths} from "../../../services/routes/appRoutes";
import {faFileExcel, faPlus} from "@fortawesome/pro-light-svg-icons";
import {useHistory} from "react-router-dom";
import useTideTable from "../../../components/utility/TideReactTable/useTideTable";
import TideReactTable from "../../../components/utility/TideReactTable/TideReactTable";
import {filterTypes} from "../../../services/searchUtils";
import {ApiContext} from "../../../services/api/api-config";
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';
import { taxDocumentsTableColumns, taxDocumentTableListSGroups } from '../../../services/modelUtils/taxDocumentUtils';

const requestFilters={sGroups: taxDocumentTableListSGroups};
const filtersConfig = {
    placeholder: 'Buscar un minuta...', filters: [
        {main: true, field: 'title'},
        {type: filterTypes.TEXT, field: 'title', label:'Buscar por título'},
        {type: filterTypes.TEXT, field: 'project.name', label:'Buscar por proyecto'},
        {type: filterTypes.TEXT, field: 'client.name', label:'Buscar por cliente'},
    ]
};

const InvoicesList = () => {
    const urlFilters = useUrlFilters();
    
    const api = useContext(ApiContext);
    const history = useHistory();

    //Table config
    const table = useTideTable({
        entity: 'taxDocuments',
        columns: taxDocumentsTableColumns,
        requestFilters,
        ...urlFilters
    });

    const { tableProps, filters, setFilters } = table;

    useSearchHistory(table);

    const exportToExcel = useCallback(() => {window.location.href = api.taxDocuments.getExportExcelUrl({pagination: false, ...requestFilters})}, [api]);
    const handleAdd = useCallback(()=>history.push(paths.invoicesNew), [history]);

    const tools = useMemo(()=>[
        { icon: faPlus, callback: handleAdd, text: "Agregar factura",  testId: 'add-invoices-button' },
        {icon: faFileExcel, callback: exportToExcel, text: "Descargar Excel", testId: 'export-excel-button'},
    ],[handleAdd, exportToExcel]);

    return (
        <div className={"invoicesList wind-scene"}>

            <TopBar
                title="Facturas"
                filters={filters}
                onFiltersChange={setFilters}
                filtersConfig={filtersConfig}
            />

            <ToolBar tools={tools} />

            <div className='center-container'>
                <TideReactTable {...tableProps} />
            </div>
        </div>
    );
}

export default InvoicesList;