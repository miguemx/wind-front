import React, {useState, useCallback, useContext, useEffect} from 'react';
import './BankAccountFormModal.scss'
import Modal from "../../../components/utility/Modal/Modal";
import WindInput from "../../../components/formComponents/WindInput/WindInput";
import TideEntitySelect from "../../../components/utility/TideEntitySelect/TideEntitySelect";
import {ApiContext} from "../../../services/api/api-config";
import useFormState from "../../../hooks/useFormState";
import {getNotifier} from "../../../services/notifier";
import {prepareBankAccountForServer, getEmptyBankAccount, convertBankAccountToForm} from "../../../services/modelUtils/bankAccountUtils";
import Button from '../../../components/utility/Button/Button';
import useScript from '../../../hooks/useScript';

const BankAccountFormModal = ({onClose, onBankAccountSaved, accountId}) => {
    useScript('https://cdn.belvo.io/belvo-widget-1-stable.js');
    const api = useContext(ApiContext);
    const {form, setForm, bindSimple} = useFormState( () => getEmptyBankAccount() );
    const [message, setMessage] = useState();

    const close = useCallback(() => {
        if (onClose)
            onClose()
    }, [onClose]);

    // ------ Load data ------
    useEffect(() => {
        if (accountId) {
            let id = accountId+'';
            api.bankAccounts.get({ id })
                .then( accounts => setForm( convertBankAccountToForm(accounts) ) );
        }
    }, [accountId, api, setForm]);

    const saveBankAccount = useCallback(() => {
        let transaction;

        try {
            transaction = prepareBankAccountForServer({
                ...form
            });
        } catch (e) {
            return getNotifier().warning(e.message);
        }
        if ( accountId === 0 ) {
            api.bankAccounts.create({params: transaction})
            .then(() => {
                getNotifier().success('La cuenta ha sido creada correctamente');
                if(onBankAccountSaved){
                    onBankAccountSaved();
                }
                if(onClose){
                    onClose();
                }
            });
        }
        else {
            getNotifier().error('La cuenta no puede ser guardada');
        }
    }, [form, api, onClose, onBankAccountSaved, accountId]);

    const saveLinkBelvo = useCallback((link, institution) => {
        let parameters = {
            clabe: form.clabe,
            belvoToken: link,
            belvoInstitution: institution
        };
        
        let id = accountId+'';
        api.bankAccounts.update({id, params: parameters})
            .then(() => {
                getNotifier().success('La cuenta se ha conectado correctamente');
                onClose();
            });
    }, [api,onClose, accountId, form]);

    const onExit = useCallback((data) => {
        setMessage("Saliste de la aplicación, para conectar da click en el botón:");
    }, []);

    const onEvent = useCallback((event) => {
        
    }, []);
    
    const showConnect = useCallback((token) => {
        // config options: 
        // https://developers.belvo.com/docs/widget-startup-configuration
        // for default generated links are recurring
        const config = {
            callback: (link, institution) => saveLinkBelvo(link, institution),
            onExit: (data) => onExit(data),
            onEvent: (data) => onEvent(data),
            resources: ["ACCOUNTS", "OWNERS", "TRANSACTIONS"],
            external_id: `WIND-${accountId}`  // local user id -> 
        }
        window.belvoSDK.createWidget(token.access, config).build();
    }, [saveLinkBelvo, onEvent, onExit, accountId]);

    const desconectarBelvo = useCallback((link, institution) => {
        let parameters = {
            clabe: form.clabe,
            belvoToken: null,
            belvoInstitution: null
        };
        
        let id = accountId+'';
        api.bankAccounts.update({id, params: parameters})
            .then(() => {
                getNotifier().success('La cuenta se ha desconectado correctamente');
                onClose();
            });
    }, [api,onClose, accountId, form]);
    
    return (
        <Modal className={'BankAccountFormModal'}
               onClose={close}
               secondaryButtonAction={close}
               mainButtonAction={ saveBankAccount }
               mainButtonText="Guardar"
        >
            <div className='BankAccountForm'>
                <form onSubmit={(e)=>{e.preventDefault()}}>
                    <div className='form-container'>
                        <h2 className="UnderlinedTitle modal-title secondary">
                            <span className="title-content">Agregar nueva cuenta</span>
                        </h2>

                        <div className='form-row mt-20'>
                            <WindInput
                                placeholder="Agrega un nombre a la cuenta"
                                {...bindSimple('name')}
                            />
                            {
                                !!accountId ?
                                <TideEntitySelect
                                    className='project-field'
                                    entity='banks'
                                    placeholder={"Selecciona un banco"}
                                    autoSelect={false}
                                    {...bindSimple("bank")}
                                />
                                
                                :
                                <TideEntitySelect
                                    className='project-field'
                                    entity='banks'
                                    placeholder={"Selecciona un banco"}
                                    autoSelect={true}
                                    {...bindSimple('bank')}
                                />
                            }
                            
                        </div>
                        <div className='form-row mt-20'>
                            <WindInput
                                placeholder="Agrega el número de cuenta la cuenta "
                                {...bindSimple('number')}
                            />
                            <WindInput
                                placeholder="Agrega cuenta clabe de la cuenta "
                                {...bindSimple('clabe')}
                            />
                        </div>
                        <div>{message}</div>
                        {   (!!accountId && !form.belvoToken) &&
                            <div className='form-row mt-20'>
                                <Button color={"info"} onClick={() => api.belvo.generateToken().then(showConnect)}>Conectar con Belvo</Button>
                            </div>
                        }

                        {   (!!form.belvoToken) &&
                            <div className='form-row mt-20'>
                                <Button color={"danger"} onClick={ () => desconectarBelvo() }>Desconectar de Belvo</Button>
                            </div>
                        }
                        
                    </div>
                </form>
                <div id="belvo" />
            </div>
        </Modal>
    );
};

export default BankAccountFormModal;
