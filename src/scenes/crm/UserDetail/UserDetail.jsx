import { faFileImport, faPencil, faPlus } from '@fortawesome/pro-light-svg-icons';
import React, { useContext, useMemo, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import DetailHeader from '../../../components/utility/DetailHeader/DetailHeader';
import ToolBar from '../../../components/utility/ToolBar/ToolBar';
import TopBar from '../../../components/utility/TopBar/TopBar';
import { ApiContext } from '../../../services/api/api-config';
import { usersDetailSGroups } from '../../../services/modelUtils/userUtils';
import { getNotifier } from '../../../services/notifier';
import { paths } from '../../../services/routes/appRoutes';
import UserMainDetails from './components/UserMainDetails/UserMainDetails';

const UserDetail = () => {
  const api = useContext(ApiContext);
  const history = useHistory();

  const { id } = useParams();

  // ----- Load User data
  const customProp = 'UserDetail.get.'+id;

  useEffect(()=>{
    api.users.get({ id, customProp, params: { sGroups: usersDetailSGroups } })
      .catch(() => getNotifier().error('No se encontró el usuario solicitado'));
  },[api, id, customProp]);

  const user = useSelector(s=>s.api[customProp])||[];

  const handleAddUser = useCallback(() => history.push(paths.usersNew), [history]);
  const handleImport = useCallback(() => getNotifier().warning("Implementación pendiente"), []);

  const tools = useMemo(() => [
    { icon: faPlus, callback: handleAddUser, text: 'Nuevo usuario' },
    { icon: faFileImport, callback: handleImport, text: 'Descargar reporte' },
    { icon: faPencil, callback: () => history.push(paths.usersEdit.replace(':id', id)), text: 'Editar usuario' },
  ], [handleAddUser, handleImport, history, id]);

  return (
    <div className={"UserDetail wind-scene"}>

      <TopBar
        title="Usuario"
        titleLinkBack
      />

      <ToolBar
        tools={tools}
      />

      <div className='center-container'>
        <DetailHeader title={`Perfil de ${user?.fullName||'usuario'}`} />

        <div className="general-content">
          <UserMainDetails user={user} />
        </div>
      </div>
    </div>
  );
}

export default UserDetail;
