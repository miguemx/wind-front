import React from 'react';
import SimpleCardTwoColumns from '../../../../../components/layout/SimpleCardTwoColumns/SimpleCardTwoColumns';
import UserAvatar from '../UserAvatar/UserAvatar';
import './UserInfo.scss';

const UserInfo = ({ user }) => {

  const Avatar = <UserAvatar user={user} className="avatar" />;

  return (
    <SimpleCardTwoColumns className="UserInfo" title="Información general" child2={Avatar}>
      <div className="general-content__row">
        <div className="general-content__row-info">
          <p className="general-content__row-info-value">{user?.fullName||"-"}</p>
          <p className="general-content__row-info-label">Nombre</p>
        </div>

        <div className="general-content__row-info">
          <p className="general-content__row-info-value">{user?.client?.name||"-"}</p>
          <p className="general-content__row-info-label">Empresa</p>
        </div>

        <div className="general-content__row-info">
          <p className="general-content__row-info-value">{user?.username||"-"}</p>
          <p className="general-content__row-info-label">Usuario</p>
        </div>

        <div className="general-content__row-info">
          <p className="general-content__row-info-value">{user?.email||"-"}</p>
          <p className="general-content__row-info-label">Email</p>
        </div>

        <div className="general-content__row-info">
          <p className="general-content__row-info-value">{user?.phone||"-"}</p>
          <p className="general-content__row-info-label">Teléfono</p>
        </div>
      </div>
    </SimpleCardTwoColumns>
  );
}
 
export default UserInfo;