import React, {useEffect, useCallback, useContext, useState} from 'react'
import { useSelector } from 'react-redux';
import Button from '../../../../../components/utility/Button/Button';
import { ApiContext } from '../../../../../services/api/api-config';

const Connect = ({setLinkId}) => {
    useScript('https://cdn.belvo.io/belvo-widget-1-stable.js');
    const api = useContext(ApiContext);
    const me=useSelector(({api})=>api.me);
    const [message, setMessage] = useState();

    const onSuccess = useCallback((link, institution) => {
        localStorage.setItem("belvo_link", link);
        setLinkId(link);
    }, [setLinkId]);

    const onExit = useCallback((data) => {
        setMessage("Saliste de la aplicación, para conectar da click en el botón:");
    }, []);

    const onEvent = useCallback((event) => {
        //console.log("event", event);
    }, []);

    const showConnect = useCallback((token) => {
        // config options: 
        // https://developers.belvo.com/docs/widget-startup-configuration
        // for default generated links are recurring
        const config = {
            callback: (link, institution) => onSuccess(link, institution),
            onExit: (data) => onExit(data),
            onEvent: (data) => onEvent(data),
            resources: ["ACCOUNTS", "OWNERS", "TRANSACTIONS"],
            external_id: `WIND-${me.id}`  // local user id -> 
        }
        window.belvoSDK.createWidget(token.access, config).build();
    }, [onSuccess, me, onEvent, onExit]);

    useEffect(() => {
        const linkId = localStorage.getItem("belvo_link");

        if(linkId && typeof(linkId)!="undefined"){
            setLinkId(linkId);
        } else {
            api.belvo.generateToken().then(showConnect);
        }
    }, [api, showConnect, setLinkId]);

    return (
        <>
            <div id="belvo" />
            <div>{message &&
                    <div>
                        {message} <Button onClick={() => api.belvo.generateToken().then(showConnect)}>Conectar</Button>
                    </div>}
            </div>
        </>
    )
}

function useScript(src) {
    useEffect(
        () => {
          const node = document.createElement('script');
          node.src = src;
          node.type = 'text/javascript';
          node.async = true;
          document.body.appendChild(node);
        },
        [src]
    )
}

export default Connect;