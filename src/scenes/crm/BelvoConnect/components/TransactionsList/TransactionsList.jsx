import React, {  useMemo } from 'react'
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import { moneyFormatter } from '../../../../../services/currencyUtils';

export const TransactionsList = ({linkId, setLinkId}) => {
    const columns = useMemo(() => [
        {
            Header: 'Institución',
            accessor: 'account.institution.name',
            sortable: false,
        },
        {
            Header: 'Cuenta',
            accessor: 'account.name',
            sortable: false,
        },
        {
            Header: 'Monto',
            id: 'amount',
            accessor: (trx) => `$ ${moneyFormatter(trx.amount)} ${trx.currency}`,
            sortable: false,
        },
        {
            Header: 'Fecha',
            id: 'created_at',
            accessor: (trx) => trx.value_date,
            sortable: false,
        },
        {
            Header: 'Descripción',
            accessor: 'description',
            sortable: false,
        },
        {
            Header: 'Estatus',
            accessor: 'status',
            sortable: false,
        }
    ], []);

    const requestOptions = useMemo(() => ({
        customProp: 'belvo_transactions',
        filters: {
            linkId
        }
    }) , [linkId]);
    
    const table = useTideTable({
        entity: 'belvo',
        getMethod: 'getTransactions',
        columns,
        requestOptions
    });

    return (
        <div>
            <h2>
                Listado de transacciones
            </h2>

            <p>
                (las transacciones tardan unos minutos en sincronizarse, si no aparecen intenta unos minutos más tarde)
            </p>

            

            <div>
                {linkId && <TideReactTable key={linkId}
                    {...table.tableProps}
                />}
            </div>
        </div>
    );
}
