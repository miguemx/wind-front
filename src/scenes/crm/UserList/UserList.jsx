
import React, {useCallback, useMemo} from 'react';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {paths} from "../../../services/routes/appRoutes";
import {faPlus} from "@fortawesome/pro-light-svg-icons";
import {useHistory} from "react-router-dom";
import useTideTable from "../../../components/utility/TideReactTable/useTideTable";
import TideReactTable from "../../../components/utility/TideReactTable/TideReactTable";
import {userTableColumns, userTableSGroups} from "../../../services/modelUtils/userUtils";
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';

const requestFilters={sGroups: userTableSGroups};
const filtersConfig = { placeholder: 'Buscar un usuario...', filters: [ { main: true, field:'name' } ]};

const UserList = () => {
    const urlFilters = useUrlFilters();
    
    const history = useHistory();

    //Table config
    const table = useTideTable({
        entity: 'users',
        columns: userTableColumns,
        requestFilters,
        ...urlFilters
    });

    const { tableProps, filters, setFilters } = table;

    useSearchHistory(table);

    // ----- Link to add projects
    const handleAdd = useCallback(()=>history.push(paths.usersNew), [history]);
    const redirectToUserProfile = useCallback((user)=>history.push(paths.usersDetail.replace(':id', user?.id)), [history]);
    const tools = useMemo(()=>[
        { icon: faPlus, callback: handleAdd, text: "Agregar nuevo usuario" },
    ],[handleAdd]);

    return (
        <div className={"UserList wind-scene"}>

            <TopBar title="Usuarios" filters={filters} onFiltersChange={setFilters} filtersConfig={filtersConfig} />

            <ToolBar tools={tools} />

            <div className='center-container'>
                <TideReactTable {...tableProps} onRowClick={redirectToUserProfile} />
            </div>

        </div>
    );
};

export default UserList;
