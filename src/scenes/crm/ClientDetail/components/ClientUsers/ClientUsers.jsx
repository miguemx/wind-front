import React, {useMemo} from 'react';
import './ClientUsers.scss';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import { clientUsersTableColumns, clientUsersTableSGroups } from '../../../../../services/modelUtils/clientUtils';
import DetailHeader from '../../../../../components/utility/DetailHeader/DetailHeader';

const ClientUsers = ({ client }) => {

  const clientUsersRequestFilters = useMemo(() => ({ client: client.id, sGroups: clientUsersTableSGroups }), [client]);

  const table = useTideTable({
    entity: 'users',
    columns: clientUsersTableColumns,
    requestFilters: clientUsersRequestFilters,
  });

  return (
    <div className={"ClientUsers"}>
      <DetailHeader title="Usuarios" />

      <TideReactTable {...table.tableProps}  />
    </div>
  );
}

export default ClientUsers;
