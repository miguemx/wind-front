import React from 'react';
import './ClientInfo.scss';
import SimpleCard from '../../../../../components/layout/SimpleCard/SimpleCard';
import { moneyFormatter } from '../../../../../services/currencyUtils';
import {paths} from "../../../../../services/routes/appRoutes";
import {Link} from "react-router-dom";


const ClientInfo = ({ client }) => {
  return (
    <SimpleCard title="Información general" className='ClientInfo'>
      <div className="info-container">
        <div className="general-content__row">

        <div className="general-content__row-info">
            <Link to={paths.usersDetail.replace(':id', client?.companyOwner?.id)} data-tooltip='Ver perfil de usuario'>
              <p className="general-content__row-info-value">{client?.companyOwner?.fullName||"-"}</p>
              <p className="general-content__row-info-label">Contacto principal</p>
            </Link>
        </div>

          <div className="general-content__row-info">
            <p className="general-content__row-info-value">{client?.companyName||"-"}</p>
            <p className="general-content__row-info-label">Razón social</p>
          </div>

          <div className="general-content__row-info">
            <p className="general-content__row-info-value">$ {moneyFormatter(client?.clientStats?.approvedQuotesSum||0)}</p>
            <p className="general-content__row-info-label">Total aprobado</p>
          </div>

          <div className="general-content__row-info">
            <p className="general-content__row-info-value">$ {moneyFormatter(client?.clientStats?.allQuotesSum||0)}</p>
            <p className="general-content__row-info-label">Total cotizado</p>
          </div>
        </div>

        <div className="general-content__row">
          <div className="general-content__row-info">
            <div className="general-content__row-info-value initials">
              <p className="general-content__row-info-value">{client?.initials||"-"}</p>
            </div>
            <p className="general-content__row-info-label">Iniciales</p>
          </div>
        </div>
      </div>
    </SimpleCard>
  );
}

export default ClientInfo;
