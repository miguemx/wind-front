
import React, {useMemo} from 'react';
import SimpleCard from '../../../../../components/layout/SimpleCard/SimpleCard';
import './ClientAccountStatement.scss';
import { clientQuotesTableColumns, clientQuotesSGroups } from '../../../../../services/modelUtils/clientUtils';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import { moneyFormatter } from '../../../../../services/currencyUtils';
import {paymentQuoteStatus, quoteStatus} from "../../../../../services/modelUtils/quoteUtils";

export const ClientAccountStatement = ({ client }) => {
    const customProp = 'Client.AccountStatement.get.'+client?.id;

    const tableParameters = useMemo(() => {
        return {
            entity: 'quotes',
            columns: clientQuotesTableColumns,
            requestFilters: {
                'project.client': client.id,
                sGroups: clientQuotesSGroups,
                'order[createdDate]':'DESC',
                status: [quoteStatus.APPROVED, quoteStatus.COMPLETED],
                paymentStatus:[paymentQuoteStatus.PAYING, paymentQuoteStatus.PENDING]
            },
            requestOptions: {
                customProp
            }
        }
    }, [client, customProp]);

    const table = useTideTable(tableParameters);

    return (
        <div className="ClientAccountStatement">
            <SimpleCard title="Estado de cuenta">
                <div className="info-container">
                    <div className="general-content__row">
                        <div className="general-content__row-info">
                            <p className="general-content__row-info-value">{`$${moneyFormatter((Number(client?.clientStats?.pendingPaymentsSum)) || 0)}`}</p>
                            <p className="general-content__row-info-label">Pendiente</p>
                        </div>
                        <div className="general-content__row-info">
                            <p className="general-content__row-info-value">{`$${moneyFormatter(Number(client?.clientStats?.paymentsSum) || 0)}`}</p>
                            <p className="general-content__row-info-label">Pagado</p>
                        </div>
                        <div className="general-content__row-info">
                            <p className="general-content__row-info-value">{client?.clientStats?.approvedProjectsCount || 0}</p>
                            <p className="general-content__row-info-label">Proyectos abiertos</p>
                        </div>
                        <div className="general-content__row-info">
                            <p className="general-content__row-info-value">{client?.clientStats?.pendingQuotesCount || 0}</p>
                            <p className="general-content__row-info-label">Cotizaciones pendientes</p>
                        </div>
                    </div>
                </div>
            </SimpleCard>

            {client?.id && <TideReactTable {...table.tableProps} />}
        </div>
    );
};
