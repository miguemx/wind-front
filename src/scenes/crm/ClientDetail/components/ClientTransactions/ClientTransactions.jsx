import React from 'react';
import { useMemo } from 'react';
import DetailHeader from '../../../../../components/utility/DetailHeader/DetailHeader';
import TideReactTable from '../../../../../components/utility/TideReactTable/TideReactTable';
import useTideTable from '../../../../../components/utility/TideReactTable/useTideTable';
import { clientTransactionsTableColumns } from '../../../../../services/modelUtils/clientUtils';
import { transactionsTableSGroups } from '../../../../../services/modelUtils/transactionUtils';
import './ClientTransactions.scss';

const ClientTransactions = ({ client }) => {
  const transactionsFilters = useMemo(() => { 
    let filters = { sGroups: transactionsTableSGroups };

    if (client?.id) {
      filters['quoteTransactions.client.id'] = client.id;
    }
    
    return filters;
  }, [client]);

  const table = useTideTable({
      entity: 'transactions',
      columns: clientTransactionsTableColumns,
      requestFilters: transactionsFilters, 
  });

  return (
    <div className={"ClientTransactions"}>
      <DetailHeader title="Pagos" />

      <TideReactTable
        {...table.tableProps} />
    </div>
  );
}
 
export default ClientTransactions;
