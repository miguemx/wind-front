import React, { useState } from 'react';
import './ClientMainDetails.scss';
import DetailTabs from '../../../../../components/layout/DetailTabs/DetailTabs';
import { clientTabs } from '../../../../../services/modelUtils/clientUtils';

const ClientMainDetails = ({ client }) => {
  const [ activeTab, setActiveTab ] = useState(() => clientTabs[0]);

  const { Component } = activeTab;

  return (
    <div className="ClientMainDetails">
      <DetailTabs
        tabs={clientTabs}
        onTabSelect={setActiveTab}
        activeTab={activeTab}
      />
      {Component && <Component client={client} />}
    </div>
  );
}
 
export default ClientMainDetails;