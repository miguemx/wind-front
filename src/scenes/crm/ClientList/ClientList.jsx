import React, { useCallback, useMemo } from 'react';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {paths} from "../../../services/routes/appRoutes";
import {faPlus} from "@fortawesome/pro-light-svg-icons";
import {useHistory} from "react-router-dom";
import useTideTable from "../../../components/utility/TideReactTable/useTideTable";
import TideReactTable from "../../../components/utility/TideReactTable/TideReactTable";
import {clientTableColumns, clientTableSGroups} from "../../../services/modelUtils/clientUtils";
import {filterTypes} from "../../../services/searchUtils";
import './ClientList.scss';
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';

const requestFilters={sGroups: clientTableSGroups};

const filtersConfig = {
    placeholder: 'Buscar un cliente...', filters: [
        {main: true, field: 'name'},
        {type: filterTypes.TEXT, field: 'users.fullName', label:'Buscar por usuarios'},
        {type: filterTypes.ORDER, field: 'order[name]', label:'Ordenar por nombre'}
    ],
};

const ClientList = () => {
    const urlFilters = useUrlFilters();
    const history = useHistory();

    //Table config
    const table = useTideTable({
        entity: 'clients',
        columns: clientTableColumns,
        requestFilters,
        ...urlFilters
    });

    const { tableProps, filters, setFilters } = table;

    useSearchHistory(table);
    
    // ----- Link to add projects
    const handleAdd = useCallback(()=>history.push(paths.clientNew), [history]);
    const tools = useMemo(()=>[
        { icon: faPlus, callback: handleAdd, text: "Agregar nuevo cliente", testId: 'add-client-button' },
    ],[handleAdd]);

    return (
        <div className={"ClientList wind-scene"}>

            <TopBar
                title="Clientes"
                filters={filters}
                onFiltersChange={setFilters}
                filtersConfig={filtersConfig}
            />

            <ToolBar tools={tools} />

            <div className='center-container'>
                <TideReactTable {...tableProps} />
            </div>
        </div>
    );
};

export default ClientList;
