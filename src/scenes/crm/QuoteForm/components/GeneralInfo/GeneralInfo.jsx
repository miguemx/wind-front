import React, {useCallback, useEffect, useMemo} from 'react';
import WindDatePicker from '../../../../../components/formComponents/WindDatePicker/WindDatePicker';
import WindSelect from '../../../../../components/formComponents/WindSelect/WindSelect';
import WindSwitch from '../../../../../components/formComponents/WindSwitch/WindSwitch';
import MoneyField from '../../../../../components/utility/MoneyField/MoneyField';
import TideEntitySelect from '../../../../../components/utility/TideEntitySelect/TideEntitySelect';
import { quoteStatusOptions } from '../../../../../services/modelUtils/quoteUtils';
import './GeneralInfo.scss';
import {projectSelectorSGroups} from "../../../../../services/modelUtils/projectUtils";
import useFormState from "../../../../../hooks/useFormState";

const GeneralInfo = ({ total, subtotal, form, setForm }) => {
  const { bindSimple } = useFormState(()=>({ expirationDate: new Date() }));
  const clientID = form.client?.id;
  // ----- Filter projects selector -----
  const projectFilters = useMemo(() =>({
    client: form.client?.id,
    'order[updatedDate]':'DESC',
    sGroups: projectSelectorSGroups
  }), [form.client]);

  // Make the project and the client match
  const {project, client} = form;
  useEffect(()=>{
    if( project?.client && client ){
      setForm(form=>({
        ...form,
        client: project.client
      }));
    }
  },[project, client, setForm]);
  // If the client change, delete the project if it's from another client
  const handleClientChange = useCallback((client)=>{
    setForm(form=>({
      ...form,
      client,
      project: form?.project?.client?.id === client.id? form.project : null
    }))
  },[setForm]);
  // If the project change, set the client
  const handleProjectChange = useCallback((project)=>{
    setForm(form=>({
      ...form,
      project,
      client: project?.client,
    }))
  },[setForm]);

  const quoteRequestFilters = useMemo(() =>(
    { client: clientID }
  ), [clientID]);

  const newProjectParams = useMemo(()=>({client: clientID, sGroups: projectFilters.sGroups}), [clientID, projectFilters]);
  const newClientParams = useMemo(()=>({sendAccess: false}), []);

  return (
    <>
      <div className='fields'>
        <TideEntitySelect
          className='project-field'
          entity='clients'
          placeholder={"Compañía"}
          value={form.client}
          onChange={handleClientChange}
          preload
          creatable
          createExtraParams={newClientParams}
        />

        <TideEntitySelect
          className='project-field'
          entity='projects'
          placeholder={"Proyecto"}
          additionalFilters={projectFilters}
          value={form.project}
          onChange={handleProjectChange}
          preload
          creatable
          createExtraParams={newProjectParams}
        />

        <WindSwitch
          className='project-field'
          placeholder={'IVA'}
          {...bindSimple('iva')}
        />

        <TideEntitySelect
          className='project-field'
          entity='quotationRequests'
          placeholder={"Solicitud"}
          {...bindSimple('quotationRequest')}
          labelCreator={qr=> qr.title}
          additionalFilters={quoteRequestFilters}
          preload
        />
      </div>

      <div className='fields'>
        <WindDatePicker
          className='project-field'
          placeholder={"Expiración"}
          {...bindSimple('expirationDate')}
          label={'Expiración'}
        />
        <WindSelect
          className={'project-field'}
          options={quoteStatusOptions}
          placeholder={"Estado"}
          {...bindSimple('status')}
        />

        <MoneyField
          className='project-field'
          placeholder={"Subtotal"}
          amount={subtotal}
        />
        <MoneyField
          className='project-field'
          placeholder={"Total"}
          amount={total}
        />
      </div>
    </>
  );
}

export default GeneralInfo;
