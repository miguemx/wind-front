import React, { useEffect, useRef, useCallback } from 'react';
import './QuoteProductForm.scss';
import WindInput from '../../../../../components/formComponents/WindInput/WindInput';
import WindSwitch from '../../../../../components/formComponents/WindSwitch/WindSwitch';
import IconButton from '../../../../../components/utility/IconButton/IconButton';
import RichTextEditor from '../../../../../components/formComponents/RichTextEditor/RichTextEditor';
import { faMinusSquare } from '@fortawesome/pro-light-svg-icons';
import useCallbackCreator from 'use-callback-creator';

const QuoteProductForm = ({ quoteProduct, onChange, onRemove }) => {
  const richTextEditor = useRef();

  const handleChange = useCallbackCreator((name, value) => {
    onChange({ ...quoteProduct, [name]: value });
  }, [onChange, quoteProduct]);

  const setTextEditorReference = useCallback((newTextEditor) => {
    richTextEditor.current = newTextEditor;
    quoteProduct.getDescription = newTextEditor?.getContentHTML;
  },[quoteProduct]);

  useEffect(() => {
    quoteProduct.getDescription = richTextEditor.current.getContentHTML;
  }, [quoteProduct]);

  const handleProductRemove = useCallback(() => {
    onRemove();
  }, [onRemove]);

  return (
    <div className={'QuoteProductForm'}>
      <div className='fields'>
        <WindInput
          className='project-field'
          placeholder={"Nombre del producto"}
          value={quoteProduct?.name}
          onChange={handleChange('name')}
        />
        <WindInput
          className='project-field'
          placeholder={"Cantidad"}
          value={quoteProduct?.quantity}
          onChange={handleChange('quantity')}
          type={'number'}
        />
        <WindInput
          className='project-field'
          placeholder={"Precio"}
          type={'number'}
          value={quoteProduct?.price}
          onChange={handleChange('price')}
        />
        <WindSwitch
          className='project-field'
          placeholder={'Opcional'}
          value={quoteProduct?.isOptional}
          onChange={handleChange('isOptional')}
        />
        <IconButton
          className={'remove-button'}
          onClick={handleProductRemove}
          data-tooltip={'Remover producto'}
          icon={faMinusSquare}
          color='danger'
        />
      </div>
      <div className='description'>
        <RichTextEditor
          ref={setTextEditorReference}
          html={quoteProduct?.description}
          placeholder={'Agrega una descripción ...'}
        />
      </div>
      <hr className="rounded" />
    </div>
  );
}
 
export default QuoteProductForm;
