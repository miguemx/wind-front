import React, {useCallback, useContext, useEffect, useLayoutEffect, useMemo, useRef} from 'react';
import './QuoteForm.scss';
import {faPlusSquare, faSave} from '@fortawesome/pro-light-svg-icons';
import { useSelector } from 'react-redux';
import {useHistory, useLocation, useParams} from 'react-router-dom';
import ToolBar from '../../../components/utility/ToolBar/ToolBar';
import TopBar from '../../../components/utility/TopBar/TopBar';
import useFormState from '../../../hooks/useFormState';
import { ApiContext } from '../../../services/api/api-config';
import { getEmptyQuote, getQuoteSubtotal, prepareQuoteForServer, getIvaAmount } from '../../../services/modelUtils/quoteUtils';
import { getNotifier } from '../../../services/notifier';
import { paths } from "../../../services/routes/appRoutes";
import SimpleCard from '../../../components/layout/SimpleCard/SimpleCard';
import GeneralInfo from './components/GeneralInfo/GeneralInfo';
import QuoteProductForm from './components/QuoteProductForm/QuoteProductForm';
import { getEmptyQuoteProduct } from '../../../services/modelUtils/quoteProductsUtils';
import _ from 'lodash';
import useCallbackCreator from 'use-callback-creator';
import classNames from 'classnames';
import {projectSelectorSGroups} from "../../../services/modelUtils/projectUtils";
import {quoteDetailSGroups, convertQuoteToForm} from "../../../services/modelUtils/quoteUtils";

const QuoteForm = () => {
  const api = useContext(ApiContext);
  const history = useHistory();
  const { form, setForm, bindInput, bindSimple } = useFormState(() =>getEmptyQuote());
  const { id } = useParams();

  const cloning = history.location.pathname.indexOf('clone') !== -1;

  const subtotal = getQuoteSubtotal(form);
  const total = subtotal + getIvaAmount(form, subtotal);

  // ----- Load quote from server -----
  useEffect(()=>{
    if(id)
      api.quotes.get({ id, params:{sGroups: quoteDetailSGroups}, customProp:'_' })
        .then(quote => setForm(convertQuoteToForm(quote)));
  },[api, id, setForm]);

  // ----- Autofocus first input -----
  const titleInputRef = useRef();
  useLayoutEffect(()=>titleInputRef.current.focus() ,[]);

  // ------ Preload project if needed ------
  const { search } = useLocation();
  useEffect(()=>{
    const preloadProjectId = (new URLSearchParams(search)).get('project');
    if( preloadProjectId ){
      api.projects.get( { id:preloadProjectId, params:{sGroups: projectSelectorSGroups}, customProp:'_' } ).then( project=>{
        if(project)
          setForm( form=>({...form, project, client: project.client}));
      })
    }
  },[api, search, setForm]);

  // ------ Save to server ------
  const loadingId = 'QuoteForm.create';
  const loading = useSelector(s=>!!s.loadingIds[loadingId]);
  const handleSave = useCallback(()=>{
    const {id} = form;
    const create = !id || cloning;
    const apiMethod = create? 'create':'update';
    let quote;

    try{
        quote = prepareQuoteForServer({...form, total: total, subtotal: subtotal, status: form.status?.value});
    }
    catch (e){
        return getNotifier().warning(e.message);
    }

    const params = {
      id,
      loadingId,
      params: quote,
    };

    api.quotes[apiMethod](params)
        .then((quote)=>{
          getNotifier().success(`La cotización se ha ${create?'creado':'editado'} correctamente`);
            history.replace( paths.quoteDetail.replace(':id', quote.id) );
        });
  }, [form, api, history, total, subtotal, cloning]);
  console.log(api);

  // ------ Form state handlers ------
  const addQuoteProduct = useCallback(()=>{
    setForm({
      ...form,
      quoteProducts: [...form.quoteProducts, getEmptyQuoteProduct()]
    });
  },[setForm, form]);

  const removeQuoteProduct = useCallbackCreator((index)=>{
    const newQuoteProducts = [...form.quoteProducts];
    newQuoteProducts.splice(index, 1);

    setForm({ ...form, quoteProducts: newQuoteProducts });
  },[setForm, form]);

  const handleQuoteProductChange = useCallbackCreator((index, quoteProduct)=>{
    const newQuoteProducts = [...form.quoteProducts];
    newQuoteProducts[index] = quoteProduct;
    setForm({ ...form, quoteProducts: newQuoteProducts });
  }, [setForm, form]);

  // ------ Toolbar definition ------
  const tools = useMemo(()=>[
    { icon: faSave, callback: handleSave, text: "Guardar cotización", disabled: loading, testId: "save-quote-button"  },
    { icon: faPlusSquare, callback: addQuoteProduct, text: "Añadir producto", disabled: loading, testId: "add-quote-product-button"  },
  ],[handleSave, loading, addQuoteProduct]);

  let title = "Nueva cotización";
  if( id ){
    if( cloning )
      title = "Clonar cotización";
    else
      title = "Editar cotización";
  }

  return (
    <div className={"QuoteForm wind-scene"}>

      <TopBar
        title={title}
        titleLinkBack
      />

      <ToolBar tools={tools} />

      <div className='center-container'>

        <div className='quote-header' >
          <input className='title-input' placeholder="Agrega un título a la cotización" {...bindInput('title')} ref={titleInputRef} />
        </div>

        <SimpleCard className='general-info' title="Información general">
          <GeneralInfo
            bindSimple={bindSimple}
            total={total}
            subtotal={subtotal}
            form={form}
            setForm={setForm}
          />
        </SimpleCard>

        <SimpleCard className={classNames('products', form.quoteProducts?.length === 1 && 'single-quote-product')} title="Productos">
          {_.map(form.quoteProducts, (quoteProduct, index)=>(
            <QuoteProductForm
              key={quoteProduct.id||index}
              onRemove={removeQuoteProduct(index)}
              quoteProduct={quoteProduct}
              onChange={handleQuoteProductChange(index)}
            />
          ))}
        </SimpleCard>
      </div>
    </div>
  );
}

export default QuoteForm;
