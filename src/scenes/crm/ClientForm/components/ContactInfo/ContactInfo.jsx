import React from 'react'
import WindInput from '../../../../../components/formComponents/WindInput/WindInput';
import WindSwitch from '../../../../../components/formComponents/WindSwitch/WindSwitch';
import classNames from "classnames";
import './ContactInfo.scss';

const ContactInfo = ({ bindSimple, form, className }) => {
    return (
        <>
            <div className={classNames("ContactInfo", className)}>
                <WindSwitch
                    className='project-field max30p'
                    placeholder={'Agregar información de contacto'}
                    {...bindSimple('addContactInfo')}
                />
                {form.addContactInfo && 
                    <WindSwitch
                        className='project-field'
                        placeholder={'Enviar credenciales'}
                        {...bindSimple('sendAccess')}
                    />}
                
                {form.addContactInfo && 
                    <WindInput 
                        className='WindInput project-field'
                        placeholder={"Email de contato"}
                        {...bindSimple('email')}
                        inputProps={{maxLength:60}}
                    />}
            </div>
            
            {form.addContactInfo && 
                <div className={classNames("ContactInfo ContactInfo--mt", "className")}>
                    <WindInput 
                        className='project-field'
                        placeholder={"Nombre del contacto"}
                        {...bindSimple('name')}
                        inputProps={{maxLength:100}}
                    />
                    <WindInput 
                        className='WindInput project-field'
                        placeholder={"Apellido del contacto"}
                        {...bindSimple('lastname')}
                        inputProps={{maxLength:100}}
                    />
                    <WindInput 
                        className='WindInput project-field'
                        placeholder={"Teléfono de contacto"}
                        {...bindSimple('phone')}
                        inputProps={{maxLength:20}}
                    />
                </div>}
        </>
    )
}

export default ContactInfo;