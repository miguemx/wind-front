import React from 'react'
import WindInput from '../../../../../components/formComponents/WindInput/WindInput';
import classNames from "classnames";
import './FiscalInfo.scss';

const FiscalInfo = ({ setForm, fiscalData, className }) => {
    const onChangeFiscalData = (key, value) => {
        setForm(prev => ({
            ...prev,
            fiscalData: {
                ...prev.fiscalData,
                [key]: value
            }
        }));
    }

    return (
        <>
            <div className={classNames("FiscalInfo", className)}>
                <WindInput 
                    className='project-field'
                    placeholder={"Nombre Fiscal"}                    
                    inputProps={{maxLength:100}}
                    value={fiscalData?.businessName||''}
                    onChange={(value) => onChangeFiscalData('businessName', value)}
                />
            </div>
            
            <div className={classNames("FiscalInfo FiscalInfo--mt", "className")}>
            <WindInput 
                    className='WindInput project-field'
                    placeholder={"Email de facturación"}
                    inputProps={{maxLength:60}}
                    value={fiscalData?.email||''}
                    onChange={(value) => onChangeFiscalData('email', value)}
                />

                <WindInput 
                    className='WindInput project-field'
                    placeholder={"RFC"}
                    inputProps={{maxLength:100}}
                    value={fiscalData?.rfc||''}
                    onChange={(value) => onChangeFiscalData('rfc', value)}
                />
                <WindInput 
                    className='WindInput project-field'
                    placeholder={"Nombre Comercial"}
                    inputProps={{maxLength:20}}
                    value={fiscalData?.comercialName||''}
                    onChange={(value) => onChangeFiscalData('comercialName', value)}
                />
            </div>

            <div className={classNames("FiscalInfo FiscalInfo--mt", className)}>
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Calle"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.street||''}
                    onChange={(value) => onChangeFiscalData('street', value)}
                />
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Número Externo"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.externalNumber||''}
                    onChange={(value) => onChangeFiscalData('externalNumber', value)}
                />
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Número Interno"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.internalNumber||''}
                    onChange={(value) => onChangeFiscalData('internalNumber', value)}
                />
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Colonia"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.neighborhood||''}
                    onChange={(value) => onChangeFiscalData('neighborhood', value)}
                />

            </div>

            <div className={classNames("ContactInfo ContactInfo--mt", className)}>
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Delegacion o Municipio"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.delegation||''}
                    onChange={(value) => onChangeFiscalData('delegation', value)}
                />
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Ciudad"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.city||''}
                    onChange={(value) => onChangeFiscalData('city', value)}
                />
                <WindInput
                    className='WindInput project-field'
                    placeholder={"País"}
                    inputProps={{maxLength:255}}
                    value={fiscalData?.country||''}
                    onChange={(value) => onChangeFiscalData('country', value)}
                />
                <WindInput
                    className='WindInput project-field'
                    placeholder={"Código Postal"}
                    inputProps={{maxLength:5}}
                    value={fiscalData?.zip||''}
                    onChange={(value) => onChangeFiscalData('zip', value)}
                />
            </div>
        </>
    )
}

export default FiscalInfo;