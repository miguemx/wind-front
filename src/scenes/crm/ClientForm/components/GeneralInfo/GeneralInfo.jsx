import React from 'react'
import WindInput from '../../../../../components/formComponents/WindInput/WindInput';
import WindSwitch from '../../../../../components/formComponents/WindSwitch/WindSwitch';
import classNames from "classnames";
import './GeneralInfo.scss';

const GeneralInfo = ({ bindSimple, form, className }) => {
    return (
        <>
            <div className={classNames("GeneralInfo", className)}>
                {form.isLegalPerson && 
                    <WindInput 
                        className='WindInput project-field'
                        placeholder={"Razón Social"}
                        {...bindSimple('companyName')}
                        inputProps={{maxLength:255}}
                    />}
                    
                    {!form.isLegalPerson && 
                    <WindInput 
                        className='WindInput project-field'
                        placeholder={"Nombre(s)"}
                        {...bindSimple('clientName')}
                        inputProps={{maxLength:255}}
                    />}

                {!form.isLegalPerson && 
                    <WindInput
                        className='WindInput project-field'
                        placeholder={"Apellido Paterno"}
                        {...bindSimple('lastName')}
                        inputProps={{maxLength:255}}
                    />}
                
                {!form.isLegalPerson && 
                    <WindInput
                        className='WindInput project-field'
                        placeholder={"Apellido Materno"}
                        {...bindSimple('secondLastName')}
                        inputProps={{maxLength:255}}
                    />}
                
                <WindInput 
                    className='project-field'
                    placeholder={"Iniciales"}
                    {...bindSimple('initials')}
                    inputProps={{maxLength:8}}
                />
                <WindSwitch
                    className='project-field'
                    placeholder={'Persona moral'}
                    {...bindSimple('isLegalPerson')}
                />
            </div>
            <div className={classNames("GeneralInfo GeneralInfo--mt", className)}>
                <WindInput
                    className='project-field max30p'
                    placeholder={"Teléfono"}
                    {...bindSimple('phone')}
                    inputProps={{maxLength:20}}
                />
            </div>
        </>
    )
}

export default GeneralInfo;