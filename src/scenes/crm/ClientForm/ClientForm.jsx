import React, {useMemo, useCallback, useContext, useEffect} from 'react';
import './ClientForm.scss';
import TopBar from "../../../components/utility/TopBar/TopBar";
import {paths} from "../../../services/routes/appRoutes";
import { faFloppyDisk } from "@fortawesome/pro-light-svg-icons";
import {useSelector} from "react-redux";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import useFormState from "../../../hooks/useFormState";
import GeneralInfo from './components/GeneralInfo/GeneralInfo';
import SimpleCard from "../../../components/layout/SimpleCard/SimpleCard";
import WindTitleInput from "../../../components/formComponents/WindTitleInput/WindTitleInput";
import { getNotifier } from '../../../services/notifier';
import { ApiContext } from '../../../services/api/api-config';
import { prepareClientForServer, clientDetailSGroups, prepareServerToForm, prepareClientForServerUpdate } from '../../../services/modelUtils/clientUtils';
import { useHistory } from 'react-router-dom';
import ContactInfo from './components/ContactInfo/ContactInfo';
import { useParams } from 'react-router-dom';
import TideEntitySelect from '../../../components/utility/TideEntitySelect/TideEntitySelect';
import FiscalInfo from './components/FiscalInfo/FiscalInfo';

const ClientForm = () => {
    const { id } = useParams();
    const { form, bindSimple, setForm } = useFormState(()=>({ startDate: new Date() }));
    const history = useHistory();
    const api = useContext(ApiContext);
    const loadingId = `ClientForm.${id}`;

    // ----- Load Client data
    const loadClient = useCallback(() => {
        api.clients.get({ id, loadingId, params: { sGroups: clientDetailSGroups } })
            .then(client => setForm(prepareServerToForm(client)))
        .catch(() => getNotifier().error('No se encontró el cliente solicitado'));
      }, [api, id, loadingId, setForm]);
      
    useEffect(()=>{ 
        if(id) loadClient();
    },[id, loadClient]);

    const handleSave = useCallback(()=>{
        let client;
        const method = id ? 'update' : 'create';

        try{
            if(!id)
                client = prepareClientForServer({...form});
            else
                client = prepareClientForServerUpdate({...form});
        } catch (e){
            return getNotifier().warning(e.message);
        }

        api.clients[method]({ id, params:client, loadingId })
            .then((client) => {
                getNotifier().success("El cliente se guardó correctamente 🎉");
                history.push(paths.clientDetail.replace(':id', client.id));
            });
    }, [api, form, loadingId, history, id]);

    const loading = useSelector(s=>!!s.loadingIds[loadingId]);

    const tools = useMemo(()=>[
        { icon: faFloppyDisk, callback: handleSave, text: "Guardar cliente", disabled: loading  },
    ],[handleSave, loading]);

    // ----- Filter projects selector -----
    const mainContactFilters = useMemo(() =>({
        client: id,
        'order[updatedDate]':'DESC',
        sGroups: ['user_read']
    }), [id]);
    
    return (
        <div className={"ClientForm wind-scene"}>

            <TopBar
                title={`${id?'Editar':'Nuevo'} cliente`}
                titleLinkBack
            />

            <ToolBar tools={tools} />

            <div className='center-container'>
                <WindTitleInput
                    {...bindSimple('clientName')}
                    value={form?.clientName}
                    label="Nombre"
                    placeholder="Nombre del cliente"
                    inputProps={{maxLength:100}}
                />

                <SimpleCard
                    className='general-info'
                    title="Información general"
                >
                    <GeneralInfo
                        bindSimple={bindSimple}
                        form={form}
                    />
                </SimpleCard>

                <SimpleCard
                    className='general-info'
                    title="Información de contacto"
                >
                {/* Only captures the main contact for new clients */}
                    {!id ?
                        <ContactInfo
                            bindSimple={bindSimple}
                            form={form}
                        /> 
                        :
                        <TideEntitySelect
                            className='main-contact-field'
                            entity='users'
                            placeholder={"Contacto principal"}
                            {...bindSimple('companyOwnerId')}
                            additionalFilters={mainContactFilters}
                            preload={true}
                        />
                    }
                </SimpleCard>

                <SimpleCard
                    className='general-info'
                    title="Datos Fiscales"
                >
                        <FiscalInfo
                            setForm={setForm}
                            fiscalData={form?.fiscalData}
                        />
                </SimpleCard>
            </div>
        </div>
    );
}

export default ClientForm;
