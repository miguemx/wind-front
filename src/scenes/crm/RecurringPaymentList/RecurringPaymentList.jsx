
import React, {useCallback, useContext, useMemo} from 'react';
import TopBar from "../../../components/utility/TopBar/TopBar";
import ToolBar from "../../../components/utility/ToolBar/ToolBar";
import {faPlus} from "@fortawesome/pro-light-svg-icons";
import {faFileExport} from "@fortawesome/pro-light-svg-icons";
import {recurringPaymentListSGroups, recurringPaymentColumns} from "../../../services/modelUtils/recurringPaymentUtils";
import {filterTypes} from "../../../services/searchUtils";
import {ApiContext} from "../../../services/api/api-config";
import useTideTable from "../../../components/utility/TideReactTable/useTideTable";
import TideReactTable from "../../../components/utility/TideReactTable/TideReactTable";
import useSearchHistory from '../../../hooks/useSearchHistory';
import useUrlFilters from '../../../hooks/useUrlFilters';
import useBoolean from '../../../hooks/useBoolean';
import RecurringPaymentsFormModal from '../RecurringPaymentsFormModal/RecurringPaymentsFormModal';

const requestFilters = {sGroups: recurringPaymentListSGroups};
const filtersConfig = {
    placeholder: 'Buscar una cuenta...', filters: [
        {main: true, field: 'concept'},
        {type: filterTypes.TEXT, field: 'concept', label: 'Buscar por concepto de pago'},
    ]
};

const RecurringPaymentList = () => {
    const urlFilters = useUrlFilters();
    
    const api = useContext(ApiContext);
    const [showingForm, showForm, hideForm] = useBoolean(false);
    const exportToExcel = useCallback(() => {window.location.href = api.recurringPayments.getExportExcelUrl({pagination: false, sGroups: recurringPaymentListSGroups})}, [api]);

    // ----- 
    const tools = useMemo(() => [
        {icon: faPlus, callback: showForm, text: "Agregar nuevo pago recurrente", testId: 'add-recurring-payment-button'},
        {icon: faFileExport, callback: exportToExcel, text: "Exportar pagos recurrentes", testId: 'export-recurring-payments-button'},
    ], [exportToExcel, showForm]);

    //Table config
    const table = useTideTable({
        entity: 'recurringPayments',
        columns: recurringPaymentColumns,
        requestFilters,
        ...urlFilters
    });

    const { tableProps, filters, setFilters } = table;

    useSearchHistory(table);
    
    return (
        <div className={"RecurringPaymentList wind-scene"}>

            <TopBar
                title="Pagos recurrentes"
                filters={filters}
                onFiltersChange={setFilters}
                filtersConfig={filtersConfig}
            />

            <ToolBar tools={tools}/>

            <div className='center-container'>
                <TideReactTable {...tableProps} />
            </div>

            {showingForm &&
                <RecurringPaymentsFormModal
                    onClose={hideForm}
                    onRecurringPaymentSaved={() => table.reload()}  
                />
            }
        </div>
    );
}

export default RecurringPaymentList;