import React, {useContext, useEffect} from 'react';
import {ApiContext} from "../../../services/api/api-config";
import { paths } from "../../../services/routes/notLoggedRoutes";

const Logout = () => {
    const api = useContext(ApiContext);
    useEffect(()=>{
        // goto login
        api.logout();
        window.location.href = paths.login;
    },[api]);

    return (
        <div className={"Logout.jsx"}>
            logging out...
        </div>
    );
};

export default Logout;
