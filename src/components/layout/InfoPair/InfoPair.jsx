import React from 'react';
import './InfoPair.scss';

const InfoPair = ({value, title}) => {

    return (
        <div className={"InfoPair"}>
            <div className='info-value'>{value}</div>
            <div className='info-title'>{title}</div>
        </div>
    );
};

export default InfoPair;
