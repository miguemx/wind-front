import React from 'react';
import './SimpleCardTwoColumns.scss';
import UnderlinedTitle from "../UnderlinedTitle/UnderlinedTitle";
import classNames from "classnames";

const SimpleCardTwoColumns = ({title, className, children, child2, ...props}) => {
    return (
        <div className={classNames("SimpleCardTwoColumns", className)} {...props} >
            {child2 &&
                <div className='SimpleCardTwoColumns__columnLeft'>
                    {child2}
                </div>}
            <div className='SimpleCardTwoColumns__columnRight'>
                {!!title && 
                    <UnderlinedTitle className='card-title' secondary>
                        {title}
                    </UnderlinedTitle>}
                <div className='card-content'>
                    {children}
                </div>
            </div>
        </div>
    );
};

export default SimpleCardTwoColumns;
