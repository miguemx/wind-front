import './LogoBackground.scss';
import {useMemo} from "react";
import {useLocation } from 'react-router-dom';
import logo from "../../../assets/images/wind-isotipo.png";
import {positions, routesPositions, straightPaths} from "./logoBackgroundUtils";
import _ from 'lodash';

const LogoBackground = () => {

    const path = useLocation().pathname;

    const styles = useMemo(()=>{

        const isStraight = !!_.find( straightPaths, p=>p===path);
        let position;

        if( isStraight )
            position = {};
        else {
            position = _.find(routesPositions, rp => rp.path === path)?.position;
            if (!position)
                position = _.sample(positions);
        }

       return {
           container: { filter: isStraight?"none":"blur(20px)" },
           logo: position
    };
    },[ path ]);

    return <div className={"LogoBackground"} style={ styles.container } >
        <img className='logo' alt="l" src={logo} style={ styles.logo } />
    </div>;
};

export default LogoBackground;
