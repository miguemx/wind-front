import {paths} from "../../../services/routes/appRoutes";
import {paths as publicPaths } from "../../../services/routes/notLoggedRoutes";

//All the positions the logo in the background could be
export const positions = [
    { top: "20%", right: "50%", transform: 'rotate(-45deg) scale(0.8)' },
    { top: "-10%", right: "0", transform: 'rotate(45deg) scale(0.8)' },
    { top: "10%", right: "30%", transform: 'rotate(-90deg) scale(1.2)' },
    { top: "10%", right: "0", transform: 'rotate(10deg)' },
    { top: "40%", right: "10%", transform: 'rotate(-100deg) scale(1.1)' },
];

//Specific positions for a given route, routes not defined should use a random position
export const routesPositions = [
    { path: paths.projects, position: positions[0] },
];

//This paths should have the straight position and no blur
export const straightPaths = [ publicPaths.login ];
