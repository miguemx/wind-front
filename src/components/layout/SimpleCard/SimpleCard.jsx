import React from 'react';
import './SimpleCard.scss';
import UnderlinedTitle from "../UnderlinedTitle/UnderlinedTitle";
import classNames from "classnames";

const SimpleCard = ({title, className, children, ...props}) => {

    return (
        <div className={classNames("SimpleCard", className)} {...props} >
            {!!title && <UnderlinedTitle className='card-title' secondary>{title}</UnderlinedTitle>}
            <div className='card-content'>
                {children}
            </div>
        </div>
    );
};

export default SimpleCard;
