import React, {useCallback} from 'react';
import './DetailTabs.scss';
import _ from 'lodash';
import UnderlinedTitle from '../UnderlinedTitle/UnderlinedTitle';
import { useLocation, useHistory } from "react-router-dom";

const URL_PARAM_TAB = 'tab';

const DetailTabs = ({ tabs, onTabSelect, activeTab }) => {
  const { search } = useLocation();
  const history = useHistory();

  if (search) {
    const urlTab = (new URLSearchParams(search)).get(URL_PARAM_TAB);

    if (urlTab) {
      const tab = _.find(tabs, { value: urlTab });
      
      if(tab)
        onTabSelect(tab);
    }
  }
  
  const handleTabClick = useCallback((tab) => {
    history.push({search: `?${URL_PARAM_TAB}=${tab.value}`})
    onTabSelect(tab);
  }, [history, onTabSelect]);

  return (
    <div className={"DetailTabs"}>
      {_.map(tabs, (formatTab, index) => {

        const isActive = formatTab.value === activeTab.value;

        return (
          <button
            type="button"
            className="DetailTabs__tab"
            key={formatTab.value||index}
            onClick={() => handleTabClick(formatTab)}
          >
          <div className="DetailTabs__tab-title">
            {isActive ? (
              <UnderlinedTitle Tag="h4">{formatTab.label}</UnderlinedTitle>
            ) : (
              <span>{formatTab.label}</span>
            )}
          </div>
        </button>
        );
      })}
    </div>
  );
}
 
export default DetailTabs;