import React from 'react';
import './MainLoadingIndicator.scss';
import {useSelector} from "react-redux";
import {Player} from "@lottiefiles/react-lottie-player";
import animation from '../../../assets/animations/73205-windmill.json';

const MainLoadingIndicator = () => {

    const isLoadingRequest = useSelector(({loading})=>!!loading);

    if(!isLoadingRequest)
        return null;

    return (
        <div className={"MainLoadingIndicator"}>
            <Player src={animation} autoplay loop />
            <span>cargando...</span>
        </div>
    );
};

export default MainLoadingIndicator;
