import React from 'react';
import './UnderlinedTitle.scss';
import classNames from "classnames";

const UnderlinedTitle = ({ Tag = "h1", children, secondary, className, ...props }) => {

    if(secondary){
        Tag = "h2";
    }

    return (
        <Tag className={classNames("UnderlinedTitle", className, secondary && "secondary")} {...props}>
            <span className='title-content'>{children}</span>
        </Tag>
    );
};

export default UnderlinedTitle;
