import React from 'react';
import './SearchButton.scss';
import {faSearch} from "@fortawesome/pro-light-svg-icons";
import {faSearch as faSearchActive} from "@fortawesome/pro-duotone-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import useBoolean from "../../../hooks/useBoolean";
import SearchBar from "../SearchBar/SearchBar";
import _ from 'lodash';
import classNames from "classnames";

const SearchButton = ({ filtersConfig, filters, onFiltersChange }) => {

    const [isOpen, open, close] = useBoolean();

    const isActive = !!_.find(filters);

    return (<>
        <button className={classNames( "SearchButton", isActive &&"active")} data-tooltip={"Buscar"} onClick={open}>
            <FontAwesomeIcon icon={isActive ? faSearchActive : faSearch}/>
        </button>
        {isOpen && <SearchBar
            onClose={close}
            filtersConfig={filtersConfig}
            filters={filters}
            onFiltersChange={onFiltersChange}
        />}
    </>);
};

export default SearchButton;
