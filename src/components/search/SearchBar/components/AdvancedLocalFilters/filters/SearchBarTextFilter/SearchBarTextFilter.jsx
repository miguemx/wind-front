import React from 'react';
import WindInput from "../../../../../../formComponents/WindInput/WindInput";

const SearchBarTextFilter = ({ value, onChange, config }) => {
    return (
        <div className={"TextFilter SearchBarFilter"}>
            <WindInput placeholder={config?.label} value={value} onChange={onChange} />
        </div>
    );
};

export default SearchBarTextFilter;
