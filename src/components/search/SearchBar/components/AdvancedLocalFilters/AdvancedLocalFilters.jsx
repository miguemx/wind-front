import React from 'react';
import './AdvancedLocalFilters.scss';
import _ from 'lodash';
import useCallbackCreator from "use-callback-creator";
import {filterTypes} from "../../../../../services/searchUtils";
import SearchBarTextFilter from "./filters/SearchBarTextFilter/SearchBarTextFilter";
import SearchBarOrderFilter from "./filters/SearchBarOrderFilter/SearchBarOrderFilter";
import SearchBarSelectFilter from "./filters/SearchBarSelectFilter/SearchBarSelectFilter";
import SearchBarDateFilter from './filters/SearchBarDateFilter/SearchBarDateFilter';

const filterDefinition = {
    [filterTypes.TEXT]: SearchBarTextFilter,
    [filterTypes.ORDER]: SearchBarOrderFilter,
    [filterTypes.SELECT]: SearchBarSelectFilter,
    [filterTypes.DATE]: SearchBarDateFilter,
}
const FilterComponent = ({type, ...props})=>{
    const Component = filterDefinition[type]||filterDefinition[filterTypes.TEXT];
    return <Component {...props} />
};

const AdvancedLocalFilters = ({ onFiltersChange, filters, filtersConfig }) => {

    const filterList = _.filter( filtersConfig?.filters, filter=>!filter.main );
    const handleFilterChange = useCallbackCreator((field, value)=>{
        const newFilters = {...filters};
        if (value === '' || typeof value === 'undefined')//Erase keys of empty filters
            delete newFilters[field];
        else // Or set the new value to filter
            newFilters[field] = value;
        onFiltersChange(newFilters);
    },[filters, onFiltersChange]);

    if(!filterList.length)
        return null;
    return (
        <div className={"AdvancedLocalFilters"}>
            {_.chunk(filterList, 4).map((row, index) => 
                <div className={"filterRow"} key={index}>
                {row.map(filter => 
                    <FilterComponent
                        key={filter.field}
                        type={filter.type}
                        value={filters[filter.field]}
                        onChange={handleFilterChange(filter.field)}
                        config={filter}
                    />
                )}
                </div>
            )}
        </div>
    );
};

export default AdvancedLocalFilters;
