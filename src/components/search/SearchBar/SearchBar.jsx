import React, {useCallback, useLayoutEffect, useRef, useState} from 'react';
import './SearchBar.scss';
import {createPortal} from "react-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEarthAmericas, faFilterList, faMagnifyingGlass, faTimes} from "@fortawesome/pro-light-svg-icons";
import _ from 'lodash';
import classNames from "classnames";
import AdvancedLocalFilters from "./components/AdvancedLocalFilters/AdvancedLocalFilters";

const defaultConfig = {
    placeholder: 'Escribe para buscar...'
};

const GLOBAL_MODE = 'GLOBAL_MODE';
const LOCAL_MODE = 'LOCAL_MODE';

/**
 * The search bar has two modes.
 * Local filter mode. Used by the parent by the control props, requires de filtersConfig prop
 * Global filter mode. It searches throughout all the system, it requires no props since the results will link to the corresponding page
 *
 * @param onClose Callback to close the search bar
 * @param filtersConfig { placeholder: string, filters: [ { field: string, main: bool, type: string, label: string } ] }
 * @param filters { field: any } The local filters value to control the component
 * @param onFiltersChange Filter change callback to control the component
 * @returns function Component
 * @constructor
 */
const SearchBar = ({
                       onClose,
                       //For local filtering
                       filtersConfig={},
                       filters={},
                       onFiltersChange,
                   }) => {

    //Config for local filtering
    const config = {...defaultConfig, ...filtersConfig};
    const mainFilter = _.find(filtersConfig?.filters, f=>f.main) || filtersConfig?.filters?.[0];
    const localFiltersAvailable = !!mainFilter;

    //Global filter state
    const [globalFilter, setGlobalFilter] = useState('');

    //Mode handling
    const [searchMode, setSearchMode ] = useState(localFiltersAvailable? LOCAL_MODE : GLOBAL_MODE );
    const changeSearchMode = (newMode)=>{
        if(newMode === searchMode) return;
        setSearchMode(newMode);
        if(newMode === LOCAL_MODE)
            setGlobalFilter('');
    }

    // Handle main filter input changes
    const handleFilterChange = useCallback((event)=>{
        const {value} = event.target;

        if(searchMode === GLOBAL_MODE ){
            setGlobalFilter(value);
        }
        else {
            const newFilters = {...filters};
            if (value === '' || typeof value === 'undefined')//Erase keys of empty filters
                delete newFilters[mainFilter.field];
            else // Or set the new value to filter
                newFilters[mainFilter.field] = value;
            onFiltersChange(newFilters);
        }
    },[filters, onFiltersChange, mainFilter, searchMode]);

    // Handle bar closing on click outside
    const barWindowRef = useRef();
    useLayoutEffect(()=>{
        const clickListener = e =>{
            if( !barWindowRef.current.contains( e.target ) )
               onClose();
        }
        const keyDownListener = (e) => {
            if (e.keyCode === 13 || e.keyCode === 27 ) //enter or esc
                onClose()
        }
        //setTimeout takes the listener binding out of the execution line of the event listener which opened this component.
        // Otherwise the same event that opened the bar may close it instantly.
        setTimeout(()=>document.addEventListener( 'click', clickListener ), 0);
        document.addEventListener( 'keyup', keyDownListener )
        return ()=>{
            document.removeEventListener( 'click', clickListener );
            document.removeEventListener( 'keyup', keyDownListener);
        }
    },[onClose]);

    //Focus on open
    const mainInputRef = useRef();
    useLayoutEffect(()=>mainInputRef.current?.focus(), []);

    //Is any filter active
    const isActive = !!_.find(filters);

    const cleanFilters = (filters)=>{
        let newFilters = {};

        _.forEach(filters, (value, key)=>{
            if(value === '' || typeof value === 'undefined' || value === null || value === 'null')
                return;
            newFilters[key] = value;
        });

        onFiltersChange(newFilters);
    }

    return createPortal(
        <div className={"SearchBar"}>
            <div className={classNames('bar-window', isActive && 'active')} ref={barWindowRef}>
                <div className='main-bar'>
                    <button className='clear-button' data-tooltip={isActive?"Limpiar filtros":undefined} onClick={()=>onFiltersChange({})}>
                        <FontAwesomeIcon icon={isActive? faTimes : faMagnifyingGlass} className='clear-icon' />
                    </button>
                    <input className='main-input'
                           placeholder={ searchMode===GLOBAL_MODE? 'Buscar en todo el sistema...':config.placeholder }
                           value={(searchMode === GLOBAL_MODE? globalFilter : filters?.[mainFilter?.field])||'' }
                           onChange={handleFilterChange}
                           ref={mainInputRef}
                    />
                    <div className='type-buttons'>
                        {localFiltersAvailable &&
                        <button
                            className={classNames('type-btn', searchMode===LOCAL_MODE&&'active')}
                            onClick={()=>changeSearchMode(LOCAL_MODE)}
                            data-tooltip={"Buscar en la pantalla actual"}
                        >
                            <FontAwesomeIcon icon={faFilterList}/>
                        </button>}
                        <button
                            className={classNames('type-btn', searchMode===GLOBAL_MODE&&'active')}
                            onClick={()=>changeSearchMode(GLOBAL_MODE)}
                            data-tooltip={"Buscar en todo el sistema"}
                        >
                            <FontAwesomeIcon icon={faEarthAmericas}/>
                        </button>
                    </div>
                </div>
                <div className={'advance-filtering'} >
                    {searchMode === LOCAL_MODE &&
                    <AdvancedLocalFilters filters={filters} onFiltersChange={cleanFilters} filtersConfig={filtersConfig} />}
                </div>
            </div>
        </div>
        , document.body);
};

export default SearchBar;

