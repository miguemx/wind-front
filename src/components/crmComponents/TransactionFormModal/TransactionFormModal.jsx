import React, {useCallback, useContext} from 'react'
import FormModal from "../../FormModal/FormModal";
import './TransactionFormModal.scss';
import Button from '../../utility/Button/Button';
import {ApiContext} from "../../../services/api/api-config";
import {getNotifier} from "../../../services/notifier";
import {prepareCompleteTransactionForServer, transactionStatusList, transactionStatus} from "../../../services/modelUtils/transactionUtils";
import {useSelector} from "react-redux";

const TransactionFormModal = ({onClose}) => {
    const api = useContext(ApiContext);
    const loadingId = 'TransactionFormModal.create';
    
    const transactionFields = [
        {name:'concept', label:'Concepto', type:'text'},
        {name:'transactionDate', label:'Fecha de la transacción', type:'datepicker'},
        {name:'transactionType', label:'Tipo de transacción', type:'dropdown', 
            options:[
                {value: 'income', label: 'Ingreso'},
                {value: 'expense', label: 'Gasto'}
            ]
        },
        {name:'amount', label:'Cantidad', type:'number'},
        {name: 'transactionCategory', label: 'Categoría de la transacción', type: 'entity', entity: 'transactionCategories', filterLocal: false, preload: true},
        {name:'status', label:'Estado de la transacción', type:'dropdown',  options: transactionStatusList },
        {name:'bankAccount', label:'Selecciona la cuenta', type:'entity', entity:'bankAccounts', autoSelect: true},
    ];

    const handleCreateTransaction = useCallback((form)=>{
        let transaction;
        
        try{
            transaction = prepareCompleteTransactionForServer(form);
        }
        catch (e){
            return getNotifier().warning(e.message);
        }

        api.transactions.create({params: transaction, loadingId})
            .then((transaction)=>{
                getNotifier().success(`La transacción se ha creado correctamente`);
                onClose && onClose();
            })
            .catch((e)=>{ getNotifier().error(e.message) })
    }, [api, loadingId, onClose]);

    const loading = useSelector(s=>!!s.loadingIds[loadingId]);

    return (
        <FormModal
            title={"Nueva transacción"}
            className={"TransactionFormModal"}
            confirmButtonText={'Guardar'}
            cancelButtonText={'Cancelar'}
            onClose={onClose}
            fields={transactionFields}
            onSubmit={handleCreateTransaction}
            disabled={loading}
            initialState={{
                transactionDate: new Date(),
                transactionType: {value: 'income', label: 'Ingreso'},
                status: transactionStatusList.find(s => s.value===transactionStatus.APPROVED)
            }}
        >
            <div className='action-buttons'>
                <Button 
                    outline
                    data-test-id="cancel-trx-button"
                    type="button"
                    onClick={onClose}
                    className="Button outline danger button-large"
                >
                    Cancelar
                </Button>
                
                <Button 
                    outline
                    data-test-id="save-trx-button"
                    type="submit"
                    className="Button outline button-large"
                >
                    Guardar
                </Button>
            </div>
        </FormModal>
    )
}

export default TransactionFormModal;