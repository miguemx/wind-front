import React, {useMemo, useCallback} from 'react';
import TideReactTable from "../../utility/TideReactTable/TideReactTable";
import useTideTable from "../../utility/TideReactTable/useTideTable";
import {memorandumListSGroups} from "../../../services/modelUtils/memorandumUtils";
import moment from 'moment';
import {useHistory} from "react-router-dom";
import {paths} from "../../../services/routes/appRoutes";

const MemorandumTable = ({project}) => {
    const history = useHistory();
    const columns = useMemo(()=>[
        {Header:"Título", accessor:'title'},
        {Header:"Creador", accessor:'createdBy.fullName'},
        {Header:"Creación", id: 'createdDate', accessor: (q) => (q.createdDate? moment(q.createdDate).format('DD/MM/YYYY'):null)},
    ], []);

    const memorandumFilters = useMemo(() => ({
        sGroups: memorandumListSGroups,
        'order[createdDate]':'DESC',
        project: project?.id
    }), [project]);

    const table = useTideTable({
        entity: 'memoranda',
        columns,
        requestFilters: memorandumFilters,
    });

    const onRowClick = useCallback((row) => {
        history.push(paths.memorandumDetail.replace(':id', row.id) )
    }, [history]);

    return (
        <div className={"QuotationRequestTable"}>
            <TideReactTable
                {...table.tableProps}
                onRowClick={onRowClick}
            />
        </div>
    );
}

export default MemorandumTable;