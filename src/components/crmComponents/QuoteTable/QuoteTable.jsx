import React, {useMemo} from 'react';
import TideReactTable from "../../utility/TideReactTable/TideReactTable";
import useTideTable from "../../utility/TideReactTable/useTideTable";
import { getQuoteStatusTrans, quoteListSGroups } from "../../../services/modelUtils/quoteUtils";
import moment from 'moment';
import { moneyFormatter } from '../../../services/currencyUtils';
import {Link} from "react-router-dom";
import {paths} from "../../../services/routes/appRoutes";

const QuoteTable = ({project}) => {
    const columns = useMemo(()=>[
        {
            Header: 'Folio',
            accessor: (quote) =>
                <Link to={paths.quoteDetail.replace(':id', quote.id)} data-tooltip={'Ver cotización'} >
                    {quote.folio}
                </Link>,
            id:'folio'
        },
        {
            Header:"Título",
            accessor: (quote)=><Link to={paths.quoteDetail.replace(':id', quote.id)} data-tooltip={'Ver cotización'} >
                {quote.title}
            </Link>,
            id:'title'
        },
        {Header:"Estado", id:'status', accessor: (q) => getQuoteStatusTrans(q.status) },
        {Header:"Pagado", id:'payed',  accessor: (q) => `$ ${moneyFormatter(Number(q.total) - Number(q.pendingAmount))}`},
        {Header:"Precio", id:'price', accessor: (q) => `$ ${moneyFormatter(Number(q.total))}` },
        {Header:"Creación", accessor: (q) => (q.createdDate? moment(q.createdDate).format('DD/MM/YYYY'):null)},
    ], []);

    const quoteFilters = useMemo(() => ({
        sGroups:quoteListSGroups, 'order[createdDate]':'DESC',
        project: project?.id}), [project]);

    const table = useTideTable({
        entity: 'quotes',
        columns,
        requestFilters: quoteFilters,
    });

    return (
        <div className={"QuoteTable"}>

            <TideReactTable
                {...table.tableProps}
            />

        </div>
    );
};

export default QuoteTable;
