import React from 'react';
import './CompanyBadge.scss';

const CompanyBadge = ({ text }) => {

    return (
        <div className={"CompanyBadge"}>
            {text}
        </div>
    );
};

export default CompanyBadge;
