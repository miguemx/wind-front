import React, {useCallback} from 'react';
import './TopBar.scss';
import UnderlinedTitle from "../../layout/UnderlinedTitle/UnderlinedTitle";
//import MenuButton from "../menu/MenuButton/MenuButton";
import SearchButton from "../../search/SearchButton/SearchButton";
import {useHistory} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/pro-light-svg-icons";

const TopBar = ( {title, titleLinkBack, titleTooltip, filtersConfig, filters, onFiltersChange} ) => {

    const history = useHistory();
    const handleTitleClick = useCallback((e)=>{
        e.preventDefault();
        history.goBack();
    },[history]);

    return (
        <div className={"TopBar"}>
            {title && <div>
                {titleLinkBack?
                <a href='/' data-tooltip={titleTooltip || 'Regresar'} className={'back-link'} onClick={handleTitleClick}>
                    <button className='back-button'>
                        <FontAwesomeIcon icon={faArrowLeft} />
                    </button>
                    <UnderlinedTitle className='title'>{title}</UnderlinedTitle>
                </a>:
                <UnderlinedTitle data-tooltip={titleTooltip}>{title}</UnderlinedTitle>}
            </div>}
            <div className='buttons-container'>
                <SearchButton
                    filtersConfig={filtersConfig}
                    filters={filters}
                    onFiltersChange={onFiltersChange}/>
               {/* <MenuButton />  */}
            </div>
        </div>
    );
};

export default TopBar;
