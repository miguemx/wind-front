import React, { useContext } from 'react';
import './AppVersion.scss';
import { version } from "../../../pckg.json";
import { ApiContext } from '../../../services/api/api-config';

const AppVersion = () => {
  const api = useContext(ApiContext);

  return (
    <div className={'AppVersion'}>
        <p>F {version} - B {api?.appVersion} - Build {process.env.REACT_APP_BUILD_NUMBER}</p>
    </div>
  );
}

export default AppVersion;
