import React from 'react';
import './Button.scss';
import classNames from "classnames";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/pro-light-svg-icons";

const Button = ({ children, color, outline, onClick, disabled, loading, ...props }) => {

    return (
        <button className={
            classNames("Button", loading&&'loading', color, outline&&'outline', disabled&&'disabled')}
            onClick={onClick}
            disabled={disabled}
            {...props}
        >
            <span className='content'>{children}</span>
            {!!loading &&
                /* -- SPINNER -- */
                <span className='spinner-outer'>
                    <span className='spinner-inner'>
                        <FontAwesomeIcon icon={faSpinner} spin />
                    </span>
                </span>}

        </button>
    );
};

export default Button;
