import React, { useContext } from 'react'
import './NavBar.scss';
import getAppRoutes from '../../../services/routes/appRoutes';
import {Link} from "react-router-dom";
import _ from 'lodash';
import { SecurityContext } from '../../../services/SecurityManager';
import classNames from 'classnames';

const NavBar = () => {
    const security = useContext(SecurityContext);
    const routes = getAppRoutes(security);
    const [open, setOpen] = React.useState(false);

	return (
		<nav className="NavBar">
            <div>
                <img src="/static/media/wind-imagotipo-sm.4c8ed3d6.png" alt="" />
            </div>
            <ul className={classNames('NavBar__list-items', open ? "NavBar__list-items--open" : "")}>
            { _.map(routes, (route, i) => {
                if (route.hidden) {
                    return null;
                }
                const { nestedRoutes } = route;
                return (
                    <li className="container-route" key={ route.path||i }>
                        <div className="primary">
                            <Link className="main-primary" to={ route.path }>
                                {route.name}
                            </Link>
                        </div>
                        {
                            <ul className='main-secondary'>
                            { _.map(nestedRoutes, (route, i) => {
                                if (route.hidden) { return null; }
                                return (
                                    <li className="secondary" key={route.path||i} >
                                        <Link className="secondary-link" to={route.path}>
                                            {route.name}
                                        </Link>
                                    </li>
                                );
                            })}
                        </ul>
                        }
                    </li>
                );
            })}
            </ul>

            <div 
                className={classNames("NavBar__hambuger", open ? "open" : "")}
                onClick={() => setOpen(!open)}
            >
                <div className="line"></div>
                <div className="line"></div>
                <div className="line"></div>
            </div>
		</nav>
	)
}

export default NavBar;
