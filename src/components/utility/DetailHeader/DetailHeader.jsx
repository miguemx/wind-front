import classNames from 'classnames';
import React from 'react';
import UnderlinedTitle from '../../layout/UnderlinedTitle/UnderlinedTitle';
import './DetailHeader.scss';

const DetailHeader = ({ title, tooltip, subtitle, className }) => {
  return (
    <div className={classNames("DetailHeader", className)}>
      <UnderlinedTitle secondary>{title}</UnderlinedTitle>
      <span className='client-status' data-tooltip={tooltip}>
        {subtitle}
      </span>
    </div>
  );
}
 
export default DetailHeader;
