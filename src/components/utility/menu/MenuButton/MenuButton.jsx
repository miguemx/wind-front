import React, {useCallback, useRef} from 'react';
import './MenuButton.scss';
import {Player} from "@lottiefiles/react-lottie-player";
import hamburgerAnim from './assets/5145-menu-open-and-close.json';
import useBoolean from "../../../../hooks/useBoolean";
import Menu from "../Menu/Menu";
import classNames from "classnames";

const MenuButton = () => {

    const lottie = useRef(null);
    const setLottieRef = useCallback(instance=>lottie.current = instance, []);

    const [isOpen, open, close] = useBoolean();

    const handleHoverIn = useCallback(()=>{
        if( !isOpen)
            lottie.current?.playSegments([0,20], true);
    },[isOpen]);
    const handleHoverOut = useCallback(()=>{
        if(!isOpen)
            lottie.current?.playSegments([20, 0], true);
    },[isOpen]);
    const handleClick = useCallback(()=>{
        if(isOpen){
            close();
            lottie.current?.playSegments([70, 140], true);
        }
        else{
            open();
            lottie.current?.playSegments([0, 70], true);
        }
    },[isOpen, open, close]);

    return (<>
        <button
            className={classNames("MenuButton", isOpen&&"open")}
            onMouseEnter={handleHoverIn}
            onMouseLeave={handleHoverOut}
            onClick={handleClick}
            data-tooltip={isOpen?"Cerrar menú":"Abrir menú"}
        >
            <span className='text'>
                { isOpen? "cerrar":"menú" }
            </span>
            <div className='lottie-container'>
                <Player
                    keepLastFrame={true}
                    src={hamburgerAnim}
                    lottieRef={setLottieRef} />
            </div>
        </button>
        {isOpen && <Menu close={close}/>}
    </>);
};

export default MenuButton;
