import classNames from 'classnames';
import React from 'react';
import { useCallback, useContext } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getUserAvatarUrl } from '../../../../services/modelUtils/userUtils';
import { paths } from '../../../../services/routes/appRoutes';
import './Avatar.scss';
import { ApiContext } from '../../../../services/api/api-config';

const Avatar = ({ className }) => {
  const api = useContext(ApiContext);
  const me = useSelector(({api})=>api.me);
  const history = useHistory();

  const style = { backgroundImage: `url(${getUserAvatarUrl(me, api)})` };

  const redirectToProfile = useCallback(() => {
    history.push(paths.usersDetail.replace(':id', me.id));
  }, [history, me]);

  return (
    <div className={classNames("Avatar", className)} style={style} onClick={redirectToProfile} />
  );
}
 
export default Avatar;
