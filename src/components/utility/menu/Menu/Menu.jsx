import React, { useContext } from 'react';
import {createPortal} from "react-dom";
import  './Menu.scss';
import getAppRoutes from '../../../../services/routes/appRoutes';
import useBodyClass from "../../../../hooks/useBodyClass";
import {Link} from "react-router-dom";
import { matchPath } from "react-router";
import FollowingBalls from "../FollowingBalls/FollowingBalls";
import AppVersion from '../../AppVersion/AppVersion';
import _ from 'lodash';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faScrubber } from '@fortawesome/pro-light-svg-icons';
import Avatar from '../Avatar/Avatar';
import { SecurityContext } from '../../../../services/SecurityManager';

const Menu = ({close}) => {
    const security = useContext(SecurityContext);
    const routes = getAppRoutes(security);

    const routeActiveClass = (route) => {
        const match = matchPath(window.location.pathname, route);

        return match ? 'active' : '';
    };

    const nestedRouteActiveClass = (route) => {
        const match = matchPath(window.location.pathname, route);

        return match && match.isExact ? 'active' : '';
    };

    useBodyClass('overflow-hidden');

    return createPortal(
        <div className={"Menu animate__animated animate__zoomIn"}>
            <FollowingBalls />

            {_.map(routes, (route, i) => {
                if (route.hidden) { return null; }

                const { nestedRoutes } = route;
                const match = matchPath(window.location.pathname, route)
                return (
                    <div className='route-container' key={route.path||i}>
                        <div className={classNames('main-link-container', routeActiveClass(route))} >
                            {match? <div className="main-link" onClick={()=>(close())} ><Link className={'main-link'} to={route.path}>
                                <span className='active-dot' />{route.name}
                            </Link></div> : 
                            <Link className={'main-link'} to={route.path}>
                                <span className='active-dot' />{route.name}
                            </Link>  }
                            {nestedRoutes && (
                                <div className='secondary-links'>
                                    {
                                        _.map(nestedRoutes, (route, i) => {
                                            if (route.hidden) { return null; }
                                            
                                            const match = matchPath(window.location.pathname, route);
                                            return (
                                                <div className={classNames('secondary-link-container', nestedRouteActiveClass(route))} key={route.path||i} >
                                                    {match? <div className="secundary-link" onClick={()=>(close())}> 
                                                    <Link className={'secondary-link'} to={route.path}>
                                                        <FontAwesomeIcon icon={faScrubber} className='active-icon' />{route.name}
                                                    </Link> </div> : 
                                                    <Link className={'secondary-link'} to={route.path}>
                                                        <FontAwesomeIcon icon={faScrubber} className='active-icon' />{route.name}
                                                    </Link>}
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            )}
                        </div>
                    </div>
                );
            })}

            <Avatar />
            <AppVersion />
        </div>
    , document.body);
};

export default Menu;
