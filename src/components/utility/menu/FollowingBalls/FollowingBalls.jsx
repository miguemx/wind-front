import React, {useEffect, useRef} from 'react';
import './FollowingBalls.scss';
import logo from '../../../../assets/images/wind-isotipo.png';

const FollowingBalls = () => {

    const balls = useRef([]);

    useEffect(()=>{
        let lastX, lastY = 0;
        let runningAnimation = false;
        const moveBallsTowardsTarget = ()=>{
            let allThere = true;//true if all circles are in their final destination
            const windowWidth = window.innerWidth;
            //balls is an array of dom nodes
            balls.current.forEach( (ball, i) => {
                if(!ball) return;//When the component unmounts we may get null
                //Get actual ball size and position
                const {width, height, left, top} = ball.getBoundingClientRect();
                //The target x,y to move to, displaced from the corner to the center of each circle
                const ballTarget = {x: lastX - width / 2, y: lastY - height / 2};
                //Compute the distance to move on each axis and in direct line
                const distanceX = ballTarget.x - left;
                const distanceY = ballTarget.y - top;
                const totalDistance = Math.sqrt( distanceX**2 + distanceY**2 );
                //Compute the speed based on 3 factors
                let speed = 1;//base speed in pixels
                speed *= 1 + i*0.5 ; // size divider (this supposes the balls are sorted by size in ascending order)
                speed *= totalDistance/windowWidth * 10;// distance multiplier, normalized for window size

                //Stop movement if it's already inside a radius of the cursor
                if ( totalDistance < 5 )
                //if (Math.abs(distanceX) <= 3 && Math.abs(distanceY) <= 3)
                    return;
                //When all components are in place, we stop the animation frame loop
                allThere = false;
                //Get the velocity vector using the sine and cosine
                const velocityX = speed * ( distanceX/totalDistance );
                const velocityY = speed * ( distanceY/totalDistance );
                //I want to move it move it
                ball.style.transform = `translate(${left+velocityX}px, ${top+velocityY}px)`
            });

            if(allThere){
                runningAnimation = false;
            }
            else {
                requestAnimationFrame(moveBallsTowardsTarget);
            }
        };
        const mouseMove = (e)=>{
            lastX = e.clientX;
            lastY = e.clientY;
            if( !runningAnimation ){
                runningAnimation = true;
                requestAnimationFrame(moveBallsTowardsTarget);
            }
        }
        document.addEventListener('mousemove', mouseMove);
        return ()=>document.removeEventListener('mousemove', mouseMove);
    });


    return (
        <div className={"FollowingBalls"}>
            <div className='ball ball1' ref={ball=>balls.current[0] = ball} />
            <div className='ball ball2' ref={ball=>balls.current[1] = ball} />
            <div className='ball ball3' ref={ball=>balls.current[2] = ball} />
            <img src={logo} alt={""} className='ball logo' ref={ball=>balls.current[3] = ball} />
        </div>
    );
};

export default FollowingBalls;
