import React from 'react';
import './MoneyField.scss';
import classNames from 'classnames';
import _ from 'lodash';
import { moneyFormatter } from '../../../services/currencyUtils';

const MoneyField = ({ amount, className, placeholder, ...props}) => {
  return (
    <div className={classNames( "MoneyField", !_.isNull(amount) &&"with-content", className)} {...props}>
      <div className='field'>$ {moneyFormatter(amount)} </div>
      {!!placeholder && <span className='placeholder'>{placeholder}</span>}
    </div>
  );
}
 
export default MoneyField;