import moment from "moment";

export const isEmpty = (value, {field}) => {
    const {label} = field;
    if (value)
        return `El campo ${label} puede estar vacío`;
    return true;
};

export const onlyNumbersInString = (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;

    if (!value.match(/^[0-9]+$/))
        return `El campo ${label} debe contener únicamente números`;

    return true;
};

export const exactLengthValidation = (exactLength) => (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;

    if (value.length !== exactLength)
        return `El campo ${label} debe contener ${exactLength} caracteres`;

    return true;
};

export const isEmail = () => (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;


    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)) {
        return true;
    }
    return `El campo ${label} debe ser un correo válido`;
}

export const minLengthValidation = (minLength) => (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;

    if (value.length < minLength)
        return `El campo ${label} debe contener al menos ${minLength} caracteres`;

    return true;
};

export const maxLengthValidation = (maxLength) => (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;

    if (`${value}`.length > maxLength)
        return `El campo ${label} debe contener un máximo de ${maxLength} caracteres`;

    return true;
};

export const isValidDate = (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;

    const date = moment(value);
    if (!date.isValid())
        return 'Ingresa una fecha válida en el campo ' + label;

    return true;
};

export const isFutureDate = (value, {field}) => {
    const validDate = isValidDate(value, {field});
    if (validDate !== true)
        return validDate;

    if (moment().isAfter(value, 'day'))
        return 'Debes agregar una fecha futura en el campo ' + field.label;

    return true;
};

export const minOrEqualNumValidation =  (minNum) => (value, {field}) => {
    const {label} = field;
    if (!value)
        return 'Debes llenar el campo ' + label;

    if (value<minNum)
        return `El campo ${label} debe ser mayor o igual a ${minNum} `;

    return true;
};
/**
 *
 * @param validations
 * @param type "and"|"or"
 * @returns {function(...[*]=)}
 */
export const validationComposer = (validations = [], type, {message} = {}) => (value, extra) => {
    if (type !== 'or' && type !== 'and')
        throw new Error('Bad validation configuration. The "type" parameter should either the string "and" or "or"');

    const errors = [];
    for (let i = 0; i < validations.length; i++) {
        const result = validations[i](value, extra);
        if (result !== true)
            errors.push(result);
    }
    if (errors.length && type === 'and')
        return message || errors.join(' y ');
    else if (errors.length === validations.length && type === 'or')
        return message || errors.join(' o ');

    return true;
};

export default {
    isEmpty,
    exactLengthValidation,
    isFutureDate,
    maxLengthValidation,
    minLengthValidation,
    onlyNumbersInString,
    validationComposer,
    minOrEqualNumValidation,
    isEmail
};
