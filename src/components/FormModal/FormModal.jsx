import React, {useCallback, useState} from 'react';
import Modal from "../utility/Modal/Modal";
import classNames from 'classnames';
import Field from "./Field";
import './FormModal.scss';

/**
 *
 * @param children React objects to be rendered under the form
 * @param className A class to add to the container Modal. It will always have the classes FormModal and Modal as well
 * @param fields Array of objects. See Field.js for the object schema
 * @param errors Array of strings describing the errors in the form
 * @param initialState Object used to initialized the form state
 * @param onClearErrors function Called when the errors displayed are hidden, the errors parameter should be cleaned in this function
 * @param onClose Function called to close the modal with the click event as parameter if it exists.
 * @param onSubmit Function called with the form state as parameter. Only called if all validations are passed
 * @param submitButtonText Text on the bottom submit button. Can a string or a component. Defaults to "Guardar"
 * @param title The Modal title
 * @param onFormChange Called on every form change. If it returns a value it will be used as the new state Warning, this is not a controlled component
 * @param modalProps Props to pass down to Modal.jsx
 * @returns {*}
 * @constructor
 */
const FormModal = ({
                       children,
                       className,
                       fields = [],
                       errors = [],
                       initialState,
                       onClearErrors,
                       onClose,
                       onSubmit,
                       submitButtonText = 'Guardar',
                       title,
                       onFormChange,
                       ...modalProps

                   }) => {

    const [formState, setFormState] = useState(initialState || {});

    const handleChange = useCallback((value, field) => {
        const newState = {...formState, [field]: value};
        const processedData = onFormChange && onFormChange(newState, formState);
        setFormState( processedData || newState );
    }, [formState, onFormChange]);


    const handleSubmit = useCallback((e) => {
        if (e) e.preventDefault();
        onSubmit(formState);
    }, [formState, onSubmit]);

    return (
        <Modal
            className={classNames("FormModal", className)}
            title={title}
            confirmButtonText={submitButtonText}
            onClose={onClose}
            onConfirm={handleSubmit}
            {...modalProps}
        >
            <form className='fields' onSubmit={handleSubmit}>
                {fields.map(field =>
                    <Field
                        key={field.name}
                        field={field}
                        onChange={handleChange}
                        value={formState[field.name]}
                        form={formState}
                        fieldProps={field.fieldProps}
                    />)}
                
                {children}
                
            </form>
        </Modal>
    );
};

export default FormModal;
