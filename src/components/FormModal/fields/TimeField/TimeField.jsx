import React, {useCallback} from 'react';
import './TimeField.scss';
import _ from 'lodash';
import {pad} from "../../../../services/numberUtils";

const initialTime = {
    hour: 0,
    minute: 0
}
const TimeField = ({value = initialTime, onChange, field}) => {

    const hours = _.range(0, 24);
    const minutes = _.range(0, 60);

    const {name, label, hideLabel} = field;

    const handleChange = useCallback(({target}) => {
        const newValue = {...value};
        newValue[target.dataset.part] = parseInt(target.value);
        onChange && onChange(newValue, name);
    }, [onChange, name, value]);

    const labelVisible = !!value && !hideLabel;

    return <div className='TimeField'>
        {labelVisible && <span className='label'>{label}</span>}
        <div className='time-portion'>
            <select className='input left' value={value?.hour || ''} data-part='hour' onChange={handleChange}>
                {hours.map(hour => <option key={hour} value={hour}>{pad(hour)}</option>)}
            </select>
            <span className='time-points'>:</span>
            <select className='input right' value={value?.minute || ''} data-part='minute' onChange={handleChange}>
                {minutes.map(minute => <option key={minute} value={minute}>{pad(minute)}</option>)}
            </select>
        </div>
    </div>;
};

export default React.memo(TimeField);
