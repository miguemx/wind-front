import React, {useCallback} from 'react';
import WindDatePicker from '../../../formComponents/WindDatePicker/WindDatePicker';

const DateField = ({value, onChange, field}) => {
    const {name, label} = field;
    const handleChange = useCallback((value) => onChange && onChange(value, name), [onChange, name]);

    return (
        <WindDatePicker 
            label={label}
            placeholder={label}
            value={value}
            onChange={handleChange}
        />
    );
};

export default React.memo(DateField);
