export const fieldTypes = {
    text: 'text',
    datepicker: 'datepicker',
    time: 'time',
    number: 'number',
    boolean: 'boolean',
    entity: 'entity',
    dropdown: 'dropdown',
};
