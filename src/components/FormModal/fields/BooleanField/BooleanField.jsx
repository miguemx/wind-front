import React, {useCallback} from 'react';
import SwitchInput from "../../../SwitchInput/SwitchInput";


const BooleanField = ({value, onChange, field}) => {

    const {name, label} = field;
    const handleChange = useCallback((value)=>{
        onChange && onChange(value, name)
    },[onChange, name]);

    return (
        <div className={"BooleanField"}>
            <SwitchInput
                label={label}
                value={!!value}
                onChange={handleChange} />
        </div>
    );
};

export default React.memo(BooleanField);
