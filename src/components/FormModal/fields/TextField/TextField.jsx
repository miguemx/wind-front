import React, {useCallback} from 'react';
import './TextField.scss';
import WindInput from '../../../../components/formComponents/WindInput/WindInput';

const TextField = ({value = '', onChange, field, inputProps}) => {
    const {name, label} = field;
    const handleChange = useCallback((value) => onChange && onChange(value, name), [onChange, name]);

    return <WindInput 
            className='WindInput project-field'
            placeholder={label}
            onChange={handleChange}
            inputProps={inputProps} 
            value={value}
        />
};

export default React.memo(TextField);
