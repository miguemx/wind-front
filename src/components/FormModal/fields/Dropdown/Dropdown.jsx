import React, {useCallback} from 'react';
import './Dropdown.scss';
import WindSelect from '../../../formComponents/WindSelect/WindSelect';

const Dropdown = ({value, onChange, field}) => {

    const {name, label, filterLocal, ...fieldProps} = field;

    const handleChange = useCallback((value)=>onChange && onChange(value, name),[onChange, name]);

    return (
        <div className={"Dropdown"}>
            <WindSelect
                placeholder={label}
                name={name}
                value={value}
                onChange={handleChange}
                {...fieldProps}
            />
        </div>
    );
};

export default Dropdown;
