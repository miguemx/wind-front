import React from 'react';
import './WindDatePicker.scss';
import DatePicker from 'react-date-picker';
import classNames from "classnames";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArrowLeft,
    faArrowLeftToLine,
    faArrowRight,
    faArrowRightToLine,
} from "@fortawesome/pro-light-svg-icons";

const WindDatePicker = ({ className, value, onChange, label, disabled }) => {

    return (
        <div className={classNames("WindDatePicker", className)}>
            {!!label && <span className='label'>{label}</span>}

            <DatePicker
                locale={'es-mx'}
                calendarIcon={false}
                nextLabel={<FontAwesomeIcon icon={faArrowRight}/>}
                next2Label={<FontAwesomeIcon icon={faArrowRightToLine}/>}
                prevLabel={<FontAwesomeIcon icon={faArrowLeft}/>}
                prev2Label={<FontAwesomeIcon icon={faArrowLeftToLine}/>}
                value={value}
                onChange={onChange}
                disabled={disabled}
            />
        </div>
    );
};

export default WindDatePicker;
