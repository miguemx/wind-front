import React, {useCallback} from 'react';
import './WindInput.scss';
import classNames from "classnames";
import _ from 'lodash';

const WindInput = ({ value, onChange, placeholder, type, className, disabled, inputProps={}, ...props }) => {

    const handleChange = useCallback((e)=>{
        onChange?.(e.target.value);
    },[onChange]);

    return (
        <div className={classNames( "WindInput", value&&"with-content", className)} {...props}>
            <input
                className={classNames('input-el', inputProps.className)}
                value={typeof value === 'undefined' || _.isNull(value) ? '':value}
                onChange={handleChange}
                type={type}
                disabled={disabled}
                {...inputProps}
            />
            {!!placeholder && <span className='placeholder'>{placeholder}</span>}
        </div>
    );
};

export default WindInput;
