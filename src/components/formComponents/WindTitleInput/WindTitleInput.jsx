import React, {useCallback} from 'react';
import './WindTitleInput.scss';
import classNames from "classnames";

const WindTitleInput = ({ value, onChange, placeholder, type, className, inputProps={}, ...props }) => {

    const handleChange = useCallback((e)=>{
        onChange?.(e.target.value);
    },[onChange]);

    return (
        <div className={classNames('WindTitleInput', className)} >
            <input 
                className='WindTitleInput__input' 
                placeholder={placeholder}
                onChange={handleChange}
                {...inputProps}
                value={value||''}
            />
        </div>
    );
};

export default WindTitleInput;
