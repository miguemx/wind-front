// - poner el tipo de documento con descripción
// - 

import {  copyPropertyOrNull, copyPropertyOrThrow } from "../formUtils";
import { Link } from "react-router-dom";
import { paths } from "../routes/appRoutes";

export const fiscalDataTableColumns = [
    {
        Header: "RFC",
        id: 'id',
        accessor: td => {return <Link to={paths.fiscalesEdit.replace(':id', td.id)} data-tooltip={'Ver/Editar'} >
                    {td.rfc}
                </Link> }
    },
    {
        Header: "Razón social",
        id: 'businessName',
        accessor: 'businessName'
    },
    {
        Header:"Nombre comercial",
        id: 'comercialName',
        accessor: 'comercialName'
    },
    {
        Header:"Correo",
        id: 'email',
        accessor: 'email'
    },
];

export const fiscalDataTableListSGroups = [
    'fiscal_data_read',
];

export const convertFiscalDataToForm = (fiscalData) => {
    return {
        ...fiscalData,
    }
}


export const getEmptyFiscalData = () => {
    return {
        id: null,
        rfc: null,
        businessName: null,
        email: null,
        comercialName: null,
        street: null,
        externalNumber: null,
        internalNumber: null,
        neighborhood: null,
        delegation: null,
        city: null,
        state: null,
        country: null,
        zip: null,
        client: null,
        isSatWsConfigured: null,
        tenantFiscal: null,
        satWsId: null,
        satWsExtractionId: null,
        isFirstDataExtracted: null,
        nextExtractionDate: null,
    };
}


export const fiscalDataSGroups = [
    'fiscal_data_read',
];

export const prepareFiscalDataForServer = (form) => {
    let fiscalData = {}

    copyPropertyOrThrow( form, fiscalData, "rfc", "Por favor escribe el RFC" );
    copyPropertyOrThrow( form, fiscalData, "businessName", "Por favor escribe la razón social" );
    copyPropertyOrThrow( form, fiscalData, "email", "Por favor escribe el correo electrónico" );
    copyPropertyOrThrow( form, fiscalData, "zip", "Por favor escribe código postal" );
    
    copyPropertyOrNull( form, fiscalData, "comercialName" );
    copyPropertyOrNull( form, fiscalData, "street" );
    copyPropertyOrNull( form, fiscalData, "externalNumber" );
    copyPropertyOrNull( form, fiscalData, "internalNumber" );
    copyPropertyOrNull( form, fiscalData, "neighborhood" );
    copyPropertyOrNull( form, fiscalData, "city" );
    copyPropertyOrNull( form, fiscalData, "delegation" );
    copyPropertyOrNull( form, fiscalData, "state" );
    copyPropertyOrNull( form, fiscalData, "country" );
    
    return fiscalData;
}

/*
export const taxDocumentStatus = {
    ACTIVE: "ACTIVE",
    RECEIVED: "RECEIVED",
    CANCELLED: "CANCELLED",
};

export const taxDocumentStatusTrans = {
    RECEIVED: "Recibida",
    CANCELLED: "Cancelada",
    ACTIVE: "Activa",
};

export const taxDocumentStatusOptions = _.map(taxDocumentStatus, (value, key) => {
    return { value: key, label: taxDocumentStatusTrans[key]||'' };
});



export const taxDocumentDetailSGroups = [
    'tax_document_read',
    'tax_document_item_read',
    'company_read',
    'client_read',
    'quote_read_folio',
    'quote_read_total',
    'quote_read_project',
    'project_read_name',
    'app_file_read'
];


export const taxDocumentTypes = {
    I: 'INVOICE', // income
    E: 'CREDIT_NOTE', // outcome
    P: 'PAYMENT',
    N: 'PAYROLL',
    T: 'MERCHANDISE_TRANSFER' // Merchandise Transfer (Traslado)
}

export const transTaxDocumentTypes = {
    I: 'Factura',
    E: 'Nota de crédito',
    P: 'Pago',
    N: 'Nómina',
    T: 'Traslado de mercancía'
}

export const taxDocumentTypesOptions = _.map(taxDocumentTypes, (value, key) => {
    return { value: key, label: value };
});

export const paymentTypes = {
    PUE: 'PUE',
    PPD: 'PPD'
}

export const paymentTypesTrans = {
    PUE: 'Pago en una sola exhibición',
    PPD: 'Pago en parcialidades'
}

export const paymentTypesOptions = _.map(paymentTypes, (value, key) => {
    return { value: key, label: paymentTypesTrans[key]||'' };
});


export const paymentMethods = {
    1:	'Cash',
    2:	'Payroll check',
    3:	'Electronic transfer',
    4:	'Credit card',
    5:	'Electronic wallet',
    6:	'Digital money',
    8:	'Pantry vouchers',
    12:	'Settlement',
    13:	'Payment by subrogation',
    14:	'Payment by consignment',
    15:	'Condonation',
    17:	'Compensation',
    23:	'Novation',
    24:	'Confusion',
    25:	'Debt remittance',
    26:	'Prescription or expiration',
    27:	'To creditor satisfaction',
    28:	'Debit card',
    29:	'Service card',
    99:	'To define'
}

export const paymentMethodsTrans = {
    1:	'Efectivo',
    2:	'Cheque nominativo',
    3:	'Transferencia electrónica de fondos',
    4:	'Tarjeta de crédito',
    5:	'Monedero electrónico',
    6:	'Dinero electrónico',
    8:	'Vales de despensa',
    12:	'Dación en pago',
    13:	'Pago por subrogación',
    14:	'Pago por consignación',
    15:	'Condonación',
    17:	'Compensación',
    23:	'Novación',
    24:	'Confusión',
    25:	'Remisión de deuda',
    26:	'Prescripción o caducidad',
    27:	'Satisfacción del acreedor',
    28:	'Tarjeta de débito',
    29:	'Tarjeta de servicios',
    99:	'Por definir'
}

export const paymentMethodsOptions = _.map(paymentMethods, (value, key) => {
    return { value: key, label: paymentMethodsTrans[key]||'' };
});


export const currency = {
    MXN: 'Mexican pesos',
    USD: 'US dollar',
    EUR: 'Euro',
}

export const currencyTrans = {
    MXN: 'Pesos mexicanos',
    USD: 'Dólar estadounidense',
    EUR: 'Euro'
}

export const currencyOptions = _.map(currency, (value, key) => {
    return { value: key, label: currencyTrans[key]||'' };
});


export const unitCode = {
    H87: 'Pieza',
    EA: 'Elemento',
    E48: 'Unidad de Servicio',
    ACT: 'Actividad',
    KGM: 'Kilogramo',
    E51: 'Trabajo',
    A9: 'Tarifa',
    MTR: 'Metro',
    AB: 'Paquete a granel',
    BB: 'Caja base',
    KT: 'Kit',
    SET: 'Conjunto',
    LTR: 'Litro',
    XBX: 'Caja',
    MON: 'Mes',
    HUR: 'Hora',
    MTK: 'Metro cuadrado',
    11: 'Equipos',
    MGM: 'Miligramo',
    XPK: 'Paquete',
    XKI: 'Kit (Conjunto de piezas)',
    AS: 'Variedad',
    GRM: 'Gramo',
    PR: 'Par',
    DPC: 'Docenas de piezas',
    xun: 'Unidad',
    DAY: 'Día',
    XLT: 'Lote',
    10: 'Grupos',
    MLT: 'Mililitro',
    E54: 'Viaje',
}

export const unitCodeTrans = {
    H87: 'Pieza',
    EA: 'Elemento',
    E48: 'Unidad de Servicio',
    ACT: 'Actividad',
    KGM: 'Kilogramo',
    E51: 'Trabajo',
    A9: 'Tarifa',
    MTR: 'Metro',
    AB: 'Paquete a granel',
    BB: 'Caja base',
    KT: 'Kit',
    SET: 'Conjunto',
    LTR: 'Litro',
    XBX: 'Caja',
    MON: 'Mes',
    HUR: 'Hora',
    MTK: 'Metro cuadrado',
    11: 'Equipos',
    MGM: 'Miligramo',
    XPK: 'Paquete',
    XKI: 'Kit (Conjunto de piezas)',
    AS: 'Variedad',
    GRM: 'Gramo',
    PR: 'Par',
    DPC: 'Docenas de piezas',
    xun: 'Unidad',
    DAY: 'Día',
    XLT: 'Lote',
    10: 'Grupos',
    MLT: 'Mililitro',
    E54: 'Viaje',
}

export const unitCodeOptions = _.map(unitCode, (value, key) => {
    return { value: key, label: unitCodeTrans[key]||'' };
});

export const getTotalFromDocumentItems = (taxDocumentItems) => {
    let total = 0;

    taxDocumentItems.forEach( (item) => {
        const itemAmount = Number(item.price) * Number(item.unitAmount);
        const discountAmount = item.discount ? item.discount : 0;
        const ivaAmount = item.iva ? item.iva * (itemAmount - discountAmount) : 0;        

        total += itemAmount - discountAmount + ivaAmount;
    });

    return total;
};

export const getDocumentTaxesTotal = (taxDocument) => {
    let total = 0;

    if(taxDocument.taxDocumentItems) {
        taxDocument.taxDocumentItems.forEach( (item) => {
            const itemAmount = item.price * item.unitAmount;
            const discountAmount = item.discount ? item.discount : 0;
            const ivaAmount = item.iva ? item.iva * (itemAmount - discountAmount) : 0;
            const iepsAmount = item.ieps ? item.ieps * (itemAmount - discountAmount) : 0;

            total += (ivaAmount + iepsAmount);
        });
    }

    return total;
}

export const getDocumentTotal = (document) => {
    let total = 0;

    if(document.taxDocumentItems) {
        total = getTotalFromDocumentItems(document.taxDocumentItems);
    }

    if(document.discount) {
        total = total * (1 - document.discount);
    }

    return total;
};



export const taxesList = [
    { value: 'IVA', label: 'IVA', code: '002' },
    { value: 'IEPS', label: 'IEPS', code: '003' },
    { value: 'ISR', label: 'ISR', code: '001' },
];

export const xmlTextToObject = async(xmlText) => {
    const taxDocumentObject = {};

    const xmlContent = await xmlText.text();
    const parser = new DOMParser();
    const xml = parser.parseFromString(xmlContent, 'text/xml');

    const errorNode = xml.querySelector("parsererror");

    if (!errorNode) {
        const rootElement = xml.documentElement;

        //if(!rootElement)
        //    return;

        taxDocumentObject.documentType = taxDocumentTypesOptions.find( (option) => option.value === rootElement.getAttribute("TipoDeComprobante") );
        taxDocumentObject.issuedAt = rootElement.getAttribute("Fecha");
        taxDocumentObject.paymentType = paymentTypesOptions.find( (option) => option.value === rootElement.getAttribute("MetodoPago") );
        taxDocumentObject.uuidFolioFiscal = rootElement.getElementsByTagName("cfdi:Complemento")[0].getElementsByTagName("tfd:TimbreFiscalDigital")[0].getAttribute("UUID");
        taxDocumentObject.folio = rootElement.getAttribute("Folio");
        taxDocumentObject.paymentMethod = paymentMethodsOptions.find( (option) => parseInt(option.value) === parseInt(rootElement.getAttribute("FormaPago")) );
        taxDocumentObject.currency = currencyOptions.find( (option) => option.value === rootElement.getAttribute("Moneda") );
        taxDocumentObject.subtotal = rootElement.getAttribute("SubTotal");
        taxDocumentObject.discount = rootElement.getAttribute("Descuento");
        taxDocumentObject.total = rootElement.getAttribute("Total");
        taxDocumentObject.rfc = rootElement.getElementsByTagName("cfdi:Receptor") ? rootElement.getElementsByTagName("cfdi:Receptor")[0].getAttribute("Rfc") : '';

        // parse tax document items
        taxDocumentObject.taxDocumentItems = [];

        const conceptos = rootElement.getElementsByTagName("cfdi:Conceptos")[0].getElementsByTagName("cfdi:Concepto");
        for(let i = 0; i < conceptos.length; i++) {
            const concepto = conceptos[i];
            const taxDocumentItem = {};
            const taxes = {};
            const nodeImpuestos = concepto.getElementsByTagName("cfdi:Impuestos")[0];
            const nodeTraslados = nodeImpuestos ? nodeImpuestos.getElementsByTagName("cfdi:Traslados")[0] : null;

            // search taxes
            if(nodeTraslados) {
                const traslados = nodeTraslados.getElementsByTagName("cfdi:Traslado");
                for(let j = 0; j < traslados.length; j++) {
                    const traslado = traslados[j];
                    const tax = taxesList.find( (tax) => tax.code === traslado.getAttribute("Impuesto") );

                    if(tax) {
                        taxes[tax.value.toLowerCase()] = parseFloat( traslado.getAttribute("TasaOCuota") ).toFixed(2);
                    } else {
                        taxes[tax.value.toLowerCase()] = 0;
                    }
                }
            }
            
            taxDocumentItem.productIdentification = concepto.getAttribute("ClaveProdServ");
            taxDocumentItem.description = concepto.getAttribute("Descripcion");
            taxDocumentItem.unitCode = unitCodeOptions.find( (option) => option.value === concepto.getAttribute("ClaveUnidad") );
            taxDocumentItem.unitAmount = concepto.getAttribute("Cantidad");
            taxDocumentItem.price = parseFloat(concepto.getAttribute("ValorUnitario")).toFixed(2);
            taxDocumentItem.discount = concepto.getAttribute("Descuento")||0;
            taxDocumentItem.iva = taxes.iva ? taxes.iva : 0;
            taxDocumentItem.ieps = taxes.ieps ? taxes.ieps : 0;
            taxDocumentItem.ivaAmount = taxes.iva ? taxDocumentItem.unitAmount*taxDocumentItem.price*taxes.iva : 0;
            taxDocumentItem.iepsAmount = taxes.ieps ? taxDocumentItem.unitAmount*taxDocumentItem.price*taxes.ieps : 0;

            taxDocumentItem.subtotal = concepto.getAttribute("Importe");
            taxDocumentItem.discountAmount = concepto.getAttribute("Descuento");
            taxDocumentItem.total = concepto.getAttribute("Importe") + taxDocumentItem.ivaAmount;
            taxDocumentObject.taxDocumentItems.push(taxDocumentItem);
        }
    }

    return taxDocumentObject;
}

*/