import { faCheck, faEye, faMinusSquare } from "@fortawesome/pro-light-svg-icons";
import _ from "lodash";

export const bankTransactionStatusTrans = {
  PENDING: "Pendiente",
  CONFIRMED: "Confirmado",
  DENIED: "Denegado",
};

export const bankTransactionStatusIcons = {
  PENDING: {
    icon: faEye,
    color: "info",
  },
  CONFIRMED: {
    icon: faCheck,
    color: "success",
  },
  DENIED: {
    icon: faMinusSquare,
    color: "danger",
  },
};

export const bankTransactionStatus = {
  PENDING: 'PENDING',
  CONFIRMED: 'CONFIRMED',
  DENIED: 'DENIED',
};

export const bankTransactionStatusOptions = _.map(bankTransactionStatus, (value) => {
  return { value: value, label: bankTransactionStatusTrans[value] };
});
