
export const ivaPercentage = 0.16;
export const ivaPercentageString = '16%';

export const iepsPercentage = 0.08;
export const iepsPercentageString = '8%';
