// General functions to manipulate numbers, arrays, dates, etc.

import moment from "moment";

export const displayDate = (date)=>{
    if(!date)
        return '';
    return moment(date).format('DD/MM/YYYY');
}
