import {createContext} from 'react';
import _ from 'lodash';

const permissionsToProjects = ["PROJECT_LIST", "PROJECT_CREATE", "PROJECT_UPDATE", "PROJECT_DELETE", "PROJECT_SHOW"];
const permissionsToQuotes = ["QUOTE_LIST", "QUOTE_CREATE", "QUOTE_UPDATE", "QUOTE_DELETE", "QUOTE_SHOW"];
const permissionsToClients = ["CLIENT_LIST", "CLIENT_CREATE", "CLIENT_UPDATE", "CLIENT_DELETE", "CLIENT_SHOW"];
const permissionsToUsers = ["USER_LIST", "USER_CREATE", "USER_UPDATE", "USER_DELETE", "USER_SHOW"];
const permissionsToTransactions = ["TRANSACTION_LIST", "TRANSACTION_CREATE", "TRANSACTION_UPDATE", "TRANSACTION_DELETE", "TRANSACTION_SHOW"];
const permissionsToRecurringPayments = ["RECURRING_PAYMENT_LIST", "RECURRING_PAYMENT_CREATE", "RECURRING_PAYMENT_UPDATE", "RECURRING_PAYMENT_DELETE", "RECURRING_PAYMENT_SHOW"];
const permissionsToBankAccounts = ["BANK_ACCOUNT_LIST", "BANK_ACCOUNT_CREATE", "BANK_ACCOUNT_UPDATE", "BANK_ACCOUNT_DELETE", "BANK_ACCOUNT_SHOW"];
const permissionsToMemorandums = ["MEMORANDUM_LIST", "MEMORANDUM_CREATE", "MEMORANDUM_UPDATE", "MEMORANDUM_DELETE", "MEMORANDUM_SHOW"];
//const permissionsToQuotationRequests = ["QUOTATION_REQUEST_LIST", "QUOTATION_REQUEST_CREATE", "QUOTATION_REQUEST_UPDATE", "QUOTATION_REQUEST_DELETE", "QUOTATION_REQUEST_SHOW"];
const permissionsToListUsers = ["USER_LIST"];

export default class SecurityManager{
    constructor(me){
        if( !me )
            throw new Error("The Security object requires an user to be initialized");

        this.me = me;
        this.role = (me.role && me.role.name) ? me.role.name:'';
        this.permissions = me.permissionsArray||[];
    }

    havePermission($permission){
        if(this.role==='SUPER_ADMIN'){
            return true;
        }
        return _.includes(this.permissions, $permission);
    }

    haveAllPermissions= (permissions=[])=>
        this.role==='SUPER_ADMIN' || permissions.reduce((haveAll, permission)=>haveAll&&this.havePermission(permission), true);

    haveAtLeastOnePermission= (permissions)=>
        this.role==='SUPER_ADMIN' || !!_.intersection(this.permissions, permissions).length>0;

    isActualUser= (user)=> !!(user && user.id===this.me.id );

    canSeeEntity= ()=>true;

    // Users
    isTenantUser = () => !this.me.client;

    // Update
    canUpdateUserPermissionGroups = () => this.havePermission('USER_PERMISSION_GROUP_UPDATE');

    ///////////////////////////
    // System permissions
    canAdminProjects = () => this.haveAllPermissions(permissionsToProjects);
    canAdminQuotes = () => this.haveAllPermissions(permissionsToQuotes);
    canAdminClients = () => this.haveAllPermissions(permissionsToClients);
    canAdminUsers = () => this.haveAllPermissions(permissionsToUsers);
    canAdminTransactions = () => this.haveAllPermissions(permissionsToTransactions);
    canAdminRecurringPayments = () => this.haveAllPermissions(permissionsToRecurringPayments);
    canAdminBankAccounts = () => this.haveAllPermissions(permissionsToBankAccounts);
    canAdminMemorandums = () => this.haveAllPermissions(permissionsToMemorandums);
    canAdminQuotationRequests = () => true; //this.haveAllPermissions(permissionsToQuotationRequests); // TODO: Add permission in front
    canListUsers = () => this.haveAllPermissions(permissionsToListUsers);
}

export const SecurityContext = createContext(null);
