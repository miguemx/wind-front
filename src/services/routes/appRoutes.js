import Logout from "../../scenes/public/Login/Logout";
import ProjectList from "../../scenes/crm/ProjectList/ProjectList";
import ProjectDetail from "../../scenes/crm/ProjectDetail/ProjectDetail";
import ProjectForm from "../../scenes/crm/ProjectForm/ProjectForm";
import QuoteForm from "../../scenes/crm/QuoteForm/QuoteForm";
import ClientList from "../../scenes/crm/ClientList/ClientList";
import ClientDetail from "../../scenes/crm/ClientDetail/ClientDetail";
import UserForm from "../../scenes/crm/UserForm/UserForm";
import UserList from "../../scenes/crm/UserList/UserList";
import QuotationRequestForm from "../../scenes/crm/QuotationRequestForm/QuotationRequestForm";
import UserDetail from "../../scenes/crm/UserDetail/UserDetail";
import QuotationRequestDetail from "../../scenes/crm/QuotationRequestDetail/QuotationRequestDetail";
import ProjectTable from "../../scenes/crm/ProjectTable/ProjectTable";
import QuoteDetail from "../../scenes/crm/QuoteDetail/QuoteDetail";
import MemorandumForm from "../../scenes/crm/MemorandumForm/MemorandumForm";
import MemorandumDetail from "../../scenes/crm/MemorandumDetail/MemorandumDetail";
import TransactionList from "../../scenes/crm/TransactionList/TransactionList";
import BankAccountList from "../../scenes/crm/BankAccountList/BankAccountList";
import BankAccountDetail from "../../scenes/crm/BankAccountDetail/BankAccountDetail";
import BankStatementForm from "../../scenes/crm/BankStatementForm/ BankStatementForm";
import QuoteList from "../../scenes/crm/QuoteList/QuoteList";
import MemorandumList from "../../scenes/crm/MemorandumList/MemorandumList";
import QuotationRequestList from "../../scenes/crm/QuotationRequestList/QuotationRequestList";
import ClientForm from "../../scenes/crm/ClientForm/ClientForm";
import RecurringPaymentList from "../../scenes/crm/RecurringPaymentList/RecurringPaymentList";
import BankStatementList from "../../scenes/crm/BankStatementList/BankStatementList";
import BankStatementDetail from "../../scenes/crm/BankStatementDetail/BankStatementDetail";
import Dashboard from "../../scenes/crm/Dashboard/Dashboard";
import BelvoConnect from "../../scenes/crm/BelvoConnect/BelvoConnect";
import InvoicesList from "../../scenes/crm/InvoicesList/InvoicesList";
import InvoiceForm from "../../scenes/crm/InvoiceForm/InvoiceForm";
import AdminPanel from "../../scenes/crm/AdminPanel/AdminPanel";

export const paths={

    // Accounts
    bankAccounts: '/bank-accounts',
    bankAccountDetail: '/bank-accounts/:id',

    // Bank Statements
    bankStatements: '/bank-statements',
    bankStatementDetail: '/bank-statements/:id',
    bankStatementsForm: '/bank-statements/new',

    // Projects
    projects: '/projects',
    projectNew: '/projects/new',
    projectDetail: '/projects/:id',
    projectEdit: '/projects/:id/edit',
    projectsTable: '/projects/table',

    // Quotes
    quoteForm: '/quotes/new',
    quoteDetail: '/quotes/:id',
    quoteEdit: '/quotes/:id/edit',
    quoteClone: '/quotes/:id/clone',
    quoteList: '/quotes',

    // Quote Requests
    quotationRequestNew: '/quotation_requests/new',
    quotationRequestDetail: '/quotation_requests/:id',
    quotationRequestEdit: '/quotation_requests/:id/edit',
    quotationRequestList: '/quotation_requests',

    // Clients
    clients: '/clients',
    clientNew: '/clients/new',
    clientDetail: '/clients/:id',
    clientEdit: '/clients/:id/edit',

    // Memorandums
    memorandumNew: '/memorandums/new',
    memorandumDetail: '/memorandums/:id',
    memorandumEdit: '/memorandums/:id/edit',
    memorandumList: '/memorandums',

    // Finance
    finance: '/finance',
    financeDashboard: '/finance/dashboard',
    financeIncomes: '/finance/incomes',
    financeExpenses: '/finance/expenses',
    financeAccounts: '/finance/accounts',
    financeTaxes: '/finance/taxes',
    financeTransactions: '/finance/transactions',
    financeRecurringTransactions: '/finance/recurringTransactions',
    financeBelvoConnect: '/finance/belvoConnect',
    financeAccountTransactions: '/finance/accountTransactions/:linkId',
    
    // invoices
    invoices: '/invoices',
    invoicesNew: '/invoices/new',
    invoicesDetail: '/invoices/:id',
    invoicesEdit: '/invoices/:id/edit',

    // Fiscal data
    fiscales: '/fiscalDatas',
    fiscalesSat: '/fiscalDatas/Sat',
    fiscalesNew: '/fiscalDatas/new',
    fiscalesEdit: '/fiscalDatas/:id/edit',

    // Users
    users: '/users',
    usersNew: '/users/new',
    usersDetail: '/users/:id',
    usersEdit: '/users/:id/edit',
    logout: '/logout',

    // Admin
    admin: '/admin'
};

const getAppRoutes = (securityManager)=>{

    let routes = [];
    
    // Projects
    const projectRoutes = {path:paths.projects, component: ProjectList, name: 'Proyectos', nestedRoutes: []};

    if(securityManager?.canAdminProjects()){
        projectRoutes.nestedRoutes.push({path:paths.projectsTable, component: ProjectTable, name: 'Tabla de proyectos'});    
        projectRoutes.nestedRoutes.push({path:paths.projectNew, component: ProjectForm, name: 'Nuevo Proyecto', hidden: true});
        projectRoutes.nestedRoutes.push({path:paths.projectDetail, component: ProjectDetail, name: 'Detalles del proyecto', hidden: true});
        projectRoutes.nestedRoutes.push({path:paths.projectEdit, component: ProjectForm, name: 'Editar proyecto', hidden: true});
    }

    // Memorandums
    if(securityManager?.canAdminMemorandums()){
        projectRoutes.nestedRoutes.push({path:paths.memorandumList, component: MemorandumList, name: 'Minutas'});

        projectRoutes.nestedRoutes.push({path:paths.memorandumNew, component: MemorandumForm, name: 'Nueva minuta', hidden:true});
        projectRoutes.nestedRoutes.push({path:paths.memorandumDetail, component: MemorandumDetail, name: 'Detalles de minuta', hidden: true});
        projectRoutes.nestedRoutes.push({path:paths.memorandumEdit, component: MemorandumForm, name: 'Editar minuta', hidden:true});
    }

    // Quotes
    if(securityManager?.canAdminQuotes()){
        projectRoutes.nestedRoutes.push({path:paths.quoteList, component: QuoteList, name: 'Cotizaciones'});        
        projectRoutes.nestedRoutes.push({path:paths.quoteForm, component: QuoteForm, name: 'Nueva cotización', hidden:true});
        projectRoutes.nestedRoutes.push({path:paths.quoteDetail, component: QuoteDetail, name: 'Nueva cotización', hidden:true});
        projectRoutes.nestedRoutes.push({path:paths.quoteEdit, component: QuoteForm, name: 'Editar cotización', hidden:true});
        projectRoutes.nestedRoutes.push({path:paths.quoteClone, component: QuoteForm, name: 'Clonar cotización', hidden:true});
    }

    projectRoutes.nestedRoutes.push({path:paths.quotationRequestList, component: QuotationRequestList, name: 'Solicitudes'});

    if(projectRoutes.nestedRoutes.length > 0){
        routes.push(projectRoutes);
    }

    // Quote Requests
    //if(securityManager.canAdminQuoteRequests()){
        
        routes.push({path:paths.quotationRequestNew, component: QuotationRequestForm, name: 'Nueva solicitud de cotización', hidden:true});
        routes.push({path:paths.quotationRequestDetail, component: QuotationRequestDetail, name: 'Detalle de solicitud de cotización', hidden:true});
        routes.push({path:paths.quotationRequestEdit, component: QuotationRequestForm, name: 'Editar solicitud de cotización', hidden:true});
    //}
    
    // Clients
    if(securityManager?.canAdminClients()){
        routes.push({path:paths.clients, component: ClientList, name: 'Clientes', nestedRoutes: [
                {path:paths.clientNew, component: ClientForm, name: 'Nuevo Cliente', hidden: true},
        ]});
        routes.push({path:paths.clientDetail, component: ClientDetail, name: 'Detalles del cliente', hidden: true});
        routes.push({path:paths.clientEdit, component: ClientForm, name: 'Editar cliente', hidden: true});
    }

    // Finance
    const financeRoutes = {path:paths.finance, component: Dashboard, name: 'Finanzas', nestedRoutes:[]};

    if(securityManager?.canAdminTransactions()){
        financeRoutes.nestedRoutes.push({path:paths.financeTransactions, component: TransactionList, name: 'Transacciones'});
    }
    
    if(securityManager?.canAdminBankAccounts()){
        financeRoutes.nestedRoutes.push({path:paths.financeAccounts, component: BankAccountList, name: 'Cuentas'});
        financeRoutes.nestedRoutes.push({path:paths.financeTaxes, component: ProjectForm, name: 'Impuestos'});
    }

    if(securityManager?.canAdminRecurringPayments()){
        financeRoutes.nestedRoutes.push({path:paths.financeRecurringTransactions, component: RecurringPaymentList, name: 'Pagos recurrentes'});
    }

    // -- Invoices under Finance
    financeRoutes.nestedRoutes.push( {path:paths.invoices, component: InvoicesList, name: 'Facturas'} );
    financeRoutes.nestedRoutes.push( {path:paths.invoicesNew, component: InvoiceForm, name: 'Nueva factura', hidden: true} );
    financeRoutes.nestedRoutes.push( {path:paths.invoicesEdit, component: InvoiceForm, name: 'Editar factura', hidden: true} );

    routes.push(financeRoutes);
    
    // // Accounts
    routes.push({path:paths.bankAccountDetail, component: BankAccountDetail, name: 'Cuenta', hidden: true});
    routes.push({path:paths.financeAccountTransactions, component: BelvoConnect, name: 'Transacciones de cuenta', hidden: true});

    // Bank Statements
    routes.push({path:paths.bankStatementsForm, component: BankStatementForm, name: 'Nuevo estado bancario', hidden: true});
    routes.push({path:paths.bankStatements, component: BankStatementList, name: 'Estados bancarios', hidden: true});
    routes.push({path:paths.bankStatementDetail, component: BankStatementDetail, name: 'Detalles del estado bancario', hidden: true});

    if(securityManager?.isTenantUser()){
        // Admin
        const adminRoutes = {path:paths.admin, component: AdminPanel, name: 'Administración', nestedRoutes:[]};
        routes.push( adminRoutes );
    }
    

    // Users
    const userRoutes = {path:paths.users, component: UserList, name: 'Usuarios', nestedRoutes:[]};

    if(securityManager?.canAdminUsers() || securityManager?.canListUsers()){
       userRoutes.nestedRoutes.push({path:paths.usersNew, component: UserForm, name: 'Nuevo Usuario'});
       userRoutes.nestedRoutes.push({path:paths.usersDetail, component: UserDetail, name: 'Detalles del usuario', hidden: true});
       userRoutes.nestedRoutes.push({path:paths.usersEdit, component: UserForm, name: 'Editar usuario', hidden: true});
    }

    userRoutes.nestedRoutes.push({path:paths.logout, component: Logout, name: 'Cerrar sesión'});
    routes.push(userRoutes);
    
    return routes;
};


export default getAppRoutes;
