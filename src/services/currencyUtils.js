export const moneyFormatter= ( money, currency ) =>{
    if( isNaN( Number(money) ) )
        return money;

    return Number(money).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + (currency? ` ${currency}`:'');
};

export const roundNumber = ( num, decimals=2 )=>{
    const scale = 10**decimals;
    return Math.round( num*scale ) / scale;
}
